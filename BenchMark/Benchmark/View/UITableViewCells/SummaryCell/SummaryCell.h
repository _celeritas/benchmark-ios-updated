//
//  SummaryCell.h
//  Benchmark
//
//  Created by Html on 20/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UIView *mainUIView;

@property(weak,nonatomic) IBOutlet UILabel *lblTitle;
@property(weak,nonatomic) IBOutlet UILabel *lblScore;

@end
