//
//  ArrivalServiceCell.m
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "ArrivalServiceCell.h"

@implementation ArrivalServiceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.lblQuestionNum.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(5, 5)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.lblQuestionNum.bounds;
    maskLayer.path = maskPath.CGPath;
    self.lblQuestionNum.layer.mask = maskLayer;
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
