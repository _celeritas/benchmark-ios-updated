//
//  ArrivalCommentCell.m
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "ArrivalCommentCell.h"

@implementation ArrivalCommentCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.txtComment.delegate = self;

   }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.text.length == 0) {
        
        if ([string isEqualToString:@" "]){
            
            return NO;
        }
        
    }
    else {
        return YES;
    }
    return YES;
}

@end
