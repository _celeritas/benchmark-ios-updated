//
//  ArrivalCommentCell.h
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArrivalCommentCell : UITableViewCell<UITextFieldDelegate>
@property(weak,nonatomic) IBOutlet UIButton *btnSave;
@property(weak,nonatomic) IBOutlet UITextField *txtComment;
@end
