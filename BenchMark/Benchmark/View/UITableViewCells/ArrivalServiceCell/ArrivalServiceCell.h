//
//  ArrivalServiceCell.h
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArrivalServiceCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UILabel *lblQuestionNum;
@property(strong,nonatomic) IBOutlet UILabel *lblQuestion;

@property(strong,nonatomic) IBOutlet UIButton *btnStop;
@property(strong,nonatomic) IBOutlet UIButton *btnCorrect;
@property(strong,nonatomic) IBOutlet UIButton *btnWrong;
@property(strong,nonatomic) IBOutlet UIButton *btnComment;
@property(strong,nonatomic) IBOutlet UIButton *btnPicture;


@property(strong,nonatomic) IBOutlet UIImageView *imgViewStop;
@property(strong,nonatomic) IBOutlet UIImageView *imgViewCorrect;
@property(strong,nonatomic) IBOutlet UIImageView *imgViewWrong;
@property(strong,nonatomic) IBOutlet UIImageView *imgViewComment;
@property(strong,nonatomic) IBOutlet UIImageView *imgViewPicture;

@end
