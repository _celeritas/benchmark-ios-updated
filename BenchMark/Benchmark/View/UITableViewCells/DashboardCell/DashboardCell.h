//
//  DashboardCell.h
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UIView         *mainUIView;
@property(weak,nonatomic) IBOutlet UILabel        *lblHeading;
@property(weak,nonatomic) IBOutlet UILabel        *lblDescription;
@property(weak,nonatomic) IBOutlet UILabel        *lblStatus_Days;
@property(weak,nonatomic) IBOutlet UILabel        *lblScore;
@property(weak,nonatomic) IBOutlet UILabel        *lblDepartmentName;
@property(weak,nonatomic) IBOutlet UILabel        *lblTrainer;

@end
