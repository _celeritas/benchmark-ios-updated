//
//  RoleCell.h
//  Benchmark
//
//  Created by Mac on 12/19/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoleCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIButton *btnCheck;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;

@end
