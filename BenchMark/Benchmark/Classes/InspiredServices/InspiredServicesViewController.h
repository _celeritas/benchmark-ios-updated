//
//  InspiredServicesViewController.h
//  Benchmark
//
//  Created by Mac on 5/3/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InspiredServicesViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (nonatomic, assign) BOOL isReview;
@property (strong,nonatomic)NSString *currentdatetime1;
@property (strong,nonatomic)NSString *newdatetime1;

@property (strong,nonatomic)NSString *headerTitle;
@property (strong,nonatomic)NSString *subHeader;;

@property (strong,nonatomic)NSString *assessmentIDForService;
@property(strong,nonatomic) IBOutlet UIImageView *deshView;

@end
