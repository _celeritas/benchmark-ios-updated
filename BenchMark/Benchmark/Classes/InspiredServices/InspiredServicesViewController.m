//
//  InspiredServicesViewController.m
//  Benchmark
//
//  Created by Mac on 5/3/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//
#import "AppDelegate.h"
#import "ArrivalServiceCell.h"
#import "ArrivalCommentCell.h"
#import "SummaryViewController.h"
#import "InspiredServicesViewController.h"
#import "QuestionsModel.h"
#import "AssessmentResponse.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Utility.h"


@interface InspiredServicesViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(strong,nonatomic) IBOutlet UITableView * tableView;
@property(strong,nonatomic) IBOutlet UIButton *btnProceed;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property (nonatomic, strong) IBOutlet UIView *viewImageCaptured;
@property (nonatomic, strong) IBOutlet UIImageView *imageViewCapturedImage;
@property (nonatomic, strong) IBOutlet UIButton *btnClose;
@property(strong,nonatomic) IBOutlet UILabel *lblDate;

@end

@implementation InspiredServicesViewController{


    ArrivalServiceCell *assessmentCell;
    ArrivalCommentCell *subCell;
    AppDelegate *appDelegate;
    QuestionsModel *questionsModel;
    AssessmentResponse *assessmentResponse;
    NSMutableDictionary *assessmentAnswersDic, *isAssessmentImageDic,*isAssessmentCommentDic;
     int totalYes,    totalYesNo;
    NSInteger selectedIndex;
    NSString *selectedURL;


}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
}
-(void)viewWillAppear:(BOOL)animated
{
    totalYes = 0;
    totalYesNo = 0;
   // [self.tableView reloadData];
}

-(void)setUI {
    
   // [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [self setCustomCell];
    [self deshLine];
    
    self.btnProceed.layer.borderWidth = 0.0f;
    self.btnProceed.layer.cornerRadius =self.btnProceed.frame.size.height/2.0;
    

    _lblHeader.text =[self.headerTitle uppercaseString];
    self.lblDate.text =self.subHeader;
    _viewImageCaptured.layer.cornerRadius = 10;
    _viewImageCaptured.layer.borderColor = [UIColor colorWithRed:130/255 green:60/255 blue:148/255 alpha:1].CGColor;
    _viewImageCaptured.layer.borderWidth = 1;
    
    
    self.btnClose.layer.borderWidth = 1.0f;
    self.btnClose.layer.cornerRadius = _btnClose.frame.size.height/2.0;
    self.btnClose.layer.borderColor = [UIColor clearColor].CGColor;

    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.imageViewCapturedImage.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                              cornerRadii:CGSizeMake(10, 10)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = self.imageViewCapturedImage.bounds;
    maskLayer.path = maskPath.CGPath;
    self.imageViewCapturedImage.layer.mask = maskLayer;
    
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
   
    
    assessmentAnswersDic =[[NSMutableDictionary alloc]init];
    isAssessmentImageDic =[[NSMutableDictionary alloc]init];
    isAssessmentCommentDic =[[NSMutableDictionary alloc]init];

    if (_isReview) {
        
       // [self getAssessmentResonseMasterData:_assessmentIDForQuestions];
        AssessmentResponse *Obj= [[AssessmentResponse alloc]init];
        [Obj getServiceAssementResponseMasterDataWithAssessmentID:appDelegate.assessmentIDForQuestions];

        if (appDelegate.inspiredServicesResponseMaster.count > 0) {
            
            for (int i = 0; i < appDelegate.inspiredServicesResponseMaster.count; i++)
            {
                assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:i];
                
                //Dictionary for answers
                [assessmentAnswersDic setValue:assessmentResponse.responseText forKey:[NSString stringWithFormat:@"Question%d",i]];
                  [isAssessmentImageDic setValue:assessmentResponse.isImage forKey:[NSString stringWithFormat:@"Image%d",i]];
                [isAssessmentCommentDic setValue:@"" forKey:[NSString stringWithFormat:@"Comment%d",i]];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        });
    }
    else
    {
        QuestionsModel *quesObj= [[QuestionsModel alloc]init];
        [quesObj getAssementQuestionMasterDataWithSectionName:@"12"];
        
        if (appDelegate.inspiredServicesArray.count > 0)
        {
            for (int i = 0; i < appDelegate.inspiredServicesArray.count; i++)
            {
                assessmentResponse = [appDelegate.inspiredServicesArray objectAtIndex:i];
                
                //Dictionary for answers
                [assessmentAnswersDic setValue:@"" forKey:[NSString stringWithFormat:@"Question%d",i]];
                [isAssessmentImageDic setValue:@"" forKey:[NSString stringWithFormat:@"Image%d",i]];
                [isAssessmentCommentDic setValue:@"" forKey:[NSString stringWithFormat:@"Comment%d",i]];
            }
        }
    }
    [self.tableView reloadData];

}

-(void)viewDidLayoutSubviews {
    
    self.tableView.estimatedRowHeight = 100.0f;
      self.tableView.rowHeight = UITableViewAutomaticDimension;
    [  self.tableView reloadData];
    
}

-(void)setCustomCell {

    UINib *nib = [UINib nibWithNibName:@"ArrivalServiceCell" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:@"arrivalServiceCell"];
    UINib *nibComment = [UINib nibWithNibName:@"ArrivalCommentCell" bundle:nil];
    [[self tableView] registerNib:nibComment forCellReuseIdentifier:@"arrivalCommentCell"];

}

#pragma -mark UITableView-Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
   if (_isReview) {
       
        return [appDelegate.inspiredServicesResponseMaster count];
    } else
    {
        return [appDelegate.inspiredServicesArray count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isReview == true)
    {
        assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:section];
        
        if ([[assessmentResponse btnCommentSelected] isEqualToString:@"1"]){
            return 2;
        }
        else
        {
            return 1;
        }
    }
    else {
        
        questionsModel = [appDelegate.inspiredServicesArray objectAtIndex:section];
        if ([[questionsModel btnCommentSelected] isEqualToString:@"1"]){
            return 2;
        }
        else
        {
            return 1;
            
        }
    }
    return 0;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    assessmentCell =  [tableView dequeueReusableCellWithIdentifier:@"arrivalServiceCell"];
    subCell=  [tableView dequeueReusableCellWithIdentifier:@"arrivalCommentCell"];
    
    subCell.txtComment.delegate = self;
    
      subCell.txtComment.tag = indexPath.section;

    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:assessmentCell.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(20, 20)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = assessmentCell.bounds;
    maskLayer.path = maskPath.CGPath;
    
  if (_isReview == true)
    {
        if (appDelegate.isSubmitted)
        {
            assessmentCell.btnWrong.enabled = NO;
            assessmentCell.btnCorrect.enabled = NO;
            assessmentCell.btnStop.enabled = NO;
     
            subCell.btnSave.enabled = NO;
            subCell.btnSave.alpha=0.3;
            subCell.txtComment.userInteractionEnabled=false;
            tableView.allowsSelection = NO;
            
        }
        assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:indexPath.section];
        if(indexPath.row == 0){
            
            assessmentCell.lblQuestionNum.text = [NSString stringWithFormat:@"%ld",indexPath.section+1];
            assessmentCell.lblQuestion.text = [assessmentResponse assessmentQuestion];
            
            if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"YES"])
            {
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject9"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
            }
            else if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NO"])
            {
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject16"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NA"])
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject17"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
      
            if (![[isAssessmentImageDic objectForKey:[NSString stringWithFormat:@"Image%ld",(long)indexPath.section]] isEqualToString:@""])
        
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject13"]];
            }
            else
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject12"]];
            }
            
            
            if ( assessmentResponse.responseComments.length>0)
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
            }else
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject11"]];
            }
    
            
            
            [assessmentCell.btnStop addTarget:self action:@selector(stopButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnCorrect addTarget:self action:@selector(correctButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnWrong addTarget:self action:@selector(wrongButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
   
               [assessmentCell.btnPicture addTarget:self action:@selector(pictureButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            if (appDelegate.isSubmitted)
            {
                if (assessmentResponse.responseComments.length>0)
                {
                    
                    [assessmentCell.btnComment addTarget:self action:@selector(commentButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            else {
                
                [assessmentCell.btnComment addTarget:self action:@selector(commentButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            }
            return assessmentCell;
        }
        
        else
        {
            subCell.txtComment.text =assessmentResponse.responseComments;
            
            [subCell.btnSave addTarget:self action:@selector(saveButtonTaped:) forControlEvents:UIControlEventTouchUpInside];
            
            return subCell;
        }
    }
    else
    {
        
        questionsModel = [appDelegate.inspiredServicesArray objectAtIndex:indexPath.section];
        
        if(indexPath.row == 0){
       
            
            assessmentCell.lblQuestionNum.text = [NSString stringWithFormat:@"%ld",indexPath.section+1];
            assessmentCell.lblQuestion.text = [questionsModel QuestionText];
            
            
            
            if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"YES"])
            {
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject9"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
            }
            else if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NO"])
            {
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject16"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NA"])
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject17"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
      
            
              if (![[isAssessmentImageDic objectForKey:[NSString stringWithFormat:@"Image%ld",(long)indexPath.section]] isEqualToString:@""])
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject13"]];
            }
            else
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject12"]];
            }
            
            
            if ( ![[isAssessmentCommentDic objectForKey:[NSString stringWithFormat:@"Comment%ld",(long)indexPath.section]] isEqualToString:@""] || questionsModel.commentText.length>0)
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
            }else
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject11"]];
            }
            
          //  [isAssessmentImageDic setValue:@"" forKey:[NSString stringWithFormat:@"Image%ld",(long)indexPath.section]];
            
          //  [appDelegate.isAssessmentComment setValue:@"" forKey:[NSString stringWithFormat:@"Comment%ld",(long)indexPath.section]];
            
            
            [assessmentCell.btnStop addTarget:self action:@selector(stopButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnCorrect addTarget:self action:@selector(correctButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnWrong addTarget:self action:@selector(wrongButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [assessmentCell.btnComment addTarget:self action:@selector(commentButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnPicture addTarget:self action:@selector(pictureButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
  
            return assessmentCell;
        }
        else
        {
            subCell.txtComment.text = questionsModel.commentText;
            
            [subCell.btnSave addTarget:self action:@selector(saveButtonTaped:) forControlEvents:UIControlEventTouchUpInside];
            
            return subCell;
        }
    }
    return  assessmentCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return UITableViewAutomaticDimension;
    else
        return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}

#pragma -mark UITableView-Delegates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}

-(IBAction)commentButtonTapeed:(id)sender
{
    ArrivalServiceCell *cell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    NSIndexPath *indexpath = [self.tableView indexPathForCell:cell];

    if (_isReview == true)
    {
        assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:indexpath.section];
        selectedIndex = indexpath.section;
        
        
        if ([assessmentResponse.btnCommentSelected isEqualToString:@"0"]) {
            
            [assessmentResponse setIsAnswerSelected:@"1"];
            [assessmentResponse setBtnCommentSelected:@"1"];
        }else
        {
            [assessmentResponse setIsAnswerSelected:@"0"];
            [assessmentResponse setBtnCommentSelected:@"0"];
        }

        [cell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
        [cell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
        [cell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
        [cell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
        [cell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject12"]];
        
    }
    else {
        questionsModel = [appDelegate.inspiredServicesArray objectAtIndex:indexpath.section];
        selectedIndex = indexpath.section;
        
        if ([questionsModel.btnCommentSelected isEqualToString:@"0"]) {
         
            [questionsModel setIsAnswerSelected:@"1"];
            [questionsModel setBtnCommentSelected:@"1"];
        }else
        {
            [questionsModel setIsAnswerSelected:@"0"];
            [questionsModel setBtnCommentSelected:@"0"];
        }

        [cell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
        [cell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
        [cell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
        [cell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
        [cell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject12"]];
    }
    [self.tableView reloadData];
}


-(IBAction)saveButtonTaped:(id)sender
{
    subCell = (ArrivalCommentCell *)[[[[sender superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableView indexPathForCell:subCell];
    
    subCell = (ArrivalCommentCell *)[self.tableView cellForRowAtIndexPath:path];
  //  selectedIndex = [subCell.btnSave tag];
    
    if (_isReview == true)
    {
        assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:selectedIndex];
        if(subCell.txtComment.text.length>0){
            
            // [questionsModel setIsAnswerSelected:@"1"];
            [assessmentResponse setResponseComments:subCell.txtComment.text];
            [assessmentResponse setIsAnswerSelected:@"0"];
            [assessmentResponse setBtnCommentSelected:@"0"];
            
        }
        
        else{
            [assessmentResponse setResponseComments:@""];
            [assessmentResponse setIsAnswerSelected:@"0"];
            [assessmentResponse setBtnCommentSelected:@"0"];
            
        }
    }
    else{
        questionsModel = [appDelegate.inspiredServicesArray objectAtIndex:selectedIndex];
        
        if(subCell.txtComment.text.length>0){
            
            [questionsModel setCommentText:subCell.txtComment.text];
            [questionsModel setIsAnswerSelected:@"0"];
            [questionsModel setBtnCommentSelected:@"0"];
            
        }
        
        else{
            [questionsModel setCommentText:@""];
            [questionsModel setIsAnswerSelected:@"0"];
            [questionsModel setBtnCommentSelected:@"0"];
            
        }
        
    }
    [self.tableView reloadData];
    
}


-(IBAction)stopButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableView indexPathForCell:assessmentCell];
    
    assessmentCell = (ArrivalServiceCell *)[self.tableView cellForRowAtIndexPath:path];
    
    [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
    
    [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
    
    [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject17"]];
    
    [assessmentAnswersDic setValue:@"NA" forKey:[NSString stringWithFormat:@"Question%ld",(long)path.section]];
    

   
}
-(IBAction)correctButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableView indexPathForCell:assessmentCell];;
    
    assessmentCell = (ArrivalServiceCell *)[self.tableView cellForRowAtIndexPath:path];
    
    [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
    
    [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject9"]];
    
    [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
    
    [assessmentAnswersDic setValue:@"YES" forKey:[NSString stringWithFormat:@"Question%ld",(long)path.section]];
    
    
   
}

-(IBAction)wrongButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableView indexPathForCell:assessmentCell];
    
    assessmentCell = (ArrivalServiceCell *)[self.tableView cellForRowAtIndexPath:path];
    
    [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject16"]];
    
    [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
    
    [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
    
    [assessmentAnswersDic setValue:@"NO" forKey:[NSString stringWithFormat:@"Question%ld",(long)path.section]];
    
    
}

-(IBAction)pictureButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableView indexPathForCell:assessmentCell];
        assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:path.section];
    if (_isReview)
    {
        if(![[isAssessmentImageDic objectForKey:[NSString stringWithFormat:@"Image%ld",(long)path.section]] isEqualToString:@""])
        {
           //
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
             NSString *documentsDirectory = [paths objectAtIndex:0];
             NSString *imageFilePath;
             
           //  imageFilePath = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@.png",[isAssessmentImageDic objectForKey:[NSString stringWithFormat:@"Image%ld",(long)path.section]]]];
             //   [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@.png",assessmentResponse.isImage]];
            imageFilePath =[Utility setDocumentDirectoryPath:assessmentResponse.isImage];
         
               selectedURL =assessmentResponse.imageURL;
            
                NSFileManager *fileManager = [NSFileManager defaultManager];
              //  BOOL fileExists2 =;
            
                if ( [fileManager fileExistsAtPath:imageFilePath]) {
                 
                    _imageViewCapturedImage.image = [UIImage imageWithContentsOfFile:imageFilePath];
                    [_viewImageCaptured setHidden:false];

                }
                else {
                     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    __weak InspiredServicesViewController *weakself = self;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
                    
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [weakself loadPicture:imageFilePath];
                        [MBProgressHUD hideHUDForView:weakself.view animated:YES];
                        
                    });
                }
        }
        else  if (appDelegate.isSubmitted) {
            return;
        }
        
        else if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            //
            [self takePhoto];
        }
    }
    else
    {
           //[self takePhoto];//remove this
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            [self takePhoto];
        } else
        {
            // don't show camera
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"No Camera Available." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                // Ok action example
            }];
            
            [alert addAction:okAction];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
-(void)loadPicture:(NSString*)docPath {
 
    NSData *dataImage1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:selectedURL]];
    
    self.imageViewCapturedImage.image = [UIImage imageWithData:dataImage1];
    [_viewImageCaptured setHidden:false];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if(dataImage1.length>0){
            [dataImage1 writeToFile:docPath atomically:YES];
               
           }
    });
   
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)takePhoto
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:NO completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject13"]];
    
    NSDate * date = [NSDate date];
    NSDateFormatter *dfTime = [NSDateFormatter new];
    [dfTime setDateFormat:@"MMddyyhhmmss a"];
    self.currentdatetime1 =[dfTime stringFromDate:date];
    _currentdatetime1 = [_currentdatetime1 substringToIndex:(_currentdatetime1.length - 3)];
    
    NSIndexPath *path = [self.tableView indexPathForCell:assessmentCell];
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    // define your own path and storage for image
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    documentPath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",_currentdatetime1]];
//    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    NSData *data = UIImagePNGRepresentation([InspiredServicesViewController getNormalizedImage:image]);

    [data writeToFile:documentPath atomically:YES];
    

    
    assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:path.section];
    
    [assessmentResponse setIsImage:_currentdatetime1];
    
    [isAssessmentImageDic setValue:_currentdatetime1 forKey:[NSString stringWithFormat:@"Image%ld",(long)path.section]];
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

+(UIImage*)getNormalizedImage:(UIImage *)rawImage {
    
    /*CGRect rect = CGRectMake(0,0,110,110);
     UIGraphicsBeginImageContext( rect.size );
     [rawImage drawInRect:rect];
     UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();*/
    
    if(rawImage.imageOrientation == UIImageOrientationUp)
        return rawImage;
    
    CGRect rect = CGRectMake(0,0,130,100);
    UIGraphicsBeginImageContext( rect.size);
    [rawImage drawInRect:rect];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return normalizedImage;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    selectedIndex = textField.tag;
}

#pragma mark ===UIButton Action methods ===


-(IBAction)proceedButtonTapped:(id)sender
{
   if (_isReview)
    {
        for (int i = 0; i < appDelegate.inspiredServicesResponseMaster.count; i++)
        {
            assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:i];
            
            assessmentResponse.responseText = [assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%d",i]];
            
                   assessmentResponse.isImage = [isAssessmentImageDic objectForKey:[NSString stringWithFormat:@"Image%d",i]];
            [appDelegate.inspiredServicesResponseMaster replaceObjectAtIndex:i withObject:assessmentResponse];
          
            
            if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"])
            {
                totalYes++;
            }
            if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"] || [[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"NO"])
            {
                totalYesNo++;
            }
        }
    }
    else
    {
        appDelegate.inspiredServicesResponseMaster = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < assessmentAnswersDic.count; i++)
        {
            questionsModel = [appDelegate.inspiredServicesArray objectAtIndex:i];
            assessmentResponse = [[AssessmentResponse alloc]init];
            
            assessmentResponse.responseID = [NSString stringWithFormat:@"%d", i];
            assessmentResponse.assessmentID =_assessmentIDForService;
            assessmentResponse.QuestionID = questionsModel.QuestionID;
    
            if ([[assessmentAnswersDic  objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@""])
            {
                assessmentResponse.responseText = @"NA";
            }else
            {
                assessmentResponse.responseText = [assessmentAnswersDic  objectForKey:[NSString stringWithFormat:@"Question%d",i]];
            }
            
            assessmentResponse.assesmentName = appDelegate.assessmentName;
            assessmentResponse.assesmentDate = _newdatetime1;
            assessmentResponse.assessmentQuestion = questionsModel.QuestionText;
            assessmentResponse.responseComments = questionsModel.commentText;
            assessmentResponse.departmentID = @"NA";
            assessmentResponse.sectionName = questionsModel.SectionName;
            assessmentResponse.isImage = [isAssessmentImageDic objectForKey:[NSString stringWithFormat:@"Image%d",i]];
            assessmentResponse.assessmenTrainer = @"NA";
            
            [appDelegate.inspiredServicesResponseMaster addObject:assessmentResponse];
 
            if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"])
            {
                totalYes++;
            }
            if ([[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"] || [[assessmentAnswersDic objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"NO"])
            {
                totalYesNo++;
            }
        }
    }
   
    // new
    for (int i = 0; i < appDelegate.arrAssessmentResponseMaster.count; i++)
    {
        assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
        for (int r = 0; r<appDelegate.allAssessmentArray.count; r++) {
            
            QuestionsModel*  questionsModel = [appDelegate.allAssessmentArray objectAtIndex:r];
            
            if ([assessmentResponse.assessmentQuestion isEqualToString:questionsModel.QuestionText]) {
                [questionsModel setResponseText:assessmentResponse.responseText];
            }
        }
        
    }

    SummaryViewController *target = [[SummaryViewController alloc]init];
    
    target.assessmentDataLaunchDateTime = [_newdatetime1 substringToIndex:(_newdatetime1.length - 9)];
    target.serviceTotalYes = totalYes;
    target.servicetTotalYesNo = totalYesNo;
    target.isAssessmentImageDic = isAssessmentImageDic;
    [self.navigationController pushViewController:target animated:true];
}


//InspiredServiceAssessmentResponseMaster
-(IBAction)btnClosePressed
{
    [_viewImageCaptured setHidden:true];
    _imageViewCapturedImage.image = [UIImage imageNamed:@""];
}
-(IBAction)backButtonTapped:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)deshLine{
    
    UIBezierPath * path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0.0, 10.0)];
    [path addLineToPoint:CGPointMake(self.deshView.frame.size.width, 10.0)];
    [path setLineWidth:2.0];
    CGFloat dashes[] = { path.lineWidth, path.lineWidth * 2 };
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapSquare];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.deshView.frame.size.width
                                                      , 10), false, 2);
    //  [[UIColor  colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1]setStroke];
    [[UIColor lightGrayColor]setStroke];
    
    [path stroke];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.deshView.image =image;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
