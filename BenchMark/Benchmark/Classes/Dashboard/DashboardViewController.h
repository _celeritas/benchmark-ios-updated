//
//  DashboardViewController.h
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface DashboardViewController : UIViewController <UIGestureRecognizerDelegate>

@property(strong,nonatomic) IBOutlet UITableView    *tableView;
@property(strong,nonatomic) IBOutlet UIButton       *btnLaunchAssessment;
@property(strong,nonatomic) IBOutlet UIImageView       *imgLogout;

@property (nonatomic, strong) DBManager *dbManager;

@property(strong,nonatomic) IBOutlet UIView    *tempView;
@property(strong,nonatomic) IBOutlet UIImageView *deshView;
@property(strong,nonatomic) IBOutlet UILabel *lblDashboard;

@property (nonatomic, assign) BOOL isReturnBack;

@end
