//
//  DashboardViewController.m
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "DashboardViewController.h"
#import "NewAssessmentViewController.h"
#import "AppDelegate.h"
#import "DashboardCell.h"
#import "AssessmentListingModel.h"
#import "AssessmentResponse.h"
#import "ArrivalServiceViewController.h"
#import "RoleModel.h"
#import "Utility.h"

#import "LoginViewController.h"
#import "BMHApis.h"
#import "PropertyModel.h"

//static sqlite3 *database = nil;

@interface DashboardViewController ()
{
    AppDelegate *appDelegate;
    AssessmentListingModel *assessmentListingModel;
    AssessmentResponse *assessmentResponse;
    RoleModel *roleObj ;
}
@end

@implementation DashboardViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [self configureTableViewCell];

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    UIImage *logoutImage = [DashboardViewController imageNamed:@"Icon-logout" withTintColor:[UIColor whiteColor]];
    [self.imgLogout setImage:logoutImage];
   
      // [PropertyModel deleteAllData];
   
    [Utility deshLineWithImageView:self.deshView];
    AssessmentListingModel *listModel = [AssessmentListingModel new];
    [listModel getDashboardAssessmentsData];
   
    if (appDelegate.arrDashboardData.count > 0)
    {
        [self.tableView setHidden:false];
        [self.lblDashboard setHidden:true];
        [self.tableView reloadData];
    }
    else {
        [self.tableView setHidden:true];
        [self.lblDashboard setHidden:false];
    }
 
    roleObj = [RoleModel new];
    [roleObj   getRolesFromDB];
 
}


-(void)configureTableViewCell {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DashboardCell"
                                           bundle:nil] forCellReuseIdentifier:@"DashboardCell"];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

-(NSString *)queryForFetchingRolesQuestionData {
    
    RoleModel *roleObj =[RoleModel new];
    NSArray *roleQueryArray = [assessmentListingModel.assessmentRoles componentsSeparatedByString:@","];
    NSMutableArray *tempRoleArray = [NSMutableArray new];
    for (int i =0; i<roleQueryArray.count; i++) {
        NSString *role = [roleQueryArray objectAtIndex:i];
        for (int r =0; r<appDelegate.rolesArray.count; r++) {
            roleObj = [appDelegate.rolesArray objectAtIndex:r];
            
            if ([role isEqualToString:roleObj.roleTitle]) {
                [tempRoleArray addObject:roleObj.roleID];
            }
        }
        
    }
  
   NSString *baseQuery = [NSString stringWithFormat:@" select * from AssessmentRoleTable where  AssessmentID = '%@' and",assessmentListingModel.assessmentNameID];
    NSString *roleQuery,  *finalQuery ;
  
    if (tempRoleArray.count>0) {
        
        for (int i=0; i< tempRoleArray.count; i++) {
           // roleObj = [tempRoleArray objectAtIndex:i];
            if ( i == 0) {
                roleQuery =[NSString stringWithFormat:@"RoleID = '%@' ",[tempRoleArray objectAtIndex:i]];
            }
            else {
                roleQuery =[NSString stringWithFormat:@"%@ OR RoleID = '%@' ",roleQuery,[tempRoleArray objectAtIndex:i]];
            }
            
        }
      
        finalQuery = [NSString stringWithFormat:@"%@ (%@)",baseQuery,roleQuery];
        
    }
    else {
        finalQuery = [NSString stringWithFormat:@"%@",baseQuery];
        
        
    }
    return finalQuery;
}

+ (UIImage *) imageNamed:(NSString *) name withTintColor: (UIColor *) tintColor {
    
    UIImage *baseImage = [UIImage imageNamed:name];
    
    CGRect drawRect = CGRectMake(0, 0, baseImage.size.width, baseImage.size.height);
    
    UIGraphicsBeginImageContextWithOptions(baseImage.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, baseImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // draw original image
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, drawRect, baseImage.CGImage);
    
    // draw color atop
    CGContextSetFillColorWithColor(context, tintColor.CGColor);
    CGContextSetBlendMode(context, kCGBlendModeSourceAtop);
    CGContextFillRect(context, drawRect);
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

-(IBAction)logoutButtonTapped:(id)sender {
      [ appDelegate.arrDashboardData removeAllObjects];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"UserName"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SaveUpdatedDate"];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isLogin"];

    [[NSUserDefaults standardUserDefaults]synchronize];
   // [PropertyModel deleteAllData];
    BOOL remember = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"];

    LoginViewController *dashBoard = [[LoginViewController alloc]init];
    [self.navigationController pushViewController:dashBoard animated:true];
}
#pragma -mark UITableView-Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return appDelegate.arrDashboardData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    // Create an instance of ItemCell
    DashboardCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"DashboardCell"];
   
    assessmentListingModel = [appDelegate.arrDashboardData objectAtIndex:indexPath.row];
    
    cell.mainUIView.layer.borderWidth = 0.0f;
    cell.mainUIView.layer.cornerRadius = 5.0f;
    
    cell.lblHeading.text = [assessmentListingModel.assessmentPropertyName capitalizedString];
    
    cell.lblScore.text = [assessmentListingModel.assessmentScore stringByAppendingString:@"%"];
    
    cell.lblDescription.text = assessmentListingModel.assessmentName;
    
    cell.lblDepartmentName.text = assessmentListingModel.departmentName;
    
    cell.lblTrainer.text = assessmentListingModel.assessmentTrainer;
    
    
    cell.lblStatus_Days.text = [NSString stringWithFormat:@"%@ / %@",assessmentListingModel.assessmentCurrentStatus,[self timeDifferenceLog:assessmentListingModel.assessmentSubmittedDateTime]];
    
    return cell;
}

#pragma mark ==== Calculate time Method ====
-(NSString*)timeDifferenceLog:(NSString*)time {
    
    NSDate* date1 = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    NSDate*newDate=[dateFormatter dateFromString:time];
    NSTimeInterval distanceBetweenDates = [date1 timeIntervalSinceDate:newDate];
    double secondsInADay = 3600*24;
    double secondsInAnHour = 3600;
    double secondsInAMinute = 60;
    
    NSInteger daysBetweenDates = distanceBetweenDates / secondsInADay;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    NSInteger minutesBetweenDates = distanceBetweenDates / secondsInAMinute;
    
    NSString *logTime;
    
    if (daysBetweenDates) {
        logTime = [NSString stringWithFormat:@"%zd days ago",daysBetweenDates];
    }
    else if (hoursBetweenDates){
        logTime = [NSString stringWithFormat:@"%zd hours ago",hoursBetweenDates];
    }
    else if (minutesBetweenDates){
        logTime = [NSString stringWithFormat:@"%zd min ago",minutesBetweenDates];
    }
    else {
        logTime = [NSString stringWithFormat:@"1 min ago"];
    }
    return logTime;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

#pragma -mark UITableView-Delegates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    assessmentListingModel = [appDelegate.arrDashboardData objectAtIndex:indexPath.row];
    
    ArrivalServiceViewController *target = [[ArrivalServiceViewController alloc]init];
    
    target.isReview = true;
    appDelegate.assessmentRoomNO = assessmentListingModel.assessmentRoomNO;
    appDelegate.assessmentMeal = assessmentListingModel.assessmentMeal;

    appDelegate.dashboardModel= assessmentListingModel;
    target.assessmentListingModelObj = assessmentListingModel;
    
    appDelegate.reviewID = [NSString stringWithFormat:@"%ld",(long)assessmentListingModel.ID];
    
    target.assessmentTrainer = assessmentListingModel.assessmentTrainer;
    
    appDelegate.assessmentPropertyName = assessmentListingModel.assessmentPropertyName;
    
    appDelegate.assessmentName = assessmentListingModel.assessmentName;
    appDelegate.arrAssessmentEmployees = [assessmentListingModel.assessmentEmployees componentsSeparatedByString:@","].mutableCopy;
    
    if ([assessmentListingModel.assessmentCurrentStatus isEqualToString:@"Saved"])
    {
        appDelegate.isResponsesSaved = true;
    }else
    {
        appDelegate.isSubmitted = true;
    }
  
    appDelegate.selectedIndex = indexPath.row;
    appDelegate.assessmentName = assessmentListingModel.assessmentName;
    
    target.assessmentIDForQuestions = assessmentListingModel.assessmentID;
    target.rQuery =[self queryForFetchingRolesQuestionData];
    [self.navigationController pushViewController:target animated:true];
}

#pragma -mark UITableView-Delegates

-(IBAction)launchAssessButtonTapeed:(id)sender {
    
     appDelegate.assessmentRoles =@"";
    NewAssessmentViewController *target = [[NewAssessmentViewController alloc]init];
    [self.navigationController pushViewController:target animated:true];
}

-(void)deshLine
{
    UIBezierPath * path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0.0, 10.0)];
    [path addLineToPoint:CGPointMake(self.deshView.frame.size.width, 10.0)];
    [path setLineWidth:2.0];
    CGFloat dashes[] = { path.lineWidth, path.lineWidth * 2 };
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapSquare];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.deshView.frame.size.width
                                                      , 10), false, 2);
    //  [[UIColor  colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1]setStroke];
    [[UIColor lightGrayColor]setStroke];
    
    [path stroke];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.deshView.image =image;
}

- (void)didReceiveMemoryWarning {
    self.deshView =nil;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
