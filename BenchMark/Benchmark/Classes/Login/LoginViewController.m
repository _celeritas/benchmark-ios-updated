//
//  LoginViewController.m
//  BENCHMARK
//
//  Created by Admin on 4/19/18.
//  Copyright © 2018 CS. All rights reserved.
//

#import "LoginViewController.h"
#import "DashboardViewController.h"
#import "UserLoginModel.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "ForgotPasswordViewController.h"
#import "Utility.h"
#import "BMHApis.h"
#import "GeneralInformationModel.h"
#import "AppDelegate.h"


@interface LoginViewController () <ForgotPasswordViewControllerDelegate>
{
    NSMutableArray *loginUsers;
    UserLoginModel *user;
    BOOL isRememberChecked;
    AppDelegate *appDelegate;
}
@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.authUser = @"unverified";
    
    loginUsers = [[NSMutableArray alloc]init];
    
    user = [[UserLoginModel alloc]init];
    
    user.userName = @"imran.husain@celeritas-solutions.com";
    user.userPassword = @"Montey12$";
    
    [loginUsers addObject:user];
    
    user = [[UserLoginModel alloc]init];
    
    user.userName = @"cbanks@benchmarkglobal.com";
    user.userPassword = @"Calvin123$";
    
    [loginUsers addObject:user];
    self.txtPassword.secureTextEntry =true;
    
    [_txtEmailAddress setTintColor:[UIColor whiteColor]];
    [_txtPassword setTintColor:[UIColor whiteColor]];
    
    //    [_txtEmailAddress setText:@"roshana@gmail.com"];
    //    [_txtPassword setText:@"123"];
    isRememberChecked = false;
    
    
    [self setUI];
}

-(void)emptyTextFeildValues {
    
    self.txtEmailAddress.text = @"";
    self.txtPassword.text = @"";
    
}

-(void)setUI {
    
    _signInView.backgroundColor = [UIColor clearColor];
    _signInView.layer.borderColor = [UIColor whiteColor].CGColor;
    _signInView.layer.borderWidth = 1;
    _signInView.layer.cornerRadius = 5;
    
    _btnSignIn.layer.cornerRadius = _btnSignIn.bounds.size.height/2;
    _btnSignIn.layer.borderColor = [UIColor clearColor].CGColor;
    
    NSMutableAttributedString *placeholderAttributedStringEmail = [[NSMutableAttributedString alloc] initWithAttributedString:self.txtEmailAddress.attributedPlaceholder];
    [placeholderAttributedStringEmail addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [placeholderAttributedStringEmail length])];
    self.txtEmailAddress.attributedPlaceholder = placeholderAttributedStringEmail;
    
    NSMutableAttributedString *placeholderAttributedStringPass = [[NSMutableAttributedString alloc] initWithAttributedString:self.txtPassword.attributedPlaceholder];
    [placeholderAttributedStringPass addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [placeholderAttributedStringPass length])];
    self.txtPassword.attributedPlaceholder = placeholderAttributedStringPass;

//    [self.txtEmailAddress setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    [self.txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    BOOL remember = [[NSUserDefaults standardUserDefaults] boolForKey:@"isRemeber"];
    if (remember) {
        UIImage *rememberImage = [LoginViewController imageNamed:@"btn-CheckBox-Selected" withTintColor:[UIColor whiteColor]];
        [self.imgRememberMe setImage:rememberImage];
 
        NSString *email= [[NSUserDefaults standardUserDefaults] stringForKey:@"Useremail"];
        [[NSUserDefaults standardUserDefaults]synchronize];

        self.txtEmailAddress.text =email;
        isRememberChecked=true;
    }
    else {
    
    UIImage *rememberImage = [LoginViewController imageNamed:@"btn-CheckBox" withTintColor:[UIColor whiteColor]];
        [self.imgRememberMe setImage:rememberImage];
        self.txtEmailAddress.text=@"";
        isRememberChecked=false;

    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //Setting the new text.
    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    textField.text = updatedString;
    
    //Setting the cursor at the right place
    NSRange selectedRange = NSMakeRange(range.location + string.length, 0);
    UITextPosition* from = [textField positionFromPosition:textField.beginningOfDocument offset:selectedRange.location];
    UITextPosition* to = [textField positionFromPosition:from offset:selectedRange.length];
    textField.selectedTextRange = [textField textRangeFromPosition:from toPosition:to];
    
    //Sending an action
    [textField sendActionsForControlEvents:UIControlEventEditingChanged];
    
    return NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return true;
}

-(IBAction)btnRememberMePressed {
    
    if (!isRememberChecked) {
        isRememberChecked = true;
        UIImage *rememberImage = [LoginViewController imageNamed:@"btn-CheckBox-Selected" withTintColor:[UIColor whiteColor]];
        [self.imgRememberMe setImage:rememberImage];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isRemeber"];
    } else {
        isRememberChecked = false;
        UIImage *rememberImage = [LoginViewController imageNamed:@"btn-CheckBox" withTintColor:[UIColor whiteColor]];
        [self.imgRememberMe setImage:rememberImage];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isRemeber"];

    }
}

-(IBAction)btnForgotPressed {
    
    ForgotPasswordViewController *vc = [ForgotPasswordViewController new];
    vc.delegate=self;
    [self.navigationController pushViewController:vc animated:YES];
}

+ (UIImage *) imageNamed:(NSString *) name withTintColor: (UIColor *) tintColor {
    
    UIImage *baseImage = [UIImage imageNamed:name];
    
    CGRect drawRect = CGRectMake(0, 0, baseImage.size.width, baseImage.size.height);
    
    UIGraphicsBeginImageContextWithOptions(baseImage.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, baseImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // draw original image
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, drawRect, baseImage.CGImage);
    
    // draw color atop
    CGContextSetFillColorWithColor(context, tintColor.CGColor);
    CGContextSetBlendMode(context, kCGBlendModeSourceAtop);
    CGContextFillRect(context, drawRect);
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

-(IBAction)btnSignInPressed {
    
    [_btnSignIn setEnabled:true];
    [self.view endEditing:YES];
    
    if (_txtPassword.text.length ==0 || _txtEmailAddress.text.length==0) {
        
        
        UIAlertController *alertNoResponse =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                                message:@"Please enter your email address and password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil];
        
        [alertNoResponse addAction:btnOk];
        
        [self presentViewController:alertNoResponse animated:YES completion:nil];
        
        [_btnSignIn setEnabled:true];
        return;
    }
    else if (![Utility NSStringIsValidEmail:self.txtEmailAddress.text]){
        UIAlertController *alertNoResponse =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                                message:@"Please enter a valid email address" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil];
        
        [alertNoResponse addAction:btnOk];
        [self presentViewController:alertNoResponse animated:YES completion:nil];
        return;
    }
    else {
        [self requestForLogin];
   
    }
}

-(void)requestForLogin {
    
    BMHApis *obj =[BMHApis new];
    [obj requestForGetLogin:self.txtEmailAddress.text WithPassword:self.txtPassword.text withController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        if ([[result valueForKey:@"status"] isEqualToString:@"Success"]) {
            
            NSString *subDic = [result objectForKey:@"data"];
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%@ %@",[subDic valueForKey:@"UserFirstName"],[subDic  valueForKey:@"UserLastName"]] forKey:@"UserName"];
            
            NSString *value = [subDic valueForKey:@"UserID"];

           [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"UserID"];
          //  NSString *email = [subDic valueForKey:@"UserEmailAddress"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLogin"];

            
         if (isRememberChecked) {
             
             [[NSUserDefaults standardUserDefaults] setValue:self.txtEmailAddress.text forKey:@"Useremail"];
             

            }
            [self getAssessmentDataFromServer];
        }
        else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            UIAlertController *alertNoResponse =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                                    message:@"Please enter correct password and email" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
            
            [alertNoResponse addAction:btnOk];
            
            [self presentViewController:alertNoResponse animated:YES completion:nil];
        }
        
        
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}
-(void)moveToDashboar {
  
    DashboardViewController *dashBoard = [[DashboardViewController alloc]init];
    [self.navigationController pushViewController:dashBoard animated:true];
    
}
-(void)getAssessmentDataFromServer {
    [MBProgressHUD showHUDAddedTo:self.view animated:true];

    BMHApis *obj =[BMHApis new];
    [obj requestForGetAssessmentDataFromServerWithViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        NSDictionary *dic = (NSDictionary *)result;
        
        BOOL status = [[dic valueForKey:@"statusCode"]boolValue];
        
        if (status) {
            AssessmentListingModel * obj =[AssessmentListingModel new];
            [obj   loadAssessmentLisingDataFromJSON:result];
            AssessmentResponse *resObj =[AssessmentResponse new];
            [resObj loadAssessmentResponseDataFromJSON:result];
            [resObj loadInspriedServiceDataFromJSON:result];
            GeneralInformationModel *gObj = [GeneralInformationModel new];
            [gObj loadGeneralInformationDataFromJSON:result];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
         
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self moveToDashboar];

        });


    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:true];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [_btnSignIn setEnabled:true];
    BOOL remember = [[NSUserDefaults standardUserDefaults] boolForKey:@"isRemember"];
    if (remember) {
       self.txtEmailAddress.text =[[NSUserDefaults standardUserDefaults] objectForKey:@"Useremail"];
        
        
    }
}

@end

