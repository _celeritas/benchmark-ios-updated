//
//  ForgotPasswordViewController.m
//  Standardyze
//
//  Created by Mac on 7/23/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "BMHApis.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Utility.h"


@interface ForgotPasswordViewController ()
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;


@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
}

-(void)setUI {
    
    _subView.backgroundColor = [UIColor clearColor];
    _subView.layer.borderColor = [UIColor whiteColor].CGColor;
    _subView.layer.borderWidth = 1;
    _subView.layer.cornerRadius = 5;
    
    _btnSubmit.layer.cornerRadius = _btnSubmit.bounds.size.height/2;
    _btnSubmit.layer.borderColor = [UIColor clearColor].CGColor;
    
    NSMutableAttributedString *placeholderAttributedStringPass = [[NSMutableAttributedString alloc] initWithAttributedString:self.txtEmail.attributedPlaceholder];
    [placeholderAttributedStringPass addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [placeholderAttributedStringPass length])];
    self.txtEmail.attributedPlaceholder = placeholderAttributedStringPass;

//    [self.txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
}

-(void)sendDataToserver {
    
    BMHApis *obj = [BMHApis new];
    [obj requestForPassword:self.txtEmail.text viewCOntroller:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        if (success) {
            
            NSDictionary *dict = [result objectForKey:@"response"] ;
            NSString *message = [dict valueForKey:@"message"];
            
            
            BOOL successR = [[dict valueForKey:@"success"]boolValue];
            
            //
            if (successR) {
                UIAlertController *alert =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                              message:message preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  self.txtEmail.text =@"";
                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                              }];
                
                [alert addAction:btnOk];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            else {
                UIAlertController *alert =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                              message:message preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  self.txtEmail.text =@"";
                                                                  
                                                              }];
                
                [alert addAction:btnOk];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        
        else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return true;
}

-(IBAction)btnProcedPressed {
    
    [self.view endEditing:YES];
    
    if (self.txtEmail.text.length == 0) {
        UIAlertController *alertNoResponse =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                                message:@"Please enter your email address" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil];
        
        [alertNoResponse addAction:btnOk];
        [self presentViewController:alertNoResponse animated:YES completion:nil];
        
    }
    if (![Utility NSStringIsValidEmail:self.txtEmail.text]){
        UIAlertController *alertNoResponse =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                                message:@"Please enter a valid email address" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil];
        
        [alertNoResponse addAction:btnOk];
        [self presentViewController:alertNoResponse animated:YES completion:nil];
    }
    
    else {
        [self sendDataToserver];
    }
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //Setting the new text.
    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    textField.text = updatedString;
    
    //Setting the cursor at the right place
    NSRange selectedRange = NSMakeRange(range.location + string.length, 0);
    UITextPosition* from = [textField positionFromPosition:textField.beginningOfDocument offset:selectedRange.location];
    UITextPosition* to = [textField positionFromPosition:from offset:selectedRange.length];
    textField.selectedTextRange = [textField textRangeFromPosition:from toPosition:to];
    
    //Sending an action
    [textField sendActionsForControlEvents:UIControlEventEditingChanged];
    
    return NO;
}
-(IBAction)btnBackPressed {
    
    [self.delegate emptyTextFeildValues];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

