//
//  ForgotPasswordViewController.h
//  Standardyze
//
//  Created by Mac on 7/23/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ForgotPasswordViewControllerDelegate<NSObject,UITextFieldDelegate>
@optional
-(void)emptyTextFeildValues;
@end

@interface ForgotPasswordViewController : UIViewController

@property(strong,nonatomic) id<ForgotPasswordViewControllerDelegate>delegate;

@end
