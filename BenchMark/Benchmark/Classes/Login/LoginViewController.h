//
//  LoginViewController.h
//  BENCHMARK
//
//  Created by Admin on 4/19/18.
//  Copyright © 2018 CS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *signInView;

@property (strong, nonatomic) IBOutlet UIImageView *imgRememberMe;


@property (strong, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnSignIn;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotPassword;

@property (strong, nonatomic) NSString *authUser;

@property (nonatomic) BOOL *auth;

@end
