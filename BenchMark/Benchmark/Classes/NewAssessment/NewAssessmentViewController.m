//
//  NewAssessmentViewController.m
//  BENCHMARK
//
//  Created by Admin on 4/19/18.
//  Copyright © 2018 CS. All rights reserved.
//

#import "NewAssessmentViewController.h"
#import "ArrivalServiceViewController.h"
#import "AssessmentTitleModel.h"
#import "AppDelegate.h"
#import "RoleCell.h"
#import "ClassificationModel.h"
#import "RoleModel.h"
#import "PropertyModel.h"
#import "BMHApis.h"

@interface NewAssessmentViewController ()

{
    AppDelegate *appDelegate;
    NSMutableArray *roleArray,*selectedRoleArray,*roleQueryArray,*trainerList,  *depatmentList;
    AssessmentTitleModel *assessmentTitleObj;
    RoleModel *roleObj ;
    NSString *selectedAssessmentTitle;
    AssessmentTitleModel *selectedAssessmentObj;
    NSString *selectedProperty;
    
}
@property (strong, nonatomic) NSArray *dataArray;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectAll;
@property (strong, nonatomic) IBOutlet UIStackView *stackV;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view4;
@property (strong, nonatomic) IBOutlet UIView *view5;




@end

@implementation NewAssessmentViewController
@synthesize arrAssessmentProperty, arrAssessmentTrainers, arrAssessmentTitles;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    roleQueryArray =[NSMutableArray new];
    appDelegate .arryData = [NSMutableArray new];
    [appDelegate.arrAssessmentEmployees removeAllObjects];
    [self configureTableViewCell];

    [self setUI];
    

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    [self.stackV addGestureRecognizer:tapRecognizer];

}
- (void)highlightLetter:(UITapGestureRecognizer*)sender {
    [Dropobj fadeOut];


}
-(void)viewWillAppear:(BOOL)animated {
    
  //  [appDelegate.arrAssessmentEmployees removeAllObjects];

    [self getAllDataFromServer];
}

-(void)getAllDataFromServer {
    
    appDelegate.isResponsesSaved = FALSE;
    BMHApis *obj = [BMHApis new];
    [obj requestForGetDataFromServerWithViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        if (success) {
            
            BOOL successR = [[result valueForKey:@"statuCode"]boolValue];
            //here  methods
            if (successR) {
                
                NSNumber *ID =  [result valueForKey:@"LastUpdateDate"]; ;
                NSString * updateDate = [ID stringValue];
                
                
                PropertyModel *pObj = [PropertyModel new];
                [pObj loadPropertyDataFromJSON:result];
                [pObj loadDepartmentDataFromJSON:result];
                [pObj loadTrainerDataFromJSON:result];
                [pObj loadMapDataFromJSON:result];
                [pObj loadAssessmentTitleDataFromJSON:result];
                [pObj loadAssessmentMappingDataFromJSON:result];
                [pObj loadTrianerMappingDataFromJSON:result];
                [pObj loadRolesDataFromJSON:result];
                [pObj loadAssessmentRolesMappingDataFromJSON:result];
                [pObj loadInspiredQuestionDataFromJSON:result];
                [pObj loadAssessmentQuestionDataFromJSON:result];
             
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [[NSUserDefaults standardUserDefaults] setObject:updateDate forKey:@"SaveUpdatedDate"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                         [self initialization];
                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                });
            }
            else {
                     [self initialization];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }
        }
        else {
           [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
    
}


///new work
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDown_R:130.0 G:60.0 B:148.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    if ([selectedProperty isEqualToString:@"1"]) {
        

        _lblProperty.text=[arryList objectAtIndex:anIndex];
        PropertyModel *obj = [arrAssessmentProperty objectAtIndex:anIndex];
        depatmentList = [PropertyModel getDepartmentFromDBWithPropertyID:obj.proID];
        trainerList = [PropertyModel getTrainerFromDBwithPropertyID:obj.proID];
        _txtDepartment.text= @"";
        _lblTrainer.text=@"";
        _lblAssessmentTitle.text=@"";
       _lblSelectedRole.text  =@"";

        [self disableRoleTable];
        [self setupForDepatments];
        [self disableAssismentTitle];
        [self disableTrainer];


    }
    else  if ([selectedProperty isEqualToString:@"2"]) {
      
      _txtDepartment.text=[arryList objectAtIndex:anIndex];
        PropertyModel *obj = [depatmentList objectAtIndex:anIndex];
        arrAssessmentTitles =  [AssessmentTitleModel getAssessmentTitleDataWithDepartmentID:obj.deptID];
        [self setupForTrainer];
  
    }
    else  if ([selectedProperty isEqualToString:@"3"]) {
      _lblTrainer.text=[arryList objectAtIndex:anIndex];
        [self setupForAssismentTitle];
      
    }
   else if ([selectedProperty isEqualToString:@"4"]) {
       assessmentTitleObj = [[AssessmentTitleModel alloc]init];
       assessmentTitleObj = [arrAssessmentTitles objectAtIndex:anIndex];
        self.assessmentID = assessmentTitleObj.AssessmentID;
       [self setupForRoleTable];
    }

}

-(void)setupForRoleTable {

    int aID = [self.assessmentID intValue];
    if (aID == 1 || aID < 7) {
        self.viewRole.userInteractionEnabled =true;
        self.viewRole.backgroundColor = [UIColor whiteColor];
        self.viewRole.alpha = 1.0;
        
    }
    else {
        [self disableRoleTable];
    }
    
    _lblAssessmentTitle.text = assessmentTitleObj.AssessmentName;
    selectedAssessmentTitle = assessmentTitleObj.AssessmentColumnName;
    selectedAssessmentObj =assessmentTitleObj;
}

-(void)setupForDepatments {
    
    if (self.arrAssessmentProperty.count>0) {
        
        self.viewDepartment.userInteractionEnabled =true;
        self.viewDepartment.backgroundColor = [UIColor whiteColor];
        self.viewDepartment.alpha = 1.0;
    }
    else {
        
        self.viewDepartment.backgroundColor = [UIColor lightGrayColor];
        self.viewDepartment.alpha = 0.15;
        self.viewDepartment.userInteractionEnabled =false;
        self.txtDepartment.text = @"";
    }
    
}

-(void)setupForTrainer {
    
    if (depatmentList.count>0) {
        
        self.viewTrainer.userInteractionEnabled =true;
        self.viewTrainer.backgroundColor = [UIColor whiteColor];
        self.viewTrainer.alpha = 1.0;
    }
    else {
        [self disableTrainer];
    }
    
}

-(void)disableTrainer {
    
    self.viewTrainer.backgroundColor = [UIColor lightGrayColor];
    self.viewTrainer.alpha = 0.15;
    self.viewTrainer.userInteractionEnabled =false;
    self.lblTrainer.text = @"";
}

-(void)setupForAssismentTitle {
    
    if (trainerList.count>0) {
        
        self.viewAssessmentTitle.userInteractionEnabled =true;
        self.viewAssessmentTitle.backgroundColor = [UIColor whiteColor];
        self.viewAssessmentTitle.alpha = 1.0;
    }
    else {
        
        [self disableAssismentTitle];
    }
    
}
-(void)disableAssismentTitle{
    
    self.viewAssessmentTitle.backgroundColor = [UIColor lightGrayColor];
    self.viewAssessmentTitle.alpha = 0.15;
    self.viewAssessmentTitle.userInteractionEnabled =false;
    self.lblAssessmentTitle.text = @"";
}

-(void)disableRoleTable {
    
    self.viewRole.backgroundColor = [UIColor lightGrayColor];
    self.viewRole.alpha = 0.15;
    self.viewRole.userInteractionEnabled =false;
    self.lblSelectedRole.text = @"";
    appDelegate.assessmentRoles =@"";
    [selectedRoleArray removeAllObjects];
    [roleQueryArray removeAllObjects];
}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    //done
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    selectedRoleArray = [NSMutableArray new];
    roleQueryArray = [NSMutableArray new];
    NSLog(@"ROles:%lu",(unsigned long)ArryData.count);

    if (appDelegate .arryData.count>0) {
        for (int i= 0; i<ArryData.count; i++) {
            
        NSString *strTitle = [appDelegate .arryData objectAtIndex:i];
            for (RoleModel *obj in appDelegate.rolesArray  ) {
                NSLog(@"%@-%@",obj.roleTitle ,strTitle);
                if ([obj.roleTitle isEqualToString:strTitle]) {
                    [selectedRoleArray addObject:obj.roleTitle];
                    [roleQueryArray addObject:obj];
                    break;
                }
            }
     
      }
        appDelegate.assessmentRoles =[selectedRoleArray componentsJoinedByString:@","];
        self.lblSelectedRole.text=  [appDelegate.assessmentRoles stringByReplacingOccurrencesOfString:@"," withString:@", "];
    }
    else{
        self.lblSelectedRole.text=@"";
    }
    
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (![selectedProperty isEqualToString:@"5"]){
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }}
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        
        
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

//////new work
-(void)initialization {
    
    selectedRoleArray = [[NSMutableArray alloc]init];
    arrAssessmentProperty = [PropertyModel getPropertyFromDB];
    arrAssessmentTrainers = [[NSMutableArray alloc]initWithObjects:@"Calvin Banks", nil];
 //  RoleModel* roleObj = [RoleModel new];
 //   [roleObj   getRolesFromDB];
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if (textField.text.length == 0 && [string isEqualToString:@" "]){
        
        return false;
    }
    return YES;
}

#pragma  mark ===Set UI==
-(void)setUI {
    
    self.viewRole.backgroundColor = [UIColor lightGrayColor];
    self.viewRole.alpha = 0.15;
    
    [self disableRoleTable];
    [self setupForDepatments];
    [self setupForTrainer];
    [self setupForAssismentTitle];

    
    appDelegate.assessmentRoles =@"";
    roleArray =[[NSMutableArray alloc]initWithObjects:@"Bartender",@"Cook",@"Host",@"Manager/Chef",@"Server/Order Taker", nil];
    [self deshLine];

    
    _viewDone.layer.borderWidth = 1;
    _viewDone.layer.borderColor = [UIColor colorWithRed:130.0/255.0 green:60.0/255.0 blue:148.0/255.0 alpha:1].CGColor;
    
    [self setBorderOfUIView:_viewDepartment];
    [self setBorderOfUIView:_viewAssessmentTitle];
    [self setBorderOfUIView:_viewProperty];
    [self setBorderOfUIView:_viewTrainer];
    [self setBorderOfUIView:_viewRole];
}

-(void)setBorderOfUIView:(UIView*)view {

    view.layer.borderColor = [UIColor colorWithRed:130.0/255.0 green:60.0/255.0 blue:148.0/255.0 alpha:1].CGColor;
     view.layer.borderWidth = 1;
     view.layer.cornerRadius = 5.0;
}

-(void)setBorderOfUITableView:(UITableView*)view {
    
    view.layer.borderWidth = 1;
    view.layer.borderColor = [UIColor colorWithRed:130.0/255.0 green:60.0/255.0 blue:148.0/255.0 alpha:1].CGColor;
    
}

#pragma mark ===Get data from db ===
-(NSString *)queryForFetchingRolesQuestionData {
    
    NSString *roleQuery,  *finalQuery ;
    if (self.lblSelectedRole.text.length !=0) {
        
    NSString *baseQuery = [NSString stringWithFormat:@" select * from AssessmentRoleTable where  AssessmentID = '%@' and",selectedAssessmentObj.AssessmentID];

    if (roleQueryArray.count>0) {
     
    for (int i=0; i< roleQueryArray.count; i++) {
        roleObj = [roleQueryArray objectAtIndex:i];
        if ( i == 0) {
            roleQuery =[NSString stringWithFormat:@"RoleID = '%@' ",roleObj.roleID];
        }
        else {
            roleQuery =[NSString stringWithFormat:@"%@ OR RoleID = '%@' ",roleQuery,roleObj.roleID];
        }
        
    }
        finalQuery = [NSString stringWithFormat:@"%@ (%@)",baseQuery,roleQuery];

    }
    else {
        finalQuery = [NSString stringWithFormat:@"%@",baseQuery];
        
    }
    }
    return finalQuery;
}

#pragma mark ===COnfigure Cell===
-(void)configureTableViewCell{

    [self.tableViewRole registerNib:[UINib nibWithNibName:@"RoleCell"
                                                      bundle:nil] forCellReuseIdentifier:@"RoleCell"];
    
}

#pragma  mark ===UIButtons Actions Methods===
-(IBAction)btnProperyPressed {
    
    selectedProperty = @"1";
    
    [Dropobj fadeOut];
    NSMutableArray *locrArray = [NSMutableArray new];
    for (PropertyModel *obj in arrAssessmentProperty) {
        [locrArray addObject:obj.proTitle];
    }
    arryList =locrArray;
    
    CGRect f = _scrollView.frame;
    
    [self showPopUpWithTitle:@"Select Property" withOption:arryList xy:CGPointMake(15,  f.origin.y  + self.view1.frame.size.height ) size:CGSizeMake(self.view.frame.size.width-30, 230) isMultiple:NO];

    [self.view endEditing:TRUE];
   
}

-(IBAction)btnDepartmentPressed {
    
    selectedProperty = @"2";
    NSMutableArray *locrArray = [NSMutableArray new];
    for (PropertyModel *obj in depatmentList) {
        [locrArray addObject:obj.deptTitle];
    }
    arryList =locrArray;
    [Dropobj fadeOut];
    CGRect f = _scrollView.frame;
    [self showPopUpWithTitle:@"Select Department" withOption:arryList xy:CGPointMake(15,f.origin.y  + self.view1.frame.size.height + self.view2.frame.size.height +5 ) size:CGSizeMake(self.view.frame.size.width-30, 230) isMultiple:NO];
   //   [self showPopUpWithTitle:@"Select Department" withOption:arryList xy:CGPointMake(15, self.stackV.frame.size.height/2 + f.origin.y) size:CGSizeMake(self.view.frame.size.width-30, 230) isMultiple:NO];
    
}

-(IBAction)btnTrainerPressed {
    
    selectedProperty = @"3";
        NSMutableArray *locrArray = [NSMutableArray new];
    for (PropertyModel *obj in trainerList) {
        NSString *fullName = [NSString stringWithFormat:@"%@ %@",obj.tFName,obj.tLName];
        [locrArray addObject:fullName];
    }
    arryList =locrArray;
    [Dropobj fadeOut];
    CGRect f = self.scrollView.frame;

    [self showPopUpWithTitle:@"Select Trainer" withOption:arryList xy:CGPointMake(15, f.origin.y  + self.view1.frame.size.height + self.view2.frame.size.height + self.view3.frame.size.height  +10 ) size:CGSizeMake(self.view.frame.size.width-30, 230) isMultiple:NO];

}

-(IBAction)btnAssessmentTitlePressed
{
    selectedProperty = @"4";
    NSMutableArray *locrArray = [NSMutableArray new];
    for (AssessmentTitleModel*obj in self.arrAssessmentTitles) {
        [locrArray addObject:obj.AssessmentName];
    }
    arryList =locrArray;
    [Dropobj fadeOut];
    CGRect f = self.scrollView.frame;
  [self showPopUpWithTitle:@"Select Assesement Title" withOption:arryList xy:CGPointMake(15,f.origin.y  + self.view1.frame.size.height + self.view2.frame.size.height + self.view3.frame.size.height + self.view4.frame.size.height +15) size:CGSizeMake(self.view.frame.size.width-30, 150) isMultiple:NO];

    [self.view endEditing:TRUE];

  
}
-(IBAction)btnRolePressed {
    
    selectedProperty = @"5";
    [Dropobj fadeOut];
    arryList=  roleArray;
    CGRect f = self.view5.frame;

  [self showPopUpWithTitle:@"Select Role" withOption:arryList xy:CGPointMake(15, f.origin.y - self.viewRole.frame.size.height) size:CGSizeMake(self.view.frame.size.width-30, 200) isMultiple:YES];

    [self.view endEditing:TRUE];
    
}


-(IBAction)btnLaunchAssessmentPressed {
    
    if (_lblTrainer.text.length == 0 || _lblProperty.text.length == 0 || _lblAssessmentTitle.text.length == 0  || _txtDepartment.text.length == 0 || _lblSelectedRole.text.length == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Please provide your assessment details before proceeding" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        
        [self.view endEditing:TRUE];
    
        ArrivalServiceViewController *target = [[ArrivalServiceViewController alloc]init];
        appDelegate.assessmentIDForQuestions = self.assessmentID;
        appDelegate.assessmentPropertyName = _lblProperty.text;
        target.assessmentIDForQuestions = self.assessmentID;
        target.assessmentTrainer = _lblTrainer.text;
        appDelegate.assessmentName = _lblAssessmentTitle.text;
        target.departmentName = _txtDepartment.text;
        target.isReview = false;
        target.rQuery = [self queryForFetchingRolesQuestionData];
        [self.navigationController pushViewController:target animated:true];
    }
}

-(IBAction)btnBackPressed {
    
    [self.navigationController popViewControllerAnimated:true];
}

#pragma  mark === Draw desh line ===
-(void)deshLine {
    
    UIBezierPath * path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0.0, 10.0)];
    [path addLineToPoint:CGPointMake(self.deshView.frame.size.width, 10.0)];
    [path setLineWidth:2.0];
    CGFloat dashes[] = { path.lineWidth, path.lineWidth * 2 };
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapSquare];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.deshView.frame.size.width
                                                      , 10), false, 2);
    //  [[UIColor  colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1]setStroke];
    [[UIColor lightGrayColor]setStroke];
    
    [path stroke];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.deshView.image =image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
