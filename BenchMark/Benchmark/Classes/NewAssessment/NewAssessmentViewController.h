//
//  NewAssessmentViewController.h
//  BENCHMARK
//
//  Created by Admin on 4/19/18.
//  Copyright © 2018 CS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sqlite3.h"
#import "DropDownListView.h"

@interface NewAssessmentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,kDropDownListViewDelegate>

{
    NSArray *arryList;


    DropDownListView * Dropobj;
}

@property (strong, nonatomic) IBOutlet UIView *viewProperty;
@property (strong, nonatomic) IBOutlet UIView *viewDepartment;
@property (strong, nonatomic) IBOutlet UIView *viewTrainer;
@property (strong, nonatomic) IBOutlet UIView *viewRole;
@property (strong, nonatomic) IBOutlet UIView *viewDone;


@property (strong, nonatomic) IBOutlet UIView *viewAssessmentTitle;

@property (strong, nonatomic) IBOutlet UITableView *tableViewProperty;
@property (strong, nonatomic) IBOutlet UITableView *tableViewTrainer;
@property (strong, nonatomic) IBOutlet UITableView *tableViewAssessment;
@property (strong, nonatomic) IBOutlet UITableView *tableViewRole;


@property (strong, nonatomic) IBOutlet UILabel *lblProperty;
@property (strong, nonatomic) IBOutlet UITextField *txtDepartment;
@property (strong, nonatomic) IBOutlet UILabel *lblTrainer;
@property (strong, nonatomic) IBOutlet UILabel *lblAssessmentTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectedRole;

@property (strong, nonatomic) IBOutlet UILabel *lblAssessment;


@property (strong, nonatomic) IBOutlet UIButton *btnLaunchAssessment;
@property(strong,nonatomic) IBOutlet UIImageView *deshView;

@property (nonatomic, strong) NSMutableArray *arrAssessmentProperty;

@property (nonatomic, strong) NSMutableArray *arrAssessmentTrainers;

@property (nonatomic, strong) NSMutableArray *arrAssessmentTitles;

-(void)getAssessmentTitleData:(NSString *)dbPath;

@property(retain) NSString *assessmentID;


@end
