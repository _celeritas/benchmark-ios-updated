//
//  ArrivalServiceViewController.m
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "ArrivalServiceViewController.h"
#import "InspiredServicesViewController.h"
#import "ArrivalServiceCell.h"
#import "ArrivalCommentCell.h"
#import "AssessmentResponse.h"
#import "QuestionsModel.h"
#import "GeneralInfoViewController.h"
#import "AppDelegate.h"
#import "SummaryViewController.h"
#import "AssessmentListingModel.h"
#import "RoleModel.h"
#import "Utility.h"
#import "AddEmployeeViewControlleriPhone.h"

static sqlite3 *database = nil;

@interface ArrivalServiceViewController () <senddataProtocol,UITextFieldDelegate>{
    AppDelegate *appDelegate;
    QuestionsModel *questionsModel;
    ArrivalServiceCell *assessmentCell;
    ArrivalCommentCell *subCell;
    NSString *isBack;
    NSString *currentdatetime, *newDateString,*selectedURL;
    int totalYes,    totalYesNo;
    AssessmentResponse *assessmentResponse;
    AssessmentListingModel *assessmentListingModel;
    NSMutableArray *tempAssessmentResponse;
    
    NSIndexPath *indexx;
    NSInteger selectedIndex;
    NSString *openSubCell;
    NSMutableArray *tempAssesmentArray;
    
}

@property (nonatomic, strong) IBOutlet UIView *viewImageCaptured;
@property (nonatomic, strong) IBOutlet UIImageView *imageViewCapturedImage;
@property (nonatomic, strong) IBOutlet UIButton *btnClose;

@end

@implementation ArrivalServiceViewController
@synthesize tableViewAssessment;

- (void)viewDidLoad {
 
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.dicAssessmentAnswersArray = [[NSMutableDictionary alloc]init];
    openSubCell =@"No";
    //draw desh line
    [self deshLine];
    // set UI
    [self setUI];
    //configureCell
    [self configureTableviewCell];

  //  self.sectionRows = 1;
    [self loadData];
   
}


#pragma mark ===Get data with Filter methods ===

-(void)loadData {
    
    //is From Review
    if (_isReview) {
        //selected roles
        appDelegate.assessmentRoles = appDelegate.dashboardModel.assessmentRoles;
        // get Assessments with Selected roles
         RoleModel *roleObj =[RoleModel new];
        [roleObj getAssessmentWithSelectedRoles:self.rQuery];
        //get assessments from db with saction name and asssessment iD
      QuestionsModel *model =[QuestionsModel new];
        [model getAssessmentWithAllSections:appDelegate.dashboardModel.assessmentNameID];
        // filter data with assessment id and roles
       [self filterAssessmentQuestionWithRoles];
        appDelegate.isReview = true;
        // get user's saved assessment from db
        AssessmentResponse *obj =[AssessmentResponse new];
        [obj getAssessmentResonseMasterData:_assessmentIDForQuestions];
        
        if (appDelegate.arrAssessmentResponseMaster.count > 0)
        {
            for (int i = 0; i < appDelegate.arrAssessmentResponseMaster.count; i++)
            {
                AssessmentResponse *assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
                
                //Dictionary for answers
                [appDelegate.dicAssessmentAnswersArray setValue:assessmentResponse.responseText forKey:[NSString stringWithFormat:@"Question%d",i]];
                
                [appDelegate.isAssessmentImage setValue:assessmentResponse.isImage forKey:[NSString stringWithFormat:@"Image%d",i]];
                [appDelegate.isAssessmentComment setValue:@"" forKey:[NSString stringWithFormat:@"Comment%d",i]];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        });
    }
    else {
        //is new Assessments
        //get assessments from db with saction name and asssessment iD
        QuestionsModel *model =[QuestionsModel new];
        [model getAssessmentWithAllSections:_assessmentIDForQuestions];
        // get Assessments with Selected roles
        RoleModel *roleObj =[RoleModel new];
        [roleObj getAssessmentWithSelectedRoles:self.rQuery];
        // filter data with assessment id and roles
        [self filterAssessmentQuestionWithRoles];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        });
        
    }
    
}

-(void)filterAssessmentQuestionWithRoles {
    
    appDelegate.allAssessmentArray = [NSMutableArray new];
   
    NSMutableArray *tempArr = [NSMutableArray new];
    NSMutableArray *idsArray = [NSMutableArray new];
    //match assessment ID with Role
    for (int i = 0; i<appDelegate.arrAssessmentRolesQuestions.count; i++) {
        
        RoleModel *roleObj = [appDelegate.arrAssessmentRolesQuestions objectAtIndex:i];
        for (int i = 0; i<appDelegate.arrAssessmentQuestions.count; i++) {
            
            QuestionsModel *modelObj = [appDelegate.arrAssessmentQuestions objectAtIndex:i];
            
            if ([modelObj.QuestionText isEqualToString:roleObj.QuestionText]) {
                [tempArr addObject:modelObj];
                
            }
        }
    }
    //add filtered role question idsArray
    if (appDelegate.arrAssessmentRolesQuestions.count>0) {
        
    for (int i = 0; i<appDelegate.arrAssessmentRolesQuestions.count; i++) {
        
        RoleModel *roleObj = [appDelegate.arrAssessmentRolesQuestions objectAtIndex:i];
        
        [idsArray addObject:roleObj.QuestionText];
        
    }
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:tempArr];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
  
    
        [appDelegate.arrAssessmentQuestions removeAllObjects];
        
        [appDelegate.allAssessmentArray addObjectsFromArray:arrayWithoutDuplicates];
        [appDelegate.arrAssessmentQuestions addObjectsFromArray:[self filterObjectsByKey:@"QuestionText" withArray:arrayWithoutDuplicates]];
    }
    
    else {
 
    [appDelegate.allAssessmentArray addObjectsFromArray:appDelegate.arrAssessmentQuestions];
        [appDelegate.arrAssessmentQuestions removeAllObjects];
        [appDelegate.arrAssessmentQuestions addObjectsFromArray:[self filterObjectsByKey:@"QuestionText" withArray:appDelegate.allAssessmentArray]];
        
    }
    //answer dictionaries
    for (int i = 0; i < appDelegate.arrAssessmentQuestions.count; i++) {
        //Dictionary for answers
        [appDelegate.dicAssessmentAnswersArray setValue:@"" forKey:[NSString stringWithFormat:@"Question%d",i]];
        
        [appDelegate.isAssessmentImage setValue:@"" forKey:[NSString stringWithFormat:@"Image%d",i]];
        
        [appDelegate.isAssessmentComment setValue:@"" forKey:[NSString stringWithFormat:@"Comment%d",i]];
    }
    
    [tableViewAssessment reloadData];
}

- (NSArray *) filterObjectsByKey:(NSString *) key withArray:(NSArray*)ret{
    NSMutableSet *tempValues = [[NSMutableSet alloc] init];
    NSMutableArray *uniqueArray = [NSMutableArray array];
    for(QuestionsModel *obj in ret) {
        if(! [tempValues containsObject:[obj valueForKey:key]]) {
            [tempValues addObject:[obj valueForKey:key]];
            [uniqueArray addObject:obj];
        }
    }
    return uniqueArray;
}

#pragma  mark ===UI Methods ==
-(void)setUI {
    
    tempAssessmentResponse = [[NSMutableArray alloc]init];
    isBack = @"false";
    _lblAssessmentName.text = [appDelegate.assessmentName uppercaseString];
    
    //currentDate
    NSDate * date = [NSDate date];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    newDateString = [dateFormatter stringFromDate:date];
    
    NSDateFormatter *dfTime = [NSDateFormatter new];
    [dfTime setDateFormat:@"MMddyyhhmmss a"];
    currentdatetime =[dfTime stringFromDate:date];
    
    currentdatetime = [currentdatetime substringToIndex:(currentdatetime.length - 3)];
    
    _viewImageCaptured.layer.cornerRadius = 10;
    _viewImageCaptured.layer.borderColor = [UIColor colorWithRed:130/255 green:60/255 blue:148/255 alpha:1].CGColor;
    _viewImageCaptured.layer.borderWidth = 1;
    
    self.btnClose.layer.borderWidth = 0.0f;
    self.btnClose.layer.cornerRadius = 20;
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.imageViewCapturedImage.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                              cornerRadii:CGSizeMake(10, 10)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = self.imageViewCapturedImage.bounds;
    maskLayer.path = maskPath.CGPath;
    self.imageViewCapturedImage.layer.mask = maskLayer;
    
    self.btnProceed.layer.borderWidth = 0.0f;
    self.btnProceed.layer.cornerRadius = 20;
}

-(void)viewDidLayoutSubviews {
    
    tableViewAssessment.estimatedRowHeight = 100.0f;
    tableViewAssessment.rowHeight = UITableViewAutomaticDimension;
    [tableViewAssessment reloadData];
    
}

-(void)sendBoolToPrevious:(NSString *)back {
    
    isBack = back;
    
    if (self.isReview) {
        if ([isBack isEqualToString:@"true"]) {
          //  [_btnGeneralInformation setEnabled:false];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    // [self setUI];
    self.sectionRows = 1;
     // [self loadData];
}

-(void)viewWillAppear:(BOOL)animated {
    
   // [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    totalYes = 0;
    totalYesNo = 0;
    //
   // [self.tableViewAssessment reloadData];
}

#pragma mark ===Get data from db ==
-(void) getAssessmentResonseMasterData:(NSString *)assessmentID {
    //from review
   // [appDelegate.arrAssessmentResponseMaster removeAllObjects];
    appDelegate.arrAssessmentResponseMaster  =[NSMutableArray new];

    NSString *query1=  [NSString stringWithFormat:@"select * from AssessmentResponseMaster where AssessmentID = '%@'",assessmentID];
    
    const char *sql =[query1 UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                assessmentResponse = [[AssessmentResponse alloc]init];
                
                assessmentResponse.responseID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                assessmentResponse.assessmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                assessmentResponse.QuestionID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                
                assessmentResponse.responseText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                
                assessmentResponse.assesmentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,4)];
                
                assessmentResponse.assesmentDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,5)];
                
                assessmentResponse.assessmentQuestion = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,6)];
                
                assessmentResponse.responseComments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                
                assessmentResponse.departmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,8)];
                
                assessmentResponse.sectionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,9)];
                
                //here to work
                //  self.imageViewProfile.image = [UIImage imageWithData:imageData];
                assessmentResponse.isImage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,10)];
                if (![assessmentResponse.isImage isEqualToString:@""]) {
                   assessmentResponse.imageURL = [Utility getPhotoFromServer:assessmentResponse.isImage];
                }
                
              
                
                [assessmentResponse setIsAnswerSelected:@"0"];
                [assessmentResponse setBtnCommentSelected:@"0"];
                [appDelegate.arrAssessmentResponseMaster addObject:assessmentResponse];
            }
            
            if (appDelegate.arrAssessmentResponseMaster.count > 0)
            {
                for (int i = 0; i < appDelegate.arrAssessmentResponseMaster.count; i++)
                {
                    assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
                    
                    //Dictionary for answers
                    [appDelegate.dicAssessmentAnswersArray setValue:assessmentResponse.responseText forKey:[NSString stringWithFormat:@"Question%d",i]];
                    
                    [appDelegate.isAssessmentImage setValue:assessmentResponse.isImage forKey:[NSString stringWithFormat:@"Image%d",i]];
                    [appDelegate.isAssessmentComment setValue:@"" forKey:[NSString stringWithFormat:@"Comment%d",i]];
                }
            }
           // [tableViewAssessment reloadData];
        }
    }
}

#pragma mark ===Configure table cell ===
-(void)configureTableviewCell {
    

    [self.tableViewAssessment registerNib:[UINib nibWithNibName:@"ArrivalServiceCell"
                                           bundle:nil] forCellReuseIdentifier:@"ArrivalServiceCell"];
    
    [self.tableViewAssessment  registerNib:[UINib nibWithNibName:@"ArrivalCommentCell"
                                           bundle:nil] forCellReuseIdentifier:@"ArrivalCommentCell"];
}

#pragma -mark UITableView-Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_isReview)
    {
        return [appDelegate.arrAssessmentResponseMaster count];
        
    } else {
        return [appDelegate.arrAssessmentQuestions count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isReview == true)
    {
        assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:section];
        
        if ([[assessmentResponse btnCommentSelected] isEqualToString:@"1"]){
            return 2;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        questionsModel = [appDelegate.arrAssessmentQuestions objectAtIndex:section];
        if ([[questionsModel btnCommentSelected] isEqualToString:@"1"])
        {
            return 2;
        }
        else
        {
            return 1;
        }
    }
    return 0;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    assessmentCell =  [tableView dequeueReusableCellWithIdentifier:@"ArrivalServiceCell"];
    subCell=  [tableView dequeueReusableCellWithIdentifier:@"ArrivalCommentCell"];
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:assessmentCell.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(20, 20)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    subCell.txtComment.delegate = self;
    subCell.txtComment.tag = [indexPath section];
    maskLayer.frame = assessmentCell.bounds;
    maskLayer.path = maskPath.CGPath;
    
    if (_isReview == true)
    {
        assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:indexPath.section];
        
        if (appDelegate.isSubmitted)
        {
            assessmentCell.btnWrong.enabled = NO;
            assessmentCell.btnCorrect.enabled = NO;
            assessmentCell.btnStop.enabled = NO;
            //
            //
            subCell.btnSave.enabled = NO;
            subCell.btnSave.alpha=0.3;
            subCell.txtComment.userInteractionEnabled=false;
            tableView.allowsSelection = NO;
            
           
        }
        
  
        

        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd-MMM-yyyy hh:mm a"];
        NSDate *str =[dateFormatter dateFromString:assessmentResponse.assesmentDate];
        
        NSDateFormatter *dateFormatter1 = [NSDateFormatter new];
        [dateFormatter1 setDateFormat:@"dd MMM YYYY"];
        
        NSString *d1 =[[dateFormatter1 stringFromDate:str]uppercaseString];
        
        self.lblDate.text =[NSString stringWithFormat:@"Dated : %@",d1];
        
        if(indexPath.row == 0){
            //  assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:indexPath.section];
            
            assessmentCell.lblQuestionNum.text = [NSString stringWithFormat:@"%ld",indexPath.section+1];
            assessmentCell.lblQuestion.text = [assessmentResponse assessmentQuestion];
            
            if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"YES"])
            {
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject9"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
            }
            else if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NO"])
            {
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject16"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NA"])
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject17"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            
            if (![[appDelegate.isAssessmentImage objectForKey:[NSString stringWithFormat:@"Image%ld",(long)indexPath.section]] isEqualToString:@""])
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject13"]];
            }
            else
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject12"]];
            }
            
            if ( assessmentResponse.responseComments.length>0)
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
            }else
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject11"]];
            }
            
            [appDelegate.isAssessmentComment setValue:@"" forKey:[NSString stringWithFormat:@"Comment%ld",(long)indexPath.section]];
            
            [assessmentCell.btnStop addTarget:self action:@selector(stopButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnCorrect addTarget:self action:@selector(correctButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnWrong addTarget:self action:@selector(wrongButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            
            if (appDelegate.isSubmitted)
            {
                if (assessmentResponse.responseComments.length>0)
                {
                    [assessmentCell.btnComment addTarget:self action:@selector(commentButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            
            else {
                
                [assessmentCell.btnComment addTarget:self action:@selector(commentButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [assessmentCell.btnPicture addTarget:self action:@selector(pictureButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            
            return assessmentCell;
        }
        
        else
        {
            subCell.txtComment.text =assessmentResponse.responseComments;
            
            [subCell.btnSave addTarget:self action:@selector(saveButtonTaped:) forControlEvents:UIControlEventTouchUpInside];
            
            return subCell;
        }
    }
    else
    {
     
     
        questionsModel = [appDelegate.arrAssessmentQuestions objectAtIndex:indexPath.section];

        NSDateFormatter *dateFormatter1 = [NSDateFormatter new];
        [dateFormatter1 setDateFormat:@"dd MMM YYYY"];
        
        NSString *d1 =[[dateFormatter1 stringFromDate:[NSDate date]]uppercaseString];
        self.lblDate.text =[[ NSString stringWithFormat:@"Dated : %@",d1] uppercaseString];
        
        if(indexPath.row == 0){
            
            assessmentCell.lblQuestionNum.text = [NSString stringWithFormat:@"%ld",indexPath.section+1];
            assessmentCell.lblQuestion.text = [questionsModel QuestionText];
            
            if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"YES"])
            {
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject9"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
            }
            else if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NO"])
            {
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject16"]];
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%ld",indexPath.section]] isEqualToString:@"NA"])
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject17"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            else
            {
                [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
                [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
                [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
            }
            
            if (![[appDelegate.isAssessmentImage objectForKey:[NSString stringWithFormat:@"Image%ld",(long)indexPath.section]] isEqualToString:@""])
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject13"]];
            }
            else
            {
                [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject12"]];
            }
            
            if ( ![[appDelegate.isAssessmentComment objectForKey:[NSString stringWithFormat:@"Comment%ld",(long)indexPath.section]] isEqualToString:@""] || questionsModel.commentText.length>0)
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
            }else
            {
                [assessmentCell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject11"]];
            }
            [assessmentCell.btnStop addTarget:self action:@selector(stopButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnCorrect addTarget:self action:@selector(correctButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnWrong addTarget:self action:@selector(wrongButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [assessmentCell.btnComment addTarget:self action:@selector(commentButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            [assessmentCell.btnPicture addTarget:self action:@selector(pictureButtonTapeed:) forControlEvents:UIControlEventTouchUpInside];
            
            
            return assessmentCell;
        }
        else
        {
            subCell.txtComment.text = questionsModel.commentText;
            [subCell.btnSave addTarget:self action:@selector(saveButtonTaped:) forControlEvents:UIControlEventTouchUpInside];
            
            return subCell;
        }
    }
    return  0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return UITableViewAutomaticDimension;
    else
        return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}

#pragma -mark UITableView-Delegates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isReview == true)
    {
        if (appDelegate.isSubmitted)
        {
            tableView.allowsSelection = NO;
        }
    }
}

#pragma -mark UIButtons-Actions
-(IBAction)btnAddEmployeesPressed {
    
    AddEmployeeViewControlleriPhone *addVC = [AddEmployeeViewControlleriPhone new];
    addVC.isReview =self.isReview;

    [self.navigationController pushViewController:addVC animated:true];

}

-(IBAction)btnGeneraInfoPressed
{
  
     GeneralInfoViewController *infoViewController = [[GeneralInfoViewController alloc]init];
    if (_isReview)
    {
        infoViewController.assessmentID = self.assessmentIDForQuestions;
        infoViewController.assessmentListingModelObj = self.assessmentListingModelObj;

    }
    else
    {
        infoViewController.assessmentID = currentdatetime;
    }
    infoViewController.isReview = _isReview;
    infoViewController.senddataProtocolObject = self;
    [self.navigationController pushViewController:infoViewController animated:true];

}

-(IBAction)btnClosePressed
{
    [self.mainImageView setHidden:true];
    self.imageViewCapturedImage.image = [UIImage imageNamed:@""];
}

-(IBAction)commentButtonTapeed:(id)sender
{
    ArrivalServiceCell *cell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    NSIndexPath *indexpath = [self.tableViewAssessment indexPathForCell:cell];
    
    if (_isReview == true)
    {
        assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:indexpath.section];
        selectedIndex = indexpath.section;
        
        if ([assessmentResponse.btnCommentSelected isEqualToString:@"0"]) {
            [assessmentResponse setIsAnswerSelected:@"1"];
            [assessmentResponse setBtnCommentSelected:@"1"];
        }
        else
        {
            [assessmentResponse setIsAnswerSelected:@"0"];
            [assessmentResponse setBtnCommentSelected:@"0"];
        }
        [cell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
        self.sectionRows =  2;
    }
    else
    {
        questionsModel = [appDelegate.arrAssessmentQuestions objectAtIndex:indexpath.section];
        selectedIndex = indexpath.section;
    
        if ([questionsModel.btnCommentSelected isEqualToString:@"0"]) {
            [questionsModel setIsAnswerSelected:@"1"];
            [questionsModel setBtnCommentSelected:@"1"];
        }
        else
        {
            [questionsModel setIsAnswerSelected:@"0"];
            [questionsModel setBtnCommentSelected:@"0"];
        }
      
        [cell.imgViewComment setImage:[UIImage imageNamed:@"VectorSmartObject15"]];
        self.sectionRows =  2;
    }
    [self.tableViewAssessment reloadData];
    
}

-(IBAction)saveButtonTaped:(id)sender
{
    subCell = (ArrivalCommentCell *)[[[[sender superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableViewAssessment indexPathForCell:subCell];
    
    subCell = (ArrivalCommentCell *)[tableViewAssessment cellForRowAtIndexPath:path];    
    if (_isReview == true)
    {
        assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:selectedIndex];
        if(subCell.txtComment.text.length>0) {
            
            // [questionsModel setIsAnswerSelected:@"1"];
            [assessmentResponse setResponseComments:subCell.txtComment.text];
            [assessmentResponse setIsAnswerSelected:@"0"];
            [assessmentResponse setBtnCommentSelected:@"0"];
        }
        else
        {
            [assessmentResponse setResponseComments:@""];
            [assessmentResponse setIsAnswerSelected:@"0"];
            [assessmentResponse setBtnCommentSelected:@"0"];
        }
    }
    else{
        questionsModel = [appDelegate.arrAssessmentQuestions objectAtIndex:selectedIndex];
        
        if(subCell.txtComment.text.length>0) {
            
            // [questionsModel setIsAnswerSelected:@"1"];
            [questionsModel setCommentText:subCell.txtComment.text];
            [questionsModel setIsAnswerSelected:@"0"];
            [questionsModel setBtnCommentSelected:@"0"];
        }
        else {
            [questionsModel setCommentText:@""];
            [questionsModel setIsAnswerSelected:@"0"];
            [questionsModel setBtnCommentSelected:@"0"];
        }
    }
    [self.tableViewAssessment reloadData];
}

-(IBAction)stopButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableViewAssessment indexPathForCell:assessmentCell];
    
    assessmentCell = (ArrivalServiceCell *)[tableViewAssessment cellForRowAtIndexPath:path];
    
    [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
    
    [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
    
    [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject17"]];
    
    [appDelegate.dicAssessmentAnswersArray setValue:@"NA" forKey:[NSString stringWithFormat:@"Question%ld",(long)path.section]];
    
    
}
-(IBAction)correctButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableViewAssessment indexPathForCell:assessmentCell];;
    
    assessmentCell = (ArrivalServiceCell *)[tableViewAssessment cellForRowAtIndexPath:path];
    
    [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject10"]];
    
    [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject9"]];
    
    [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
    
    [appDelegate.dicAssessmentAnswersArray setValue:@"YES" forKey:[NSString stringWithFormat:@"Question%ld",(long)path.section]];
    
}

-(IBAction)wrongButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview]superview] superview];
    
    NSIndexPath *path = [self.tableViewAssessment indexPathForCell:assessmentCell];
    
    assessmentCell = (ArrivalServiceCell *)[tableViewAssessment cellForRowAtIndexPath:path];
    
    [assessmentCell.imgViewWrong setImage:[UIImage imageNamed:@"VectorSmartObject16"]];
    [assessmentCell.imgViewCorrect setImage:[UIImage imageNamed:@"VectorSmartObject14"]];
    [assessmentCell.imgViewStop setImage:[UIImage imageNamed:@"VectorSmartObject8"]];
    [appDelegate.dicAssessmentAnswersArray setValue:@"NO" forKey:[NSString stringWithFormat:@"Question%ld",(long)path.section]];    
}

-(IBAction)pictureButtonTapeed:(id)sender
{
    assessmentCell = (ArrivalServiceCell *)[[[[[sender superview] superview] superview] superview] superview];
    
    NSIndexPath *path = [self.tableViewAssessment indexPathForCell:assessmentCell];
      assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:path.section];
    if (self.isReview)
    {
        if (![[appDelegate.isAssessmentImage objectForKey:[NSString stringWithFormat:@"Image%ld",(long)path.section]] isEqualToString:@""])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *imageFilePath;
            
           // imageFilePath = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@.png",[appDelegate.isAssessmentImage objectForKey:[NSString stringWithFormat:@"Image%ld",(long)path.section]]]];
            
            imageFilePath =[Utility setDocumentDirectoryPath:assessmentResponse.isImage];

            selectedURL =assessmentResponse.imageURL;
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            BOOL fileExists2 = [fileManager fileExistsAtPath:imageFilePath];
            NSLog(@"Path to file: %@", imageFilePath);
            
            if (fileExists2) {
                
                _imageViewCapturedImage.image = [UIImage imageWithContentsOfFile:imageFilePath];
                //[_viewImageCaptured setHidden:false];
                [self.mainImageView setHidden:false];

            }
            else {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                __weak ArrivalServiceViewController *weakself = self;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
                
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [weakself loadPicture:imageFilePath];
                    [MBProgressHUD hideHUDForView:weakself.view animated:YES];
                    
                });
            }
        
        }
        else  if (appDelegate.isSubmitted) {
            return;
        }
        else if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            [self takePhoto];
        }
    }else
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            [self takePhoto];
        } else
        {
            // don't show camera
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"No Camera Available." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                // Ok action example
            }];
            
            [alert addAction:okAction];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)loadPicture:(NSString*)docPath {
    
    NSData *dataImage1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:selectedURL]];
    
    self.imageViewCapturedImage.image = [UIImage imageWithData:dataImage1];
  //  [_viewImageCaptured setHidden:false];
    
    [self.mainImageView setHidden:false];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           if(dataImage1.length>0){
               [dataImage1 writeToFile:docPath atomically:YES];
               
           }
    });
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(IBAction)proceedButtonTapeed:(id)sender
{
    if (appDelegate.arrAssessmentEmployees.count != 0) {
        
  
    if (_isReview) {
        
        [self saveReviewAssessmentResponseLocally];
    }
    else {
        [appDelegate.arrAssessmentResponseMaster removeAllObjects];
        [self saveNewAssessmentResponseLocally];

    }
    appDelegate.totalYes = totalYes;
    appDelegate.totalYesNo = totalYesNo;
    // duplicates question answer
  /* for (int i = 0; i < appDelegate.arrAssessmentResponseMaster.count; i++)
    {
        assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
        for (int r = 0; r<appDelegate.allAssessmentArray.count; r++) {
            
            QuestionsModel*  questionsModel = [appDelegate.allAssessmentArray objectAtIndex:r];
            
            if ([assessmentResponse.assessmentQuestion isEqualToString:questionsModel.QuestionText]) {
                [questionsModel setResponseText:assessmentResponse.responseText];
            }
        }
    }
    
    SummaryViewController *target = [[SummaryViewController alloc]init];
    target.assessmentDataLaunchDateTime = [newDateString substringToIndex:(newDateString.length - 9)];
    [self.navigationController pushViewController:target animated:true];*/

    InspiredServicesViewController *target = [[InspiredServicesViewController alloc]init];
    target.isReview =self.isReview;
    target.newdatetime1 = newDateString;
    target.currentdatetime1 =currentdatetime;
    target.headerTitle =self.lblAssessmentName.text;
    target.assessmentIDForService =assessmentResponse.assessmentID ;
    target.subHeader =self.lblDate.text;
    
    [self.navigationController pushViewController:target animated:true];
}
    else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Please add employees before proceeding further" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            // Ok action example
        }];
        
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    selectedIndex = textField.tag;
}


#pragma mark ===Save assessment Response ===
-(void)saveReviewAssessmentResponseLocally {
    
    appDelegate.assessmentIDForQuestions = self.assessmentIDForQuestions;
    
    for (int i = 0; i < appDelegate.arrAssessmentResponseMaster.count; i++)
    {
        assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
        
        assessmentResponse.responseText = [appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%d",i]];
        
        assessmentResponse.isImage = [appDelegate.isAssessmentImage objectForKey:[NSString stringWithFormat:@"Image%d",i]];
        
        [appDelegate.arrAssessmentResponseMaster replaceObjectAtIndex:i withObject:assessmentResponse];
        
        if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"])
        {
            totalYes++;
        }
        if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"] || [[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"NO"])
        {
            totalYesNo++;
        }
    }
    
}

-(void)saveNewAssessmentResponseLocally {
    
    appDelegate.arrAssessmentResponseMaster  =[NSMutableArray new];
    for (int i = 0; i < appDelegate.dicAssessmentAnswersArray.count; i++)
    {
        
        questionsModel = [appDelegate.arrAssessmentQuestions objectAtIndex:i];
        
        assessmentResponse = [[AssessmentResponse alloc]init];
        
        assessmentResponse.responseID = [NSString stringWithFormat:@"%d",i];
        assessmentResponse.assessmentID = currentdatetime;
        assessmentResponse.QuestionID = questionsModel.QuestionID;
        
        if ([[appDelegate.dicAssessmentAnswersArray  objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@""])
        {
            assessmentResponse.responseText = @"NA";
        }else
        {
            assessmentResponse.responseText = [appDelegate.dicAssessmentAnswersArray  objectForKey:[NSString stringWithFormat:@"Question%d",i]];
        }
        assessmentResponse.assesmentName = appDelegate.assessmentName;
        assessmentResponse.assesmentDate = newDateString;
        assessmentResponse.assessmentQuestion = questionsModel.QuestionText;
        assessmentResponse.responseComments = questionsModel.commentText;
        assessmentResponse.departmentID = self.departmentName;
        assessmentResponse.sectionName = questionsModel.SectionName;
  
        assessmentResponse.isImage = [appDelegate.isAssessmentImage objectForKey:[NSString stringWithFormat:@"Image%d",i]];
        assessmentResponse.assessmenTrainer = self.assessmentTrainer;
        
        [appDelegate.arrAssessmentResponseMaster addObject:assessmentResponse];
        
        if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"])
        {
            totalYes++;
        }
        if ([[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"YES"] || [[appDelegate.dicAssessmentAnswersArray objectForKey:[NSString stringWithFormat:@"Question%d",i]] isEqualToString:@"NO"])
        {
            totalYesNo++;
        }
    }
}



-(IBAction)backButtonTapeed:(id)sender
{
    [appDelegate.dicAssessmentAnswersArray removeAllObjects];
    
    [appDelegate.isAssessmentImage removeAllObjects];
    
    [appDelegate.isAssessmentComment removeAllObjects];
    
    _isReview = false;
    
    appDelegate.isSubmitted = false;
    
   // appDelegate.isResponsesSaved = false;
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark ===Take photo methods ===
- (void)takePhoto
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:NO completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [assessmentCell.imgViewPicture setImage:[UIImage imageNamed:@"VectorSmartObject13"]];
    
    NSDate * date = [NSDate date];
    NSDateFormatter *dfTime = [NSDateFormatter new];
    [dfTime setDateFormat:@"MMddyyhhmmss a"];
    currentdatetime =[dfTime stringFromDate:date];
    currentdatetime = [currentdatetime substringToIndex:(currentdatetime.length - 3)];
    
    NSIndexPath *path = [self.tableViewAssessment indexPathForCell:assessmentCell];
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    // define your own path and storage for image
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    documentPath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",currentdatetime]];
//    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    NSData *data = UIImagePNGRepresentation([ArrivalServiceViewController getNormalizedImage:image]);
    [data writeToFile:documentPath atomically:YES];
    assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:path.section];
    
    [assessmentResponse setIsImage:currentdatetime];
    [appDelegate.isAssessmentImage setValue:currentdatetime forKey:[NSString stringWithFormat:@"Image%ld",(long)path.section]];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


+(UIImage*)getNormalizedImage:(UIImage *)rawImage {
    
    /*CGRect rect = CGRectMake(0,0,110,110);
     UIGraphicsBeginImageContext( rect.size );
     [rawImage drawInRect:rect];
     UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();*/
    
    if(rawImage.imageOrientation == UIImageOrientationUp)
        return rawImage;
    
    CGRect rect = CGRectMake(0,0,130,100);
    UIGraphicsBeginImageContext( rect.size);
    [rawImage drawInRect:rect];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return normalizedImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)deshLine{
    
    UIBezierPath * path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0.0, 10.0)];
    [path addLineToPoint:CGPointMake(self.deshView.frame.size.width, 10.0)];
    [path setLineWidth:2.0];
    CGFloat dashes[] = { path.lineWidth, path.lineWidth * 2 };
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapSquare];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.deshView.frame.size.width
                                                      , 10), false, 2);
    //  [[UIColor  colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1]setStroke];
    [[UIColor lightGrayColor]setStroke];
    
    [path stroke];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.deshView.image =image;
    
}

@end
