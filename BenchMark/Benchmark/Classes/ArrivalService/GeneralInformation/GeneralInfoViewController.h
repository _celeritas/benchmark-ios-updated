//
//  GeneralInfoViewController.h
//  Benchmark
//
//  Created by Admin on 4/30/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownListView.h"
#import "AssessmentListingModel.h"
@protocol senddataProtocol <NSObject>

-(void)sendBoolToPrevious:(NSString *)back;

@end

@interface GeneralInfoViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource,kDropDownListViewDelegate>

{
    
    DropDownListView * Dropobj;
       NSArray *arryList;
}
@property(nonatomic,strong) AssessmentListingModel *assessmentListingModelObj;

@property(strong,nonatomic) IBOutlet UIImageView *deshView;

@property (nonatomic, weak) id <senddataProtocol> senddataProtocolObject;

@property (strong, nonatomic) IBOutlet UIView *viewRoomNumber;
@property (strong, nonatomic) IBOutlet UIView *viewMealPeriod;

@property (strong, nonatomic) IBOutlet UITextField *txtRoomNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtMealPeriod;
@property (strong, nonatomic) IBOutlet UITableView *tableViewMealPeriod;

@property (strong, nonatomic) IBOutlet UIButton *btnSave;
@property (strong, nonatomic) IBOutlet UIButton *btnMealPeriod;

@property (nonatomic, assign) BOOL isReview;

@property (nonatomic, assign) NSString *assessmentID;

@end
