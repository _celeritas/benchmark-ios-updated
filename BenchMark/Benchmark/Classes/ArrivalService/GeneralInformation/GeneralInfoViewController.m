//
//  GeneralInfoViewController.m
//  Benchmark
//
//  Created by Admin on 4/30/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "GeneralInfoViewController.h"
#import "SummaryViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "GeneralInformationModel.h"
#import "Utility.h"

static sqlite3 *database = nil;

@interface GeneralInfoViewController ()
{
    NSMutableArray *generalInfoArray;
    NSString *roomNumber, *mealPeriod;
    AppDelegate *appDelegate;
}
@end

@implementation GeneralInfoViewController
@synthesize assessmentID;

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    
  //  return NO;
  
    if (action == @selector(paste:))
        return NO;
    return [super canPerformAction:action withSender:sender];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [Utility deshLineWithImageView:self.deshView];

    // Do any additional setup after loading the view from its nib.
 
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    generalInfoArray = [[NSMutableArray alloc]initWithObjects:@"Breakfast", @"Lunch", @"Dinner", nil];
    
    [self setUI];
    
    if (_isReview)
    {
       /* GeneralInformationModel *obj =[GeneralInformationModel new];
        obj=[obj getGeneralInfomationData:assessmentID withController:self];
      */
      
        _txtRoomNumber.text = [self.assessmentListingModelObj.assessmentRoomNO isEqualToString:@"NA"]?  @"" : self.assessmentListingModelObj.assessmentRoomNO;
        _txtMealPeriod.text = [self.assessmentListingModelObj.assessmentMeal isEqualToString:@"NA"]?  @"" : self.assessmentListingModelObj.assessmentMeal;
        
        appDelegate.serverRomeInfoArray = [NSMutableArray new];

      //  [appDelegate.serverRomeInfoArray addObject:[Utility roomGeneralInformationWithAssessID:assessmentID  withRoomNo:_txtRoomNumber.text withMeal:_txtMealPeriod.text]];
        
        //disable button
        if (appDelegate.isSubmitted)
        {
            [_txtRoomNumber setEnabled:false];
            [_btnMealPeriod setEnabled:false];
            [_btnSave setEnabled:false];
        }
    }
}

-(void)resizeTableview:(UITableView *)tb withNumberOfRows:(NSUInteger)arrayCount{
    
  /*  [tb beginUpdates];
    CGRect tFrame = tb.frame;
    tFrame.size.height = MIN(tb.contentSize.height ,arrayCount*44);
    tb.frame = tFrame;
    [tb endUpdates];*/
    
}

-(void)setUI {
   
    if (appDelegate.isSubmitted) {

        self.btnSave.userInteractionEnabled =false;
        self.btnSave.alpha= .85;
    }
    _tableViewMealPeriod.layer.borderColor =  [UIColor colorWithRed:130.0/255.0 green:60.0/255.0 blue:148.0/255.0 alpha:1].CGColor;
    _tableViewMealPeriod.layer.borderWidth = 1;
    
    self.btnSave.layer.cornerRadius = 20;
    [self setBorderOfUIView:self.viewRoomNumber];
    [self setBorderOfUIView:self.viewMealPeriod];

}

-(void)setBorderOfUIView:(UIView*)view {
    
    view.backgroundColor = [UIColor clearColor];
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 5;
    view.layer.borderColor = [UIColor colorWithRed:130.0/255.0 green:60.0/255.0 blue:148.0/255.0 alpha:1].CGColor;
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)saveIntoGeneralInfomation {
    
    appDelegate.serverRomeInfoArray = [NSMutableArray new];
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [appDelegate.serverRomeInfoArray addObject:[Utility roomGeneralInformationWithAssessID:assessmentID  withRoomNo:_txtRoomNumber.text withMeal:_txtMealPeriod.text]];
    
  /*  NSString *query = [NSString stringWithFormat:@"insert into GeneralInformation (AssessmentID, RoomNumber, MealPeriod) Values('%@','%@','%@')",assessmentID, _txtRoomNumber.text,_txtMealPeriod.text];
    
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(selectstmt) == SQLITE_DONE)
            {
                NSLog(@"DB SUPPORT - GeneralInformation INSERTED");
               [appDelegate.serverRomeInfoArray addObject:[Utility roomGeneralInformationWithAssessID:assessmentID  withRoomNo:_txtRoomNumber.text withMeal:_txtMealPeriod.text]];
            }
            else
            {
                NSLog(@"DB SUPPORT - ERROR GeneralInformation INSERT");
            }
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);*/
    [MBProgressHUD hideHUDForView:self.view animated:true];
}

-(void)deleteDataWithAssessmentID:(NSString*)assessID{
    
    sqlite3_stmt *selectstmt;
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from GeneralInformation where AssessmentID ='%@'",assessID];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &selectstmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(selectstmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)updateIntoGeneralInfomation:(NSString *)iD
{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    
    NSString *query = [NSString stringWithFormat:@"update GeneralInformation SET RoomNumber = '%@', MealPeriod = '%@' where AssessmentID = '%@'", _txtRoomNumber.text, _txtMealPeriod.text, iD];
    
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(selectstmt) == SQLITE_DONE)
            {
                NSLog(@"DB SUPPORT - GeneralInformation UPDATED");
            }
            else
            {
                NSLog(@"DB SUPPORT - ERROR GeneralInformation UPDATED");
            }
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
    [MBProgressHUD hideHUDForView:self.view animated:true];
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    
    if (number)
    {
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        return newLength <= 10 || returnKey;
        
    }
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
     [Dropobj fadeOut];

    return true;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return generalInfoArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    [tableView setSeparatorInset:UIEdgeInsetsZero];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    [tableView setSeparatorInset:UIEdgeInsetsZero];
    
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.text = [generalInfoArray objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _txtMealPeriod.text = [generalInfoArray objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [tableView setHidden:true];
}

-(IBAction)btnMealPeriodPressed
{
  
    [self.view endEditing:YES];
    arryList =generalInfoArray;
    [Dropobj fadeOut];
    CGRect f = self.viewMealPeriod.frame;
    [self showPopUpWithTitle:@"Select Meal Period" withOption:arryList xy:CGPointMake(15,  f.origin.y + self.viewMealPeriod.frame.size.height) size:CGSizeMake(self.view.frame.size.width-30, 230) isMultiple:NO];

}

-(IBAction)btnSavePressed
{
    if ([_txtMealPeriod.text isEqualToString:@""] || [_txtRoomNumber.text isEqualToString:@""])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"All fields are required to proceed." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        appDelegate.assessmentRoomNO = self.txtRoomNumber.text;
        appDelegate.assessmentMeal = self.txtMealPeriod.text;;

        [appDelegate.serverRomeInfoArray removeAllObjects];
   
        GeneralInformationModel *obj =[GeneralInformationModel new];
        [obj setGeneralInformationDataToLocalDB:[Utility getDBPath] withArray:[self setObjectForGeneralInfo]];
        
        [_senddataProtocolObject sendBoolToPrevious:@"true"];
        [self.navigationController popViewControllerAnimated:true];
        
    }
}
-(NSMutableArray *)setObjectForGeneralInfo {
    NSMutableArray *giArray = [NSMutableArray new];

    GeneralInformationModel *obj = [[GeneralInformationModel alloc]init];

    obj.gAssessID = assessmentID;
    obj.gRoomNo = self.txtRoomNumber.text;
    obj.gMeal = self.txtMealPeriod.text;
    [giArray addObject:obj];
    return giArray;
}

-(IBAction)btnBackPressed
{
    [self.navigationController popViewControllerAnimated:true];
}


-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDown_R:130.0 G:60.0 B:148.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
     //   _lblProperty.text=[arryList objectAtIndex:anIndex];
         _txtMealPeriod.text = [generalInfoArray objectAtIndex:anIndex];
    
}

- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        
        
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}



@end
