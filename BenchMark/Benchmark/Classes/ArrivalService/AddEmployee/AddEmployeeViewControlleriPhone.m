//
//  AddEmployeeViewControlleriPhone.m
//  Peninsula
//
//  Created by Mac on 1/23/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import "AddEmployeeViewControlleriPhone.h"

#import "Utility.h"
#import "AppDelegate.h"
#import "EmployeeModel.h"
#import "EmployeesCell.h"
@interface AddEmployeeViewControlleriPhone ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak,nonatomic) IBOutlet UITableView *tableView;
@property (weak,nonatomic) IBOutlet UITextField *txtEmployee;
@property (weak,nonatomic) IBOutlet UIButton *btnAdd;

@end

@implementation AddEmployeeViewControlleriPhone {
    
    AppDelegate *appDelegate;
    NSMutableArray *selectedEmployees;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
}

/*- (void)textPasteConfigurationSupporting:(id<UITextPasteConfigurationSupporting>)textPasteConfigurationSupporting
                      transformPasteItem:(id<UITextPasteItem>)item {
    
    NSLog(@"%@",item);
}*/
-(void)viewWillAppear:(BOOL)animated {
    
    selectedEmployees = [NSMutableArray new];

}

-(void)setUI {
    
    appDelegate =(AppDelegate*) [[UIApplication sharedApplication]delegate];
   if (appDelegate.isSubmitted) {
       
        self.btnAdd.userInteractionEnabled =false;
        self.btnAdd.alpha= .85;
    }

    self.tableView.estimatedRowHeight = 55.0;
    self.tableView.rowHeight =UITableViewAutomaticDimension;
    self.txtEmployee.layer.borderWidth =1.0;
    self.txtEmployee.layer.borderColor = [UIColor colorWithRed:130.0/255.0 green:60.0/255.0 blue:148.0/255.0 alpha:1].CGColor;
    self.txtEmployee.layer.cornerRadius = 5.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
     self.txtEmployee.leftView = paddingView;
     self.txtEmployee.leftViewMode = UITextFieldViewModeAlways;
    
    self.tableView.layer.borderColor = [UIColor colorWithRed:130.0/255.0 green:60.0/255.0 blue:148.0/255.0 alpha:1].CGColor;
   [self configureTableViewCell];
    
}

-(void)configureTableViewCell {
    
    [_tableView registerNib:[UINib nibWithNibName:@"EmployeesCell"
                                           bundle:nil] forCellReuseIdentifier:@"EmployeesCell"];
    
}


#pragma mark ===UITableview Datasource/Delegates ===
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return appDelegate.arrAssessmentEmployees.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    EmployeesCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"EmployeesCell"];

    cell.lblTitle.text= [appDelegate.arrAssessmentEmployees objectAtIndex:indexPath.row];
    cell.btnDelete.tag =[indexPath row];
    
    if (appDelegate.isSubmitted) {
        cell.btnDelete.hidden =true;
    }
     [cell.btnDelete addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    

    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return UITableViewAutomaticDimension;
}

-(IBAction)deleteButtonTapped:(id)sender {
    

    NSString *empName =  [appDelegate.arrAssessmentEmployees objectAtIndex:[sender tag]] ;
    [appDelegate.arrAssessmentEmployees removeObject:empName];
    [self.tableView reloadData];
    [self showAlertWithMessage:@"Employee deleted successfully."];

}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark ===UIButton Action Method ===
-(IBAction)addButtonPressed {

    if (self.txtEmployee.text.length>0) {
        
        
        if (![appDelegate.arrAssessmentEmployees containsObject: self.txtEmployee.text]) {
            EmployeeModel *obj =[EmployeeModel new];
            obj.empName=self.txtEmployee.text;
            [appDelegate.arrAssessmentEmployees addObject:self.txtEmployee.text];
            self.txtEmployee.text =@"";
            [self.tableView reloadData];
        }
        else {
            [self showAlertWithMessage:@"Employee already added with this name."];
        }
    }
    
    else {
        [self showAlertWithMessage:@"Please provide required information"];


    }
}

-(void)showAlertWithMessage:(NSString *)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(IBAction)backButtonPressed{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSLog(@"space:%@",textField.text);
    if (textField.text.length == 0) {

    if ([string isEqualToString:@" "]){
       
        return NO;
    }
        
    }
   
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    return newLength <= 50 || returnKey;
    

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
