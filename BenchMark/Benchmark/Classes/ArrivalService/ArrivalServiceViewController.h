//
//  ArrivalServiceViewController.h
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sqlite3.h"
#import "AssessmentListingModel.h"

@interface ArrivalServiceViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate>


@property(strong,nonatomic) IBOutlet UITableView *tableViewAssessment;

@property(strong,nonatomic) IBOutlet UILabel *lblAssessmentName;
@property(strong,nonatomic) IBOutlet UIButton *btnProceed;
@property(strong,nonatomic) IBOutlet UIButton *btnGeneralInformation;
@property(strong,nonatomic) IBOutlet UILabel *lblDate;
@property(nonatomic) NSInteger sectionRows;
@property (nonatomic, assign) BOOL isReview;
@property(retain) NSString *assessmentIDForQuestions;
@property(retain) NSString *assessmentTrainer;
@property(retain) NSString *assessmentName;
@property(retain) NSString *departmentName;
@property(retain) NSString *assessmentResponceID;
@property(retain) NSString *rQuery;


@property(nonatomic,strong) NSArray *items;
@property (nonatomic, retain) NSMutableArray *itemsInTable;
@property(strong,nonatomic) IBOutlet UIImageView *deshView;

@property(strong,nonatomic) IBOutlet UIView *mainImageView;
@property(nonatomic,strong) AssessmentListingModel *assessmentListingModelObj;

//@property(strong,nonatomic) IBOutlet UIImageView *imgViewStop;
//@property(strong,nonatomic) IBOutlet UIImageView *imgViewCorrect;
//@property(strong,nonatomic) IBOutlet UIImageView *imgViewWrong;
//@property(strong,nonatomic) IBOutlet UIImageView *imgViewComment;
//@property(strong,nonatomic) IBOutlet UIImageView *imgViewPicture;

@end
