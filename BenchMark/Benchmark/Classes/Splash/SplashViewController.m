//
//  SplashViewController.m
//  BENCHMARK
//
//  Created by Admin on 4/19/18.
//  Copyright © 2018 CS. All rights reserved.
//

#import "SplashViewController.h"
#import "LoginViewController.h"
#import "DashboardViewController.h"

@interface SplashViewController ()
{
    NSTimer *myTimer;
}
@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    NSString *username = [userDefault valueForKey:@"UserName"];
    if (username != nil && username.length > 0) {
        DashboardViewController *dashBoard = [[DashboardViewController alloc]init];
        [self.navigationController pushViewController:dashBoard animated:true];
    } else {
        [self performSelector:@selector(loadingNextView)
                   withObject:nil afterDelay:5.0f];
    }

   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadingNextView
{
    
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    [self.navigationController pushViewController:loginVC animated:true];
}
@end
