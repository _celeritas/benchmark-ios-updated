//
//  SummaryViewController.h
//  Benchmark
//
//  Created by Html on 20/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "sqlite3.h"
#import "DBManager.h"
#import <CoreText/CoreText.h>
#import "CustomUILabel.h"
#import <AVFoundation/AVFoundation.h>
#import <AWSS3/AWSS3.h>
@interface SummaryViewController : UIViewController<MFMailComposeViewControllerDelegate,UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) AmazonS3Client *s3;

@property(strong,nonatomic) IBOutlet CustomUILabel *lblTrainerName;
@property(strong,nonatomic) IBOutlet UIImageView *imgView;
@property(strong,nonatomic) IBOutlet UILabel *lblHeader;
@property(strong,nonatomic) IBOutlet UIView    *headerView;

@property(strong,nonatomic) IBOutlet UIView    *generalInfoView;

@property(strong,nonatomic) IBOutlet UITableView    *pdfTableView;
@property(strong,nonatomic) IBOutlet UITableView    *summaryTableView;
//@property(strong,nonatomic) IBOutlet UITableView    *responseTableView;
//@property(strong,nonatomic) IBOutlet UITableView    *inspiredServicesTableView;
//@property(strong,nonatomic) IBOutlet UITableView    *summarytwoTableView;
@property(strong,nonatomic) IBOutlet UILabel *lblAssessmentName;

@property(strong,nonatomic) IBOutlet UIButton       *btnSaved;
@property(strong,nonatomic) IBOutlet UIButton       *btnSubmit;

@property(strong,nonatomic) IBOutlet UILabel *lblScore;
@property(strong,nonatomic) IBOutlet UILabel *lblInspiredService;
@property(strong,nonatomic) IBOutlet UILabel *lblRoles;

@property(strong,nonatomic) NSMutableDictionary *isAssessmentImageDic;
@property(strong,nonatomic) NSString *webviewHTML;

@property(assign)BOOL isUpdated;
-(IBAction)backButtonTapeed:(id)sender;
-(IBAction)emailButtonTapeed:(id)sender;
-(IBAction)savedButtonTapeed:(id)sender;
-(IBAction)submitButtonTapeed:(id)sender;

@property (strong, nonatomic) NSString *score;
@property (strong, nonatomic) NSString *trainerName;
@property (strong, nonatomic) NSString *assessmentDataLaunchDateTime;

@property (nonatomic, strong) DBManager *dbManager;
@property (assign) float servicetTotalYesNo;
@property (assign) float serviceTotalYes;
@property(strong,nonatomic) IBOutlet UIView *topView;
@property(strong,nonatomic) IBOutlet UIView *bottomView;
@property(strong,nonatomic) IBOutlet UIImageView *deshView;

@property (nonatomic, strong) NSMutableArray *arrClassificaiton;

@end
