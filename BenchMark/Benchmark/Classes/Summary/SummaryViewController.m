//
//  SummaryViewController.m
//  Benchmark
//
//  Created by Html on 20/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//
#import <MBProgressHUD/MBProgressHUD.h>
#import "SummaryViewController.h"
#import "SummaryCell.h"
#import "CustomTableViewCell.h"
#import "AppDelegate.h"
#import "ClassificationModel.h"
#import "AssessmentResponse.h"
#import "AssessmentListingModel.h"
#import "DashboardViewController.h"
#import "QuestionsModel.h"
#import "UIPrintPageRenderer+PDF.h"
#import "Utility.h"
#import "BMHApis.h"
#import "GeneralInformationModel.h"
#import <AWSRuntime/AWSRuntime.h>
#import <AWSS3/AWSS3.h>

#define kPaperSizeA4 CGSizeMake(595.2,841.8)

#define CELL_LABEL_HEIGHT 40
#define SECTION_HEIGHT 25
#define SCORE_WIDTH 60
#define IMAGEVIEW_WIDTH 130
#define IMAGEVIEW_HEIGHT 100


#define CELL_TABLECELLL_HEIGHT 60
#define LABEL_LEADING 8
#define TABLEVIEW_LEADING 0
#define FONT_SIZE 18
#define FONT_NAME @"Roboto-Bold"
#define SCREEN_WIDTH    [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT   [[UIScreen mainScreen] bounds].size.height


static sqlite3 *database = nil;

@interface SummaryViewController () <UIWebViewDelegate> {
    
    UIView *view;
    AppDelegate *appDelegate;
    float countYESNO;
    float countYES;
    double finalScore;
    NSMutableArray *scoreClassification;
    ClassificationModel * classificationModel;
    
    NSString* documentDirectoryFilename;
    NSString* documentDirectory;
    NSData *pdfContentData ;
    NSString*pathToSingleItemHTMLTemplate,*pathToInvoiceHTMLTemplate, *itemHTMLContent;
    UIFont *fontPager;
    int  pageNumber;
    NSString *pdfPath,*fileName,*roomNumber,*mealType, *cat1Score, *cat2Score, *cat3Score, *cat4Score, *emailbodyText, *subjectText ;
}

@property(strong,nonatomic)IBOutlet UIWebView *webView;

@end

@implementation SummaryViewController
@synthesize arrClassificaiton;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    pageNumber=1;
    self.lblHeader.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f];
    [self.webView setHidden:YES];
    
    // id item = [appDelegate.serverRomeInfoArray  objectAtIndex:0];
    // NSDictionary *jsonDict = (NSDictionary *) item;
    
    roomNumber= appDelegate.assessmentRoomNO.length == 0 ? @"NA" : appDelegate.assessmentRoomNO ;
    mealType = appDelegate.assessmentMeal.length == 0 ? @"NA" : appDelegate.assessmentMeal ;
    
    fontPager = [UIFont fontWithName:@"Roboto-Regular" size:14];
    // Do any additional setup after loading the view from its nib.
    [self deshLine];
    [self initialization];
    [self configureTableViewCell];
    if (appDelegate.assessmentRoles.length >0) {
        self.lblRoles.text = appDelegate.assessmentRoles;
        self.lblRoles.text=  [self.lblRoles.text stringByReplacingOccurrencesOfString:@"," withString:@", "];
        
    }
    
    if (appDelegate.isReview)
    {
        if(appDelegate.isSubmitted)
        {
            [_btnSubmit setTitle:@"Submitted" forState:UIControlStateNormal];
            _btnSaved.userInteractionEnabled = false;
            _btnSubmit.userInteractionEnabled = false;
            
            _btnSubmit.alpha =0.3;
            _btnSaved.alpha =0.3;
        }else
        {
            [_btnSaved setTitle:@"Saved" forState:UIControlStateNormal];
        }
    } else
    {
        [_btnSaved setTitle:@"Save" forState:UIControlStateNormal];
    }
    
    _lblAssessmentName.text = [appDelegate.assessmentName uppercaseString];
    _lblInspiredService.text = @"0";
    
    self.btnSaved.layer.borderWidth = 0.0f;
    self.btnSaved.layer.cornerRadius = 20;
    
    self.btnSubmit.layer.borderWidth = 0.0f;
    self.btnSubmit.layer.cornerRadius = 20;
    
    
    //total score result
    double score = appDelegate.totalYes / appDelegate.totalYesNo;
    
    NSString *totalScore;
    
    if (score > 0) {
        totalScore = [NSString stringWithFormat:@"%0.f",score * 100];
    } else
    {
        totalScore = [NSString stringWithFormat:@"0"];
    }
    _score = totalScore;
    
    totalScore = [totalScore stringByAppendingString:@"%"];
    _lblScore.text = totalScore;
    
    // inspired service result
    double score1 = self.serviceTotalYes / self.servicetTotalYesNo;
    NSString *totalScore1;
    if (score1 > 0)
    {
        totalScore1 = [NSString stringWithFormat:@"%0.f",score1 * 100];
    } else {
        totalScore1 = [NSString stringWithFormat:@"0"];
    }
    
    _lblInspiredService.text = [totalScore1 stringByAppendingString:@"%"];
    // [self calculateScoreAccordingToCategories:[appDelegate getDBPath]];
    
    [self getClassficationData:[appDelegate getDBPath]];
    
}

-(void)configureTableViewCell {
    
    [self.summaryTableView registerNib:[UINib nibWithNibName:@"SummaryCell"
                                                      bundle:nil] forCellReuseIdentifier:@"SummaryCell"];
}

-(void)initialization {
    
    scoreClassification = [[NSMutableArray alloc]init];
    arrClassificaiton = [[NSMutableArray alloc]init];
    
    countYES = 0;
    countYESNO = 0;
    
}

#pragma -mark UITableView-Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrClassificaiton.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    classificationModel = [arrClassificaiton objectAtIndex:indexPath.row];
    // Create an instance of ItemCell
    SummaryCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"SummaryCell"];
    
    cell.mainUIView.layer.borderWidth = 0.0f;
    cell.mainUIView.layer.cornerRadius = 5.0f;
    
    cell.lblTitle.text = classificationModel.classificationName;
    cell.lblScore.text = classificationModel.classficationScore;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _summaryTableView)
    {
        return 60.0f;
    }
    else
    {
        return UITableViewAutomaticDimension;
    }
}

#pragma -mark UITableView-Delegates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(void)sendInEmail {
    
    
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        
        // his class should be the delegate of the mc
        mc.mailComposeDelegate = self;
        
        [mc setSubject:subjectText];
        
        
        // set some basic plain text as the message body ... but you do not need to do this :)
        [mc setMessageBody:emailbodyText isHTML:YES];
        
        // set some recipients ... but you do not need to do this :)
        [mc setToRecipients:[NSArray arrayWithObjects:@"", nil]];
        
        pdfContentData= [NSData dataWithContentsOfFile: documentDirectoryFilename];
        
        [mc addAttachmentData:pdfContentData mimeType:@"application/pdf" fileName:fileName.uppercaseString];
        
        // displaying our modal view controller on the screen with standard transition
        [self presentViewController:mc animated:YES completion:nil];
    }
    else {
        
        UIAlertController *accountAlert =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                             message:@"Please setup mail account."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil];
        
        [accountAlert addAction:btnOk];
        
        [self presentViewController:accountAlert animated:YES completion:nil];
        
    }
    
}

- (void)removeImage:(NSString *)filename {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller    didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [view removeFromSuperview];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            // [self removeImage:[NSString stringWithFormat:@"%@:%@-%@",appDelegate.assessmentPropertyName,appDelegate.assessmentName,_assessmentDataLaunchDateTime]];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
//////


#pragma mark ===Update data to server ===
-(void)updateInspiredservicAssessmentResponseMaster {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    
    for (int i  = 0; i < appDelegate.inspiredServicesResponseMaster.count; i++) {
        
        AssessmentResponse *assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:i];
        
        // Prepare the query string.
        NSString *query;
        
        if (assessmentResponse.responseComments.length>0 && assessmentResponse.isImage.length==0) {
            
            query =  [NSString stringWithFormat:@"update InspiredServiceAssessmentResponseMaster set ResponseText = '%@',ResponseComment = '%@' where ResponseID = %ld", assessmentResponse.responseText, assessmentResponse.responseComments,(long)assessmentResponse.responseID];
        }
        else  if (assessmentResponse.isImage.length>0 && assessmentResponse.responseComments.length==0) {
            
            query =  [NSString stringWithFormat:@"update InspiredServiceAssessmentResponseMaster set ResponseText = '%@',ImageData = '%@' where ResponseID = %ld", assessmentResponse.responseText, assessmentResponse.isImage,(long)assessmentResponse.responseID];
        }
        else   if (assessmentResponse.responseComments.length>0 && assessmentResponse.isImage.length>0) {
            
            query =  [NSString stringWithFormat:@"update InspiredServiceAssessmentResponseMaster set ResponseText = '%@',ResponseComment = '%@',ImageData = '%@' where ResponseID = %ld", assessmentResponse.responseText, assessmentResponse.responseComments,assessmentResponse.isImage,(long)assessmentResponse.responseID];
        }
        else {
            query =  [NSString stringWithFormat:@"update InspiredServiceAssessmentResponseMaster set ResponseText = '%@' where ResponseID = %ld", assessmentResponse.responseText, (long)assessmentResponse.responseID];
        }
        
        const char *sql =[query UTF8String];
        
        sqlite3_stmt *selectstmt;
        
        if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
        {
            if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(selectstmt) == SQLITE_DONE) {
                    
                    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) != SQLITE_OK)
                    {
                        NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
                    }
                }
                else
                {
                    NSLog(@"DB SUPPORT - ERROR AssessmentResponseMaster UPDATED");
                }
            }
            sqlite3_finalize(selectstmt);
            
        }
        sqlite3_close(database);
        
    }
    
}


-(void)restValues{
    
    [appDelegate.dicAssessmentAnswersArray removeAllObjects];
    [appDelegate.isAssessmentImage removeAllObjects];
    [appDelegate.isAssessmentComment removeAllObjects];
    appDelegate.assessmentPropertyName = @"";
    appDelegate.assessmentIDForQuestions = @"";
    appDelegate.assessmentName = @"";
    [MBProgressHUD hideHUDForView:self.view animated:true];
    //  appDelegate.isResponsesSaved = false;
    appDelegate.isSubmitted = false;
    [MBProgressHUD hideHUDForView:self.view animated:true];
    // DashboardViewController *target = [[DashboardViewController alloc]init];
    //  [self.navigationController pushViewController:target animated:true];
}

-(void)setValuesInArrayWithUpdatedStatus:(NSString *)status {
    
    appDelegate.serverResponseArray = [NSMutableArray new];
    for (AssessmentResponse *assessmentResponse in appDelegate.arrAssessmentResponseMaster) {
        [appDelegate.serverResponseArray addObject:[Utility updateAssessmentResponseMasterObject:assessmentResponse]];
    }
    //Inspired
    appDelegate.serverInspiredResponseArray = [NSMutableArray new];
    
    for (AssessmentResponse *assessmentResponse in appDelegate.inspiredServicesResponseMaster) {
        [appDelegate.serverInspiredResponseArray addObject:[Utility updateInspiredServiceResponseMasterObject:assessmentResponse]];
    }
    //listing
    NSMutableArray *listingArray =  [self setObjectForAssessmentListingWithScore:_score withStatus:status];
    appDelegate.serverListingArray = [NSMutableArray new];
    
    for (AssessmentListingModel *object in listingArray) {
        [appDelegate.serverListingArray  addObject:[Utility updateAssessmentListingDataWithModel:object withCatScore1:cat1Score withCatScore2:cat2Score withCatScore3:cat3Score withCatScore4:cat4Score]];
    }
}

-(void)setValuesInArrayWithStatus:(NSString *)status {
    
    appDelegate.serverResponseArray = [NSMutableArray new];
    for (AssessmentResponse *assessmentResponse in appDelegate.arrAssessmentResponseMaster) {
        [appDelegate.serverResponseArray addObject:[Utility assessmentResponseMasterObject:assessmentResponse]];
    }
    //Inspired
    appDelegate.serverInspiredResponseArray = [NSMutableArray new];
    
    for (AssessmentResponse *assessmentResponse in appDelegate.inspiredServicesResponseMaster) {
        [appDelegate.serverInspiredResponseArray addObject:[Utility inspiredServiceResponseMasterObject:assessmentResponse]];
    }
    //listing
    NSMutableArray *listingArray =  [self setObjectForAssessmentListingWithScore:_score withStatus:status];
    appDelegate.serverListingArray = [NSMutableArray new];
    //rosh
    for (AssessmentListingModel *object in listingArray) {
        [appDelegate.serverListingArray  addObject:[Utility assessmentListingDataWithModel:object withCatScore1:cat1Score withCatScore2:cat2Score withCatScore3:cat3Score withCatScore4:cat4Score]];
    }
}
-(void)sendPictureToAWS {
    for (int i = 0; i <appDelegate.arrAssessmentResponseMaster.count; i++) {
        
        AssessmentResponse *assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
        if (assessmentResponse.isImage.length>0 && ![assessmentResponse.isImage isEqualToString:@""]) {
            
            NSData *data = [self getImageData:assessmentResponse.isImage];
            if (data.length>0) {
                [self processBackgroundThreadUploadInBackgroundPhoto:[self getImageData:assessmentResponse.isImage] withName:assessmentResponse.isImage];
            }
            
        }
    }
    
    
    //send Inspired Image  at aws
    for (int i = 0; i <appDelegate.inspiredServicesResponseMaster.count; i++) {
        
        AssessmentResponse *assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:i];
        if (assessmentResponse.isImage.length>0 && ![assessmentResponse.isImage isEqualToString:@""]) {
            [self processBackgroundThreadUploadInBackgroundPhoto:[self getImageData:assessmentResponse.isImage] withName:assessmentResponse.isImage];
        }
    }
    
}
-(void)saveDataIntoDB {
    
    if (appDelegate.isResponsesSaved)
    {
        // update data in Local db
        /*  AssessmentResponse *responseObj = [AssessmentResponse new];
         [responseObj updateIntoAssessmentResponseMaster];
         AssessmentListingModel *listObj =[AssessmentListingModel new];
         [listObj updateIntoAssessmentListing:@"Saved":_score];
         [self updateInspiredservicAssessmentResponseMaster];
         */
        
        
        //new Response not updating locally
        [self setValuesInArrayWithUpdatedStatus:@"Saved"];
        //send Arrival Service iamge at aws
        [self sendPictureToAWS];
        dispatch_async(dispatch_get_main_queue(),^(void){
            
            [self updateDataAtSever];
            
        });
        
    }
    else
    {//here
        
        //save data in local db
        /*AssessmentResponse * obj =[AssessmentResponse new];
         [obj setAssessmentResponceDataToLocalDB:[Utility getDBPath] withArray: appDelegate.arrAssessmentResponseMaster];
         [obj setInspiredServiceDataToLocalDB:[Utility getDBPath] withArray: appDelegate.inspiredServicesResponseMaster];
         AssessmentListingModel *assessObj = [AssessmentListingModel new];
         
         [assessObj setAssessmentLsitingDataToLocalDB:[Utility getDBPath] withArray: [self setObjectForAssessmentListingWithScore:_score withStatus:@"Saved"]];*/
        
        //new Response not saing locally
        [self setValuesInArrayWithStatus:@"Saved"];
        [self sendPictureToAWS];
        dispatch_async(dispatch_get_main_queue(),^(void){
            // [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [self sendDataToServer];
            
        });
    }
    
}

-(NSMutableArray *)setObjectForAssessmentListingWithScore:(NSString*)score withStatus:(NSString*)status {
    NSMutableArray *assessArray = [NSMutableArray new];
    NSDate * date = [NSDate date];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    
    NSString *assessmentSubmittedDateTime = [dateFormatter stringFromDate:date];
    
    AssessmentListingModel *assessmentListing = [[AssessmentListingModel alloc]init];
    AssessmentResponse *assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:0];
    assessmentListing.ID =  [NSString stringWithFormat:@"%ld",(long)appDelegate.indexValue];
    assessmentListing.departmentName = assessmentResponse.departmentID;
    assessmentListing.assessmentID = assessmentResponse.assessmentID;
    assessmentListing.assessmentDataLaunchDateTime = assessmentResponse.assesmentDate;
    assessmentListing.assessmentSubmittedDateTime = assessmentSubmittedDateTime;
    assessmentListing.assessmentTrainer = assessmentResponse.assessmenTrainer;
    assessmentListing.assessmentScore = score;
    assessmentListing.assessmentCurrentStatus = status;
    assessmentListing.assessmentCommnents = @"";
    assessmentListing.assessmentNameID = appDelegate.assessmentIDForQuestions;
    assessmentListing.assessmentName = appDelegate.assessmentName;
    assessmentListing.assessmentPropertyName = appDelegate.assessmentPropertyName;
    assessmentListing.assessmentRoles = appDelegate.assessmentRoles ;
    assessmentListing.assessmentEmployees = [appDelegate.arrAssessmentEmployees componentsJoinedByString:@","];
    
    if (appDelegate.assessmentRoomNO .length == 0 || appDelegate.assessmentMeal.length == 0) {
        appDelegate.assessmentRoomNO = @"NA";
        appDelegate.assessmentMeal = @"NA";;
    }
    assessmentListing.assessmentRoomNO = appDelegate.assessmentRoomNO ;
    
    assessmentListing.assessmentMeal = appDelegate.assessmentMeal ;
    
    
    [assessArray addObject:assessmentListing];
    return assessArray;
}

-(void)updateDataAtSever {
    
    NSDictionary *parameters = [NSDictionary new];
    // if(appDelegate.serverRomeInfoArray.count == 0){
    parameters =
    @{@"AssessmentListingData":appDelegate.serverListingArray,
      @"AssessmentResponseMasterData" : appDelegate.serverResponseArray,
      @"InspiredServiceAssessmentResponseMasterData":appDelegate.serverInspiredResponseArray
      };
    
    BMHApis *obj =[BMHApis new];
    [obj requestToUpdateAssessemntDataToServerWithParameters:parameters ViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        
        if (success) {
            
            //  statuCode = 1;
            BOOL successR = [[result valueForKey:@"statusCode"]boolValue];
            if (successR) {
                [appDelegate.dicAssessmentAnswersArray removeAllObjects];
                [appDelegate.isAssessmentImage removeAllObjects];
                [appDelegate.isAssessmentComment removeAllObjects];
                [self restValues];
            }
            
            dispatch_async(dispatch_get_main_queue() , ^(void){
                [self getAssessmentDataFromServer];
            });
            
        }
        else {
            NSLog(@"something went wrong");
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
}

#pragma mark ===save data to server ===
-(void)sendDataToServer {
    NSDictionary *parameters = [NSDictionary new];
    //  if(appDelegate.serverRomeInfoArray.count == 0){
    parameters =
    @{@"AssessmentListingData":appDelegate.serverListingArray,
      @"AssessmentResponseMasterData" : appDelegate.serverResponseArray,
      @"InspiredServiceAssessmentResponseMasterData":appDelegate.serverInspiredResponseArray
      };
    
    BMHApis *obj =[BMHApis new];
    [obj requestForSaveDataToServerWithParameter:parameters ViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        if (success) {
            
            
            BOOL successR = [[result valueForKey:@"statusCode"]boolValue];
            if (successR) {
                [appDelegate.dicAssessmentAnswersArray removeAllObjects];
                [appDelegate.isAssessmentImage removeAllObjects];
                [appDelegate.isAssessmentComment removeAllObjects];
                appDelegate.assessmentPropertyName = @"";
                appDelegate.assessmentIDForQuestions = @"";
                appDelegate.assessmentName = @"";
            }
            
            /*else {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             }*/
            dispatch_async(dispatch_get_main_queue() , ^(void){
                [self getAssessmentDataFromServer];
            });
            
        }
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)getAssessmentDataFromServer {
    
    //[MBProgressHUD showHUDAddedTo:self.view animated:true];
    
    BMHApis *obj =[BMHApis new];
    [obj requestForGetAssessmentDataFromServerWithViewController:self WithSuccessBlock:^(BOOL success, NSDictionary *result) {
        NSDictionary *dic = (NSDictionary *)result;
        
        BOOL status = [[dic valueForKey:@"statusCode"]boolValue];
        if (status) {
            AssessmentListingModel * obj =[AssessmentListingModel new];
            [obj   loadAssessmentLisingDataFromJSON:result];
            AssessmentResponse *resObj =[AssessmentResponse new];
            [resObj loadAssessmentResponseDataFromJSON:result];
            [resObj loadInspriedServiceDataFromJSON:result];
            //  GeneralInformationModel *gObj = [GeneralInformationModel new];
            //  [gObj loadGeneralInformationDataFromJSON:result];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            DashboardViewController *target = [[DashboardViewController alloc]init];
            [self.navigationController pushViewController:target animated:true];
        });
        
        
    } failure:^(NSError *error, NSString *message) {
        [MBProgressHUD hideHUDForView:self.view animated:true];
    }];
}

-(void)drawHeaderFooter:(NSInteger)pageNmbr {
    // Header
    float widthHeader = kPaperSizeA4.width;
    float heightHeader =100;
    float xHeader = 0;
    float yHeader = 0;
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
    
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    [viewH setBackgroundColor:[UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f]];
    [viewH drawRect:imageHeader];
    
    [viewH addSubview:self.headerView];
    
    UIGraphicsBeginImageContextWithOptions(viewH.frame.size, NO, 0.0);
    struct CGContext *context = UIGraphicsGetCurrentContext();
    [viewH.layer renderInContext:context];
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [snapshotImageFromMyView drawInRect:imageHeader];
    
    
    // Pager
    //    NSString *txt = [NSString stringWithFormat:@"%d",pageNmbr];
    NSString *txt = [NSString stringWithFormat:@"%ld",(long)pageNmbr];
    CGSize size = [txt sizeWithAttributes: @{NSFontAttributeName: fontPager}];
    float width = size.width;
    float height = size.height;
    float x = kPaperSizeA4.width-(20+width);
    float y = kPaperSizeA4.height-(20+height);
    
    CGRect txtSize= CGRectMake(x, y, width, height);
    
    NSDictionary *textAttributes = @{NSFontAttributeName:fontPager,NSForegroundColorAttributeName:[UIColor darkGrayColor]};
    
    [txt drawWithRect:txtSize options:NSStringDrawingUsesLineFragmentOrigin attributes:textAttributes context:[[NSStringDrawingContext alloc] init]];
}

#pragma -mark Create PDF
-(void)createPDFPath {
    
    NSString * documentDirectory;
    
    //[NSString stringWithFormat:@"%@:%@-%@",appDelegate.assessmentPropertyName,appDelegate.assessmentName,_assessmentDataLaunchDateTime];
    // Retrieves the document directories from the iOS device
    //assessmenttitle Assessment of <propertyname> by <trainername> DD MMM YYYY]
    
    //19-Sep-2019 04:38 PM
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy HH:mm a"];
    
    
    
    if (appDelegate.isResponsesSaved  || appDelegate.isSubmitted) {
        
        
        
        AssessmentListingModel *obj = [appDelegate.arrDashboardData objectAtIndex:appDelegate.selectedIndex];
        
        NSDate *date  = [dateFormatter dateFromString:obj.assessmentSubmittedDateTime];
        
        // Convert to new Date Format
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        NSString *newDate = [dateFormatter stringFromDate:date];
        
        fileName = [NSString stringWithFormat:@"%@ %@.pdf",appDelegate.assessmentName,newDate];
        
        emailbodyText = [NSString stringWithFormat:@"<body><p>Please find attached the report of assessment conducted for %@ by %@</p></body>",appDelegate.assessmentPropertyName,obj.assessmentTrainer];
        
        
        subjectText=  [NSString stringWithFormat:@"%@ Assessment of %@ by %@ ",appDelegate.assessmentName,appDelegate.assessmentPropertyName,obj.assessmentTrainer];
        
    }
    else {
        
        AssessmentResponse *obj = [appDelegate.arrAssessmentResponseMaster objectAtIndex:0];
        
        NSDate *date  = [dateFormatter dateFromString:obj.assesmentDate];
        
        // Convert to new Date Format
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        NSString *newDate = [dateFormatter stringFromDate:date];
        
        
        fileName = [NSString stringWithFormat:@"%@ %@.pdf",appDelegate.assessmentName,newDate];
        emailbodyText = [NSString stringWithFormat:@"<body><p>Please find attached the report of assessment conducted for %@ by %@ %@</p></body>",appDelegate.assessmentPropertyName,obj.assessmenTrainer,obj.assesmentDate];
        
        
        subjectText=  [NSString stringWithFormat:@"%@ Assessment of %@ by %@ ",appDelegate.assessmentName,appDelegate.assessmentPropertyName,obj.assessmenTrainer];
    }
    
    
    //     fileName = [NSString stringWithFormat:@"%@ %@ %@.pdf",appDelegate.assessmentPropertyName,appDelegate.assessmentName,_assessmentDataLaunchDateTime];
    
    
    fileName=   [fileName stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    documentDirectory = [documentDirectories objectAtIndex:0];
    documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:fileName];
    // [self generateDocument:documentDirectoryFilename];
    [self delteExistFile];
    
}

-(void)delteExistFile{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL fileExists2 = [fileManager fileExistsAtPath:documentDirectoryFilename];
    NSLog(@"Path to file: %@", documentDirectoryFilename);
    
    if (fileExists2)
    {
        BOOL success = [fileManager removeItemAtPath:documentDirectoryFilename error:&error];
        
        // fileExists = false;
        
        if (!success) NSLog(@"Error: %@", [error localizedDescription]);
        NSLog(@"File Deleted");
    }
    
    [self generateDocument:documentDirectoryFilename];
}

-(void)generateDocument:(NSString *) filePath {
    //rosh
    NSLog(@"Directory: %@",filePath);
    
    //[self  delteExistFile];
    
    UIGraphicsBeginPDFContextToFile(filePath, CGRectZero, nil); // PDF Start
    
    // First Page
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,kPaperSizeA4.width, kPaperSizeA4.height), nil);
    [self drawBackground];
    [self drawHeaderFooter:pageNumber];
    
    if (pageNumber == 1) {
        [self setGeneralInformation];
    }
    
    UIGraphicsEndPDFContext();
    // PDF End
    pdfContentData = [NSData dataWithContentsOfURL:[NSURL URLWithString:documentDirectoryFilename]];
    
    
}
-(void)setupValuesView:(CGRect)imageHeader :(UIView *)subView {
    
    
    
    UIGraphicsBeginImageContextWithOptions(subView.frame.size, NO, 0.0);
    struct CGContext *context = UIGraphicsGetCurrentContext();
    [subView.layer renderInContext:context];
    
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [snapshotImageFromMyView drawInRect:imageHeader];
    
}

-(void)setGeneralInformation {
    
    NSString *trainerName,*deptname,*timeDate;
    if (appDelegate.isResponsesSaved  || appDelegate.isSubmitted) {
        AssessmentListingModel *obj = [appDelegate.arrDashboardData objectAtIndex:appDelegate.selectedIndex];
        trainerName =obj.assessmentTrainer;
        deptname = obj.departmentName;
        timeDate  = obj.assessmentSubmittedDateTime;
        
    }
    else {
        AssessmentResponse *obj = [appDelegate.arrAssessmentResponseMaster objectAtIndex:0];
        trainerName =obj.assessmenTrainer;
        deptname = obj.departmentID;
        timeDate  = obj.assesmentDate;
    }
    float widthHeader = kPaperSizeA4.width - 70;
    float heightHeader = self.generalInfoView.frame.size.height +100;
    float xHeader = 35;
    float yHeader = self.headerView.frame.size.height +25;
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
    
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    [viewH addSubview:self.generalInfoView];
    //viewH.backgroundColor =[UIColor redColor];
    self.lblHeader.text =@"General Information";
    CustomUILabel*trainer = [self createLeftLabel:0 width:viewH.frame.size.width
                             /2.0 top:self.generalInfoView.frame.size.height height:34 ctype:1];
    trainer.text = [NSString stringWithFormat:@"Trainer Name: %@",trainerName];
    [viewH addSubview:trainer];
    
    CustomUILabel *lblTime = [self createRightLabel:viewH.frame.size.width/2.0 - 1 width:viewH.frame.size.width
                              /2.0 +1 top:self.generalInfoView.frame.size.height height:34 ctype:1];
    
    [lblTime setText:[NSString stringWithFormat:@"Date and Time: %@",timeDate]];
    [viewH addSubview:lblTime];
    
    CustomUILabel*meal = [self createLeftLabel:0 width:viewH.frame.size.width
                          /2.0  top:lblTime.frame.origin.y +lblTime.frame.size.height-1 height:34 ctype:1];
    meal.text = [NSString stringWithFormat: @"Meal Period: %@",mealType ];
    [viewH addSubview:meal];
    
    CustomUILabel *roomNo = [self createRightLabel:viewH.frame.size.width/2.0 -1 width:viewH.frame.size.width
                             /2.0 +1 top:lblTime.frame.origin.y + lblTime.frame.size.height-1 height:34 ctype:1];
    [roomNo setText:[NSString stringWithFormat:@"Room Number: %@",roomNumber]];
    [viewH addSubview:roomNo];
    
    CustomUILabel *dept = [self createFullLabel:0 width:viewH.frame.size.width-1
                                            top:roomNo.frame.origin.y + roomNo.frame.size.height-1 height:34];
    [dept setText:[NSString stringWithFormat:@"Department Name: %@",deptname]];
    [viewH addSubview:dept];
    
    CustomUILabel *aName = [self createFullLabel:0 width:viewH.frame.size.width-1
                                             top:dept.frame.origin.y + dept.frame.size.height-1 height:34];
    [aName setText:[NSString stringWithFormat:@"Assessment Title: "]];
    // [viewH addSubview:aName];
    
    
    [viewH drawRect:imageHeader];
    
    [self setupValuesView:imageHeader :viewH];
    
    [self setEmployeeandRole: viewH.frame.origin.y + viewH.frame.size.height];
    
}



-(void)setEmployeeandRole:(int)y {
    
    float widthHeader = kPaperSizeA4.width - 70;
    float heightHeader = 25;
    float xHeader = 35;
    float yHeader = y + 10;
    
    float empheight = [self heightForText:[appDelegate.arrAssessmentEmployees componentsJoinedByString:@", "] font:fontPager width:widthHeader - 150];
    float rolheight = [self heightForText:self.lblRoles.text font:fontPager width:widthHeader - 150];
    int heightA ;
    if (empheight>rolheight) {
        heightA =empheight +20;;
    }
    else {
        heightA = rolheight +20;;
    }
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, SECTION_HEIGHT +heightA);
    
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    
    CustomUILabel*lblEmp = [self createLeftLabel:0 width:viewH.frame.size.width
                            /2.0 - 5 top:0 height:SECTION_HEIGHT ctype:2];
    lblEmp.text =@"Employees(s)";
    [viewH addSubview:lblEmp];
    
    CustomUILabel *lblRoles = [self createRightLabel:viewH.frame.size.width/2.0 + 5 width:viewH.frame.size.width/2.0 - 5
                                                 top:0 height:SECTION_HEIGHT ctype:2];
    
    [lblRoles setText:[NSString stringWithFormat:@"Roles Covered"]];
    [viewH addSubview:lblRoles];
    
    CustomUILabel*emp = [self createLeftLabel:0 width:lblEmp.frame.size.width
                                          top:lblEmp.frame.origin.y +lblEmp.frame.size.height-1 height:heightA ctype:1];
    emp.text =[appDelegate.arrAssessmentEmployees componentsJoinedByString:@", "];
    [viewH addSubview:emp];
    
    CustomUILabel *rolesT = [self createRightLabel:viewH.frame.size.width/2.0 + 5 width:lblRoles.frame.size.width
                                               top:lblRoles.frame.origin.y + lblRoles.frame.size.height-1  height:heightA ctype:1];
    [rolesT setText:[NSString stringWithFormat:@"%@",self.lblRoles.text]];
    [viewH addSubview:rolesT];
    
    
    [viewH drawRect:imageHeader];
    
    [self setupValuesView:imageHeader :viewH];
    [self setResult: viewH.frame.origin.y + viewH.frame.size.height];
    
    
}

-(void)setResult:(int)y {
    
    float widthHeader = kPaperSizeA4.width - 70;
    float heightHeader = self.generalInfoView.frame.size.height + 136 ;
    float xHeader = 35;
    float yHeader = y +10;
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
    
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    [viewH addSubview:self.generalInfoView];
    self.lblHeader.text =@"Result";
    
    
    CustomUILabel*lblEmp = [self createLeftLabel:0 width:viewH.frame.size.width
                            /2.0 -5 top:self.generalInfoView.frame.size.height+ self.generalInfoView.frame.origin.y
                                          height:SECTION_HEIGHT  ctype:2];
    lblEmp.text =@"Score";
    [viewH addSubview:lblEmp];
    
    CustomUILabel *lblRoles = [self createRightLabel:viewH.frame.size.width/2.0 + 5 width:viewH.frame.size.width/2.0 - 5
                                                 top:self.generalInfoView.frame.size.height+ self.generalInfoView.frame.origin.y height:SECTION_HEIGHT ctype:2];
    
    [lblRoles setText:[NSString stringWithFormat:@"Inspired Services"]];
    [viewH addSubview:lblRoles];
    
    CustomUILabel*emp = [self createLeftLabel:0 width:lblEmp.frame.size.width
                                          top:lblEmp.frame.origin.y +lblEmp.frame.size.height-1 height:110  ctype:3];
    emp.text =_lblScore.text;
    [viewH addSubview:emp];
    
    CustomUILabel *rolesT = [self createRightLabel:viewH.frame.size.width/2.0 + 5 width:lblRoles.frame.size.width
                                               top:lblRoles.frame.origin.y + lblRoles.frame.size.height-1 height:110 ctype:3];
    [rolesT setText:[NSString stringWithFormat:@"%@",_lblInspiredService.text]];
    
    [viewH addSubview:rolesT];
    
    
    [viewH drawRect:imageHeader];
    
    [self setupValuesView:imageHeader :viewH];
    
    [self setCategoriesOFAssessment: viewH.frame.origin.y + viewH.frame.size.height];
    
}

-(void)setCategoriesOFAssessment:(int)y {
    
    float widthHeader = kPaperSizeA4.width - 70;
    float heightHeader = 30;
    float xHeader = 35;
    float yHeader = y +10;
    
    
    
    if (30 >= kPaperSizeA4.height - y ) {
        yHeader =self.headerView.frame.size.height + 25;
        
        pageNumber++;
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,kPaperSizeA4.width, kPaperSizeA4.height), nil);
        [self drawBackground];
        [self drawHeaderFooter:pageNumber];
    }
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, SECTION_HEIGHT);
    
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    
    
    CustomUILabel*lblEmp = [self createLeftLabel:0 width:viewH.frame.size.width
                                             top:0
                            
                                          height:SECTION_HEIGHT  ctype:2];
    lblEmp.text =@"Categories of Assessments";
    [viewH addSubview:lblEmp];
    [viewH drawRect:imageHeader];
    [self setupValuesView:imageHeader :viewH];
    yHeader = yHeader + viewH.frame.size.height;
    //
    for (int i= 0; i<arrClassificaiton.count; i++) {
        
        if( yHeader >= kPaperSizeA4.height - 110)
        {
            yHeader =self.headerView.frame.size.height + 25;
            
            pageNumber++;
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,kPaperSizeA4.width, kPaperSizeA4.height), nil);
            [self drawBackground];
            [self drawHeaderFooter:pageNumber];
        }
        classificationModel = [arrClassificaiton objectAtIndex:i];
        float height = [self heightForText:classificationModel.classificationName font:fontPager width:widthHeader - 150];
        height = height +20;
        CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader,height);
        
        UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
        CustomUILabel *lblTitles = [self createRightLabel:0 width:viewH.frame.size.width - SCORE_WIDTH
                                                      top: 0 height:height ctype:1];
        [lblTitles setText:classificationModel.classificationName];
        // lblTitles.backgroundColor = [UIColor redColor];
        [viewH addSubview:lblTitles];
        
        CustomUILabel*score = [self createLeftLabel:lblTitles.frame.size.width -1 width:viewH.frame.size.width - lblTitles.frame.size.width +1
                                                top: 0 height:height  ctype:1];
        score.text =classificationModel.classficationScore;
        score.textAlignment = NSTextAlignmentCenter;
        
        [viewH addSubview:score];
        
        yHeader = yHeader + lblTitles.frame.size.height -2;
        
        
        [viewH drawRect:imageHeader];
        [self setupValuesView:imageHeader :viewH];
    }
    [self setScetionHeader:yHeader];
    
}

-(void)setScetionHeader:(int)y {
    
    float widthHeader = kPaperSizeA4.width - 70;
    float heightHeader = 40 + SECTION_HEIGHT;
    float xHeader = 35;
    float yHeader = y +10;
    
    
    
    if (y >= kPaperSizeA4.height - 200) {
        yHeader =self.headerView.frame.size.height + 25;
        
        pageNumber++;
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,kPaperSizeA4.width, kPaperSizeA4.height), nil);
        [self drawBackground];
        [self drawHeaderFooter:pageNumber];
    }
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
    
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    viewH.backgroundColor =[UIColor greenColor];
    
    self.lblHeader.text =@"Responses";
    [viewH addSubview:self.generalInfoView];
    CustomUILabel*lblEmp = [self createLeftLabel:0 width:viewH.frame.size.width
                                             top:self.generalInfoView.frame.size.height + self.generalInfoView.frame.origin.y
                            
                                          height:SECTION_HEIGHT  ctype:2];
    lblEmp.text = appDelegate.assessmentName;
    [viewH addSubview:lblEmp];
    
    [viewH drawRect:imageHeader];
    
    [self setupValuesView:imageHeader :viewH];
    
    // lblyframe = viewH.frame.size.height + self.generalInfoView.frame.size.height;
    [self setResponse:  viewH.frame.size.height + viewH.frame.origin.y];
    
}

-(void)setResponse:(int)y {
    
    float widthHeader = kPaperSizeA4.width - 70;
    float heightHeader = 30;
    float xHeader = 35;
    float yHeader = y ;
    
    
    //
    for (int i= 0; i<appDelegate.arrAssessmentResponseMaster.count; i++) {
        
        if( yHeader >= kPaperSizeA4.height - 110)
        {
            yHeader =self.headerView.frame.size.height + 25;
            
            pageNumber++;
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,kPaperSizeA4.width, kPaperSizeA4.height), nil);
            [self drawBackground];
            [self drawHeaderFooter:pageNumber];
        }
        
        AssessmentResponse *assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
        NSString *imageName =assessmentResponse.isImage;
        //[appDelegate.isAssessmentImage objectForKey:[NSString stringWithFormat:@"Image%d",i]];
        float comentX = 0.0;
        float comentWidth = widthHeader - SCORE_WIDTH +1;
        
        float heightQ = [self heightForText:assessmentResponse.assessmentQuestion font:fontPager width:widthHeader - 150];
        heightQ = heightQ +20;
        float comentH = 0.0 ;
        if(assessmentResponse.responseComments.length>0) {
            
            if (imageName != nil && ![imageName isEqualToString:@""]) {
                comentH   =  [self heightForText:[NSString stringWithFormat:@"Comments: %@", assessmentResponse.responseComments] font:fontPager width:widthHeader - SCORE_WIDTH - IMAGEVIEW_WIDTH - 5 ];
                
            }
            else {
                comentH   =  [self heightForText:[NSString stringWithFormat:@"Comments: %@", assessmentResponse.responseComments] font:fontPager width:widthHeader - SCORE_WIDTH  - 5 ];
            }
            comentH =comentH+20;
        }
        
        float finalHeight = 0.0;
        
        
        
        if (imageName != nil && ![imageName isEqualToString:@""] && comentH < IMAGEVIEW_HEIGHT){
            finalHeight   = IMAGEVIEW_HEIGHT+heightQ;
            
            comentX =IMAGEVIEW_WIDTH;
            comentWidth = widthHeader - SCORE_WIDTH - IMAGEVIEW_WIDTH+1;
        }
        else if(assessmentResponse.responseComments != nil && ![assessmentResponse.responseComments isEqualToString:@""] ) {
            finalHeight   = heightQ + comentH;
            
        }
        
        else {
            finalHeight =heightQ;
        }
        
        CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader,finalHeight);
        UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
        viewH.layer.borderColor = [UIColor lightGrayColor].CGColor;
        viewH.layer.borderWidth = 1.0;
        CustomUILabel *lblTitles = [self createRightLabel:0 width:viewH.frame.size.width - SCORE_WIDTH
                                                      top: 0 height:heightQ ctype:1];
        [lblTitles setText:assessmentResponse.assessmentQuestion];
        [viewH addSubview:lblTitles];
        
        if (imageName != nil && ![imageName isEqualToString:@""]) {
            UIImageView*imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, lblTitles.frame.size.height,IMAGEVIEW_WIDTH, IMAGEVIEW_HEIGHT)];
            imgView.image = [self getImageName:imageName withImageURL:assessmentResponse.imageURL];;
            [viewH addSubview:imgView];
            
        }
        
        
        if (assessmentResponse.responseComments != nil && ![assessmentResponse.responseComments isEqualToString:@""]) {
            
            CustomUILabel *lblComment = [self createFullLabel:comentX  width:comentWidth                                                       top: lblTitles.frame.size.height-1  height:finalHeight-heightQ+1];
            [lblComment setText:[NSString stringWithFormat:@"Comments: %@", assessmentResponse.responseComments]];
            lblComment.backgroundColor =[UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f];
            
            [viewH addSubview:lblComment];
            
        }
        
        
        CustomUILabel*score = [self createLeftLabel:lblTitles.frame.size.width -1 width:viewH.frame.size.width - lblTitles.frame.size.width +1
                                                top: 0 height:finalHeight  ctype:1];
        score.text =assessmentResponse.responseText;
        score.textAlignment = NSTextAlignmentCenter;
        
        [viewH addSubview:score];
        [viewH drawRect:imageHeader];
        
        yHeader = yHeader + viewH.frame.size.height -2 ;
        [self setupValuesView:imageHeader :viewH];
        
    }
    [self setResponseArrivalServices:  yHeader];
    
}

-(void)setResponseArrivalServices:(int)y {
    
    float widthHeader = kPaperSizeA4.width - 70;
    float heightHeader = 30;
    float xHeader = 35;
    float yHeader = y +20;
    
    
    
    //if (40 <= kPaperSizeA4.height - y )
    if( yHeader >= kPaperSizeA4.height - 110)
    {
        yHeader =self.headerView.frame.size.height + 25;
        
        pageNumber++;
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,kPaperSizeA4.width, kPaperSizeA4.height), nil);
        [self drawBackground];
        [self drawHeaderFooter:pageNumber];
    }
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, SECTION_HEIGHT);
    
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    
    CustomUILabel*lblEmp = [self createLeftLabel:0 width:viewH.frame.size.width
                                             top:0
                            
                                          height:SECTION_HEIGHT  ctype:2];
    lblEmp.text =@"Inspired Service";
    [viewH addSubview:lblEmp];
    [viewH drawRect:imageHeader];
    [self setupValuesView:imageHeader :viewH];
    yHeader = yHeader + viewH.frame.size.height;
    //
    for (int i= 0; i<appDelegate.inspiredServicesResponseMaster.count; i++) {
        
        if( yHeader >= kPaperSizeA4.height - 110)
        {
            yHeader =self.headerView.frame.size.height + 25;
            
            pageNumber++;
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,kPaperSizeA4.width, kPaperSizeA4.height), nil);
            [self drawBackground];
            [self drawHeaderFooter:pageNumber];
        }
        
        AssessmentResponse *assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:i];
        NSString *imageName =assessmentResponse.isImage;
        //[self.isAssessmentImageDic valueForKey:[NSString stringWithFormat:@"Image%d",i]];
        float comentX = 0.0;
        float comentWidth =viewH.frame.size.width - SCORE_WIDTH +1;
        
        float heightQ = [self heightForText:assessmentResponse.assessmentQuestion font:fontPager width:widthHeader - 150];
        heightQ = heightQ +20;
        float comentH = 0.0 ;
        if(assessmentResponse.responseComments.length>0) {
            
            if (imageName != nil && ![imageName isEqualToString:@""]) {
                comentH   =  [self heightForText:[NSString stringWithFormat:@"Comments: %@", assessmentResponse.responseComments] font:fontPager width:widthHeader - SCORE_WIDTH - IMAGEVIEW_WIDTH - 5 ];
                
            }
            else {
                comentH   =  [self heightForText:[NSString stringWithFormat:@"Comments: %@", assessmentResponse.responseComments] font:fontPager width:widthHeader - SCORE_WIDTH  - 5 ];
            }
            comentH =comentH+20;
        }
        
        float finalHeight = 0.0;
        
        
        
        if (imageName != nil && ![imageName isEqualToString:@""] && comentH < IMAGEVIEW_HEIGHT){
            finalHeight   = IMAGEVIEW_HEIGHT+heightQ;
            
            comentX =IMAGEVIEW_WIDTH;
            comentWidth = viewH.frame.size.width - SCORE_WIDTH - IMAGEVIEW_WIDTH+1;
        }
        else if(assessmentResponse.responseComments != nil && ![assessmentResponse.responseComments isEqualToString:@""] ) {
            finalHeight   = heightQ + comentH;
            
        }
        
        else {
            finalHeight =heightQ;
        }
        
        CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader,finalHeight);
        UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
        viewH.layer.borderColor = [UIColor lightGrayColor].CGColor;
        viewH.layer.borderWidth = 1.0;
        CustomUILabel *lblTitles = [self createRightLabel:0 width:viewH.frame.size.width - SCORE_WIDTH
                                                      top: 0 height:heightQ ctype:1];
        [lblTitles setText:assessmentResponse.assessmentQuestion];
        [viewH addSubview:lblTitles];
        
        if (imageName != nil && ![imageName isEqualToString:@""]) {
            
            
            
            UIImageView*imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, lblTitles.frame.size.height,IMAGEVIEW_WIDTH, IMAGEVIEW_HEIGHT)];
            imgView.image =[self getImageName:imageName withImageURL:assessmentResponse.imageURL];
            //[UIImage imageNamed:@"SplashIcon.png"];
            [viewH addSubview:imgView];
            
        }
        
        
        if (assessmentResponse.responseComments != nil && ![assessmentResponse.responseComments isEqualToString:@""]) {
            
            CustomUILabel *lblComment = [self createFullLabel:comentX  width:comentWidth                                                       top: lblTitles.frame.size.height-1  height:finalHeight-heightQ+1];
            [lblComment setText:[NSString stringWithFormat:@"Comments: %@", assessmentResponse.responseComments]];
            lblComment.backgroundColor =[UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f];
            
            [viewH addSubview:lblComment];
            
        }
        
        
        CustomUILabel*score = [self createLeftLabel:lblTitles.frame.size.width -1 width:viewH.frame.size.width - lblTitles.frame.size.width +1
                                                top: 0 height:finalHeight  ctype:1];
        score.text =assessmentResponse.responseText;
        score.textAlignment = NSTextAlignmentCenter;
        
        [viewH addSubview:score];
        
        NSLog(@"yHeader : %f ",yHeader);
        
        [viewH drawRect:imageHeader];
        
        yHeader = yHeader + viewH.frame.size.height -2 ;
        [self setupValuesView:imageHeader :viewH];
    }
    
    pageNumber =1;
}

-(UIImage *)getImageName:(NSString *)imageName withImageURL:(NSString*)imgURL {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // __weak  UIImage *img;
    BOOL fileExists2 = [fileManager fileExistsAtPath:[Utility setDocumentDirectoryPath:imageName]];
    __weak  UIImage* img;
    if (fileExists2) {
        
        return  [UIImage imageWithContentsOfFile:[Utility setDocumentDirectoryPath:imageName]];
        
    }
    else {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        __weak SummaryViewController *weakself = self;
        
        UIImage* (^igetImageData) (void) = ^{
            //
            return  [weakself loadPictureFileURL:[Utility setDocumentDirectoryPath:imageName] withSeverURL:imgURL];
        };
        img = igetImageData();
        /*  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
         
         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
         [weakself loadPicture:imgURL];
         //  [MBProgressHUD hideHUDForView:weakself.view animated:YES];
         
         });*/
    }
    return img;
}

-(UIImage *)loadPictureFileURL:(NSString*)docPath withSeverURL:(NSString*)serverURL  {
    
    NSLog(@"DocPAth: %@",docPath);
    NSData *dataImage1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:serverURL]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [dataImage1 writeToFile:docPath atomically:YES];
    });
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    return  [UIImage imageWithData:dataImage1];
    
}

- (float) heightForText: (NSString *) string font: (UIFont *) font width: (float) width {
    NSDictionary *fontAttributes = [NSDictionary dictionaryWithObject: font
                                                               forKey: NSFontAttributeName];
    CGRect rect = [string boundingRectWithSize: CGSizeMake(width, INT_MAX)
                                       options: NSStringDrawingUsesLineFragmentOrigin
                                    attributes: fontAttributes
                                       context: nil];
    return rect.size.height;
}
-(CustomUILabel *)createFullLabel:(int)left width:(int)width top:(int)top height:(int)height {
    
    CustomUILabel *lbl= [[CustomUILabel alloc] initWithFrame:CGRectMake(left, top, width , height)];
    lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    
    [lbl setNumberOfLines:0];
    [lbl setBackgroundColor:[UIColor whiteColor]];
    [lbl setTextColor:[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f]];
    lbl.layer.borderColor =[UIColor lightGrayColor].CGColor;
    lbl.layer.borderWidth = 1.0;
    [lbl setNumberOfLines:0];
    
    return lbl;
    
}

-(CustomUILabel *)createRightLabel:(int)left width:(int)width top:(int)top height:(int)height ctype:(int)type {
    
    CustomUILabel *lbl= [[CustomUILabel alloc] initWithFrame:CGRectMake(left, top, width , height)];
    [lbl setNumberOfLines:0];
    // [self setLabel:lbl];
    lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    
    
    if (type == 1) {
        [lbl setBackgroundColor:[UIColor whiteColor]];
        [lbl setTextColor:[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f]];
        lbl.layer.borderColor =[UIColor lightGrayColor].CGColor;
        lbl.numberOfLines = 0;
    }
    else if (type == 2) {
        lbl.textAlignment = NSTextAlignmentCenter;
        
        [lbl setBackgroundColor:[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f]];
        [lbl setTextColor:[UIColor whiteColor]];
        lbl.layer.borderColor =[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
        
        
    }
    else {
        lbl.textAlignment = NSTextAlignmentCenter;
        
        [lbl setBackgroundColor:[UIColor whiteColor]];
        [lbl setTextColor:[UIColor colorWithRed:213.0f/255.0f green:153.0f/255.0f blue:59.0f/255.0f alpha:1.0f]];
        lbl.layer.borderColor =[UIColor lightGrayColor].CGColor;
        lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:24];
        
    }
    
    lbl.layer.borderWidth = 1.0;
    return lbl;
    
}

-(CustomUILabel *)createLeftLabel:(int)left width:(int)width top:(int)top height:(int)height ctype:(int)type {
    
    CustomUILabel *lbl= [[CustomUILabel alloc] initWithFrame:CGRectMake(left, top, width , height)];
    [lbl setNumberOfLines:0];
    // [self setLabel:lbl];
    lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    
    if (type == 1) {
        [lbl setBackgroundColor:[UIColor whiteColor]];
        [lbl setTextColor:[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f]];
        lbl.layer.borderColor =[UIColor lightGrayColor].CGColor;
        
    }
    else if (type == 2) {
        lbl.textAlignment = NSTextAlignmentCenter;
        
        [lbl setBackgroundColor:[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f]];
        [lbl setTextColor:[UIColor whiteColor]];
        lbl.layer.borderColor =[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
        
    }
    else {
        lbl.textAlignment = NSTextAlignmentCenter;
        
        [lbl setBackgroundColor:[UIColor whiteColor]];
        [lbl setTextColor:[UIColor colorWithRed:72.0f/255.0f green:160.0f/255.0f blue:198.0f/255.0f alpha:1.0f]];
        lbl.layer.borderColor =[UIColor lightGrayColor].CGColor;
        lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:24];
        
    }
    
    lbl.layer.borderWidth = 1.0;
    
    
    return lbl;
    
}

-(void)setLabel:(CustomUILabel *)lbl {
    
    lbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    lbl.layer.borderWidth = 1.0;
    lbl.layer.borderColor =[UIColor lightGrayColor].CGColor;
    [lbl setBackgroundColor:[UIColor clearColor]];
    [lbl setTextColor:[UIColor colorWithRed:53.0f/255.0f green:53.0f/255.0f blue:53.0f/255.0f alpha:1.0f]];
    
}

-(void)drawBackground {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, kPaperSizeA4.width, kPaperSizeA4.height);
    CGContextSetFillColorWithColor(context, [[UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f] CGColor]);
    CGContextFillRect(context, rect);
}


#pragma mark ===  UIButton actions methods ===
-(IBAction)emailButtonTapeed:(id)sender {
    
    //    [self createPDFPath];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        __weak SummaryViewController *weakself = self;
        dispatch_time_t popTime =  dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
        
        dispatch_after(popTime, dispatch_get_main_queue(), ^{
            [self createPDFPath];
            [self sendInEmail];
            [MBProgressHUD hideHUDForView:weakself.view animated:YES];
            
        });
    }
    else {
        UIAlertController *accountAlert =[UIAlertController alertControllerWithTitle:@"Alert"
                                                                             message:@"Please setup mail account."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* btnOk = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil];
        
        [accountAlert addAction:btnOk];
        
        [self presentViewController:accountAlert animated:YES completion:nil];
        
    }
    
    
}

-(IBAction)backButtonTapeed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)savedButtonTapeed:(id)sender
{
    _btnSaved.enabled = false;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ self performSelector:@selector(saveDataIntoDB) withObject:nil afterDelay:0.2];
    
}

-(NSData *)getImageData:(NSString *)imgName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath;
    
    imageFilePath = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@.png",imgName]];
    
    
    /// imageFilePath = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@.png",[isAssessmentImageDic objectForKey:[NSString stringWithFormat:@"Image%ld",(long)path.section]]]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL fileExists2 = [fileManager fileExistsAtPath:imageFilePath];
    NSLog(@"Path to file: %@", imageFilePath);
    UIImage *img;
    if (fileExists2) {
        img  = [UIImage imageWithContentsOfFile: imageFilePath];
    }
    
    return  UIImageJPEGRepresentation(img, 1);
    
    
}

-(void)updateDataAtServer {
    
    if (appDelegate.isResponsesSaved)
    {
        
        
        //new Response not updating locally
        [self setValuesInArrayWithUpdatedStatus:@"Submitted"];
        
        dispatch_async(dispatch_get_main_queue(),^(void){
            [self updateDataAtSever];
            [self restValues];
            
        });
    }
    else
    {
        
        //new Response not saving locally
        
        [self setValuesInArrayWithStatus:@"Submitted"];
        dispatch_async(dispatch_get_main_queue(),^(void){
            
            [self sendDataToServer];
            
        });
    }
}

-(IBAction)submitButtonTapeed:(id)sender
{
    _btnSubmit.enabled = false;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ self performSelector:@selector(updateDataAtServer) withObject:nil afterDelay:0.2];
    
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getClassficationData:(NSString *)dbPath {
    
    [arrClassificaiton removeAllObjects];
    
    NSString *query=  [NSString stringWithFormat:@"select * from CategoryTitleTable ORDER BY Category asc"];
    //ORDER BY Category asc
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    int indexx = 0;
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                classificationModel = [[ClassificationModel alloc]init];
                classificationModel.classificationID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                classificationModel.classificationName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                indexx = indexx + 1;
                // calculate categories score
                for (int i = 0; i < appDelegate.allAssessmentArray.count; i++) {
                    
                    QuestionsModel *assessmentResponse = [appDelegate.allAssessmentArray objectAtIndex:i];
                    
                    //check for total yes and yes+no against each classfication
                    NSLog(@"Classification:%@-%@",classificationModel.classificationID,assessmentResponse.SectionName);
                    if ([classificationModel.classificationID isEqualToString:assessmentResponse.SectionName])
                    {
                        if ([assessmentResponse.responseText isEqualToString:@"YES"]) {
                            countYES++;
                        }
                        if ([assessmentResponse.responseText isEqualToString:@"YES"] || [assessmentResponse.responseText isEqualToString:@"NO"]) {
                            countYESNO++;
                        }
                    }
                }
                
                if (countYESNO>0 || countYES >0) {
                    finalScore   = countYES / countYESNO;
                } else {
                    finalScore = 0;
                }
                //
                
                NSString *totalScore = [NSString stringWithFormat:@"%0.f",finalScore * 100];
                
                switch (indexx) {
                    case 1:
                        cat1Score = totalScore;
                        break;
                        
                    case 2:
                        cat2Score = totalScore;
                        break;
                        
                    case 3:
                        cat3Score = totalScore;
                        break;
                        
                    case 4:
                        cat4Score = totalScore;
                        break;
                        
                    default:
                        break;
                }
                
                totalScore = [totalScore stringByAppendingString:@"%"];
                classificationModel.classficationScore = totalScore;
                
                [scoreClassification addObject:totalScore];
                
                countYESNO = 0;
                countYES = 0;
                
                [arrClassificaiton addObject:classificationModel];
            }
            [_summaryTableView reloadData];
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
}

- (void)processBackgroundThreadUploadInBackgroundPhoto:(NSData *)imageData withName:(NSString *)name
{
    @try
    {
        if(![ACCESS_KEY_ID isEqualToString:@"CHANGE ME"]
           && self.s3 == nil)
        {
            // Initial the S3 Client.
            self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] ;
            self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
        }
        
        NSString *videoName =[NSString stringWithFormat:@"%@",name];
        NSString *trimmedString = [videoName stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:trimmedString
                                                                 inBucket:PICTURE_BUCKET] ;
        por.contentType = @"image/jpeg";
        por.data= imageData;
        
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        [self performSelectorOnMainThread:@selector(showCheckErrorMessage:)
                               withObject:putObjectResponse.error
                            waitUntilDone:NO];
    }
    @catch (NSException *exception)
    {
        return;
    }
    @finally
    {
    }
}

- (void)showCheckErrorMessage:(NSError *)error
{
    if(error != nil)
    {
        
        UIAlertController*  alert = [UIAlertController alertControllerWithTitle:@"Upload Error"
                                                                        message:[error.userInfo objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

-(void)deshLine {
    
    UIBezierPath * path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0.0, 10.0)];
    [path addLineToPoint:CGPointMake(self.deshView.frame.size.width, 10.0)];
    [path setLineWidth:2.0];
    CGFloat dashes[] = { path.lineWidth, path.lineWidth * 2 };
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapSquare];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.deshView.frame.size.width
                                                      , 10), false, 2);
    //  [[UIColor  colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1]setStroke];
    [[UIColor lightGrayColor]setStroke];
    [path stroke];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.deshView.image =image;
}
@end
