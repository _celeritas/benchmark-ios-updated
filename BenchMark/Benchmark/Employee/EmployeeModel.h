//
//  EmployeeModel.h
//  Benchmark
//
//  Created by Mac on 6/19/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmployeeModel : NSObject
@property (strong, nonatomic) NSString* empID;
@property (strong, nonatomic) NSString* empName;
@property (strong, nonatomic) NSString* empDPTName;

@end
