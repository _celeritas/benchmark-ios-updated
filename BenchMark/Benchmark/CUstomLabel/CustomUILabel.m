//
//  CustomUILabel.m
//  FeedbackFrenzy
//
//  Created by Mac on 10/12/18.
//  Copyright © 2018 Hivelet. All rights reserved.
//

#import "CustomUILabel.h"
#import "Utility.h"

@implementation CustomUILabel

-(void)drawTextInRect:(CGRect)rect {
    
   // self.layer.borderWidth = 0.5;
  //  self.layer.borderColor = [UIColor lightGrayColor].CGColor;

    UIEdgeInsets insets = {0, 5, 0, 5};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}
/*-(void)setCornerRadius:(NSInteger)cornerRadius {
    
    self.layer.cornerRadius = cornerRadius;
    self.clipsToBounds = YES;
}*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.*/
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [super drawRect:rect];
}


@end
