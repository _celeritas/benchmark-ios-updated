//
//  CustomUILabel.h
//  BENCHMARK
//
//  Created by Mac on 10/12/18.
//  Copyright © 2019 Hivelet. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CustomUILabel : UILabel
@property (nonatomic) IBInspectable CGFloat cornerRadious;
@end
