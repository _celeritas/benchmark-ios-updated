//
//  AppDelegate.h
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "AssessmentListingModel.h"
//com.Hivelet.BenchmarkApp
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)NSMutableArray *arryData;

@property(nonatomic, strong) NSString *assessmentIDForQuestions;
@property (nonatomic, strong) NSMutableArray *arrAssessmentQuestions;
@property (nonatomic, strong) NSMutableArray *arrAssessmentListing;
@property (nonatomic, strong) NSMutableArray *arrAssessmentResponseMaster;
@property (nonatomic, strong) NSMutableArray *arrDashboardData;
@property (nonatomic, strong) NSMutableArray *allAssessmentArray;

@property (nonatomic, strong) NSMutableArray *serverListingArray;
@property (nonatomic, strong) NSMutableArray *serverResponseArray;
@property (nonatomic, strong) NSMutableArray *serverInspiredResponseArray;
@property (nonatomic, strong) NSMutableArray *serverRomeInfoArray;



@property (nonatomic) NSInteger indexValue;
@property (nonatomic) NSInteger selectedIndex;

@property (nonatomic) NSInteger indexValueForResponseMaster;

@property (assign) float score;
@property (assign) float totalYesNo;
@property (assign) float totalYes;
@property (nonatomic, strong ) AssessmentListingModel *dashboardModel;
@property (nonatomic, strong) NSMutableDictionary *dicAssessmentAnswersArray;
@property (nonatomic, strong) NSMutableDictionary *isAssessmentComment;
@property (nonatomic, strong) NSMutableDictionary *isAssessmentImage;
@property(nonatomic)BOOL isReview , isSubmitted , isResponsesSaved , isStandardSaved , isPartial;

- (NSString *) getDBPath;

@property(nonatomic) NSString *reviewID;

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSString *assessmentPropertyName;
@property(nonatomic, strong) NSString *assessmentName;
@property(nonatomic, strong) NSString *assessmentRoles;
@property(nonatomic, strong) NSString *assessmentEmployees;
@property(nonatomic, strong) NSString *assessmentMeal;

@property(nonatomic, strong) NSString *assessmentRoomNO;




-(void)copyDatabaseIntoDocumentsDirectory;

@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic, strong) NSMutableArray *inspiredServicesArray;
@property (nonatomic, strong) NSMutableArray *inspiredServicesResponseMaster;
@property (nonatomic, strong) NSMutableArray *arrClassificaiton;
@property (nonatomic, strong) NSMutableArray *rolesArray;
@property (nonatomic, strong) NSMutableArray *arrAssessmentRolesQuestions;
@property (nonatomic, strong) NSMutableArray *arrAssessmentEmployees;

@property (nonatomic, strong) NSMutableArray *allDataArray;



@end

