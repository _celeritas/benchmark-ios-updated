//
//  PropertyModel.m
//  Benchmark
//
//  Created by Mac on 7/9/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import "PropertyModel.h"
#import "AppDelegate.h"
#import "Utility.h"


static sqlite3 *database = nil;
static sqlite3_stmt *addStmt = nil;

@implementation PropertyModel {
    
    AppDelegate *appDelegate;
}

-(id)init {
    
    self = [super init];
    if (self) {
     appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];

    }
    return self;
}

-(void)deleteProperties {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {

    NSString *query =[NSString stringWithFormat: @"DELETE from Properties"];
    const char *sqlStatement = [query UTF8String];
    
    if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
        // Loop through the results and add them to the feeds array
        while(sqlite3_step(addStmt) == SQLITE_ROW) {
            
            }
        }
    }
}

-(void)loadPropertyDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"properties"];
    NSMutableArray *proArray = [NSMutableArray new];

    @try {
        if ([resultDic count] != 0) {
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setProID: [ID stringValue]];
                [theObject setProTitle:[jsonDict valueForKey:@"PropertyName"]];
                
                [proArray addObject:theObject];
            }
            [self deleteProperties];
            [self setProperyDataToLocalDB:[Utility getDBPath] withArray:proArray];
        }
       
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setProperyDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into Properties(ProID,ProTitle) Values(?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                sqlite3_bind_text(addStmt, 1, [object.proID UTF8String], -1, SQLITE_TRANSIENT);

               sqlite3_bind_text(addStmt, 2, [object.proTitle UTF8String], -1, SQLITE_TRANSIENT);
        
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)deleteDepartments {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from Departments"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadDepartmentDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"departments"];
    NSMutableArray *dptArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setDeptID: [ID stringValue]];
                 [theObject setDeptTitle:[jsonDict valueForKey:@"DepartmentName"]];
                
                
                [dptArray addObject:theObject];
            }
            [self deleteDepartments];
            [self setDepartmentDataToLocalDB:[Utility getDBPath] withArray:dptArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setDepartmentDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into Departments(DeptID,DeptTitle) Values(?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                sqlite3_bind_text(addStmt, 1, [object.deptID UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 2, [object.deptTitle UTF8String], -1, SQLITE_TRANSIENT);
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)deleteMapping {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from propertiesUsersDepartmentsMapping"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadMapDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"mappings"];
    NSMutableArray *mapArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setMapID: [ID stringValue]];
                
                NSNumber *dpt =[jsonDict valueForKey:@"DepartmentID"];
                [theObject setMapDeptID:[dpt stringValue]];
                
                NSNumber *pro =[jsonDict valueForKey:@"PropertyID"];
                [theObject setMapProID:[pro stringValue]];
                
                [mapArray addObject:theObject];
            }
            [self deleteMapping];
            [self loadMapDataFromJSON:[Utility getDBPath] withArray:mapArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)loadMapDataFromJSON:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
    
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into propertiesUsersDepartmentsMapping(DepartmentID,PropertyID) Values(?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                sqlite3_bind_text(addStmt, 1, [object.mapDeptID UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 2, [object.mapProID UTF8String], -1, SQLITE_TRANSIENT);

                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)deleteTrainers {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from trainers"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadTrainerDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"trainers"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
               // NSNumber *ID =  [jsonDict valueForKey:@"UserID"] ;
              //  [theObject setTID: [ID stringValue]];
                [theObject setTFName:[jsonDict valueForKey:@"UserFirstName"]];
                [theObject setTLName:[jsonDict valueForKey:@"UserLastName"]];
                 NSNumber *pID =  [jsonDict valueForKey:@"UserID"] ;
                [theObject setTProID:[pID stringValue]];
                [tArray addObject:theObject];
            }
            [self deleteTrainers];
            [self setTrainerDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setTrainerDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        NSDate *now = [NSDate date];
        statement = @"insert into trainers(UserFirstName,UserLastName,TrainerID) Values(?,?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
           
                sqlite3_bind_text(addStmt, 1, [object.tFName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.tLName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 3, [object.tProID UTF8String], -1, SQLITE_TRANSIENT);

                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)deleteAssessmentsTitle {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from AssessmentsTitle"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadAssessmentTitleDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"assessments"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
    
                NSNumber *pID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setAssesID:[pID stringValue]];
                [theObject setAssesTitle:[jsonDict valueForKey:@"AssessmentName"]];

                [tArray addObject:theObject];
            }
            [ self deleteAssessmentsTitle];
            [self setAssessmentTitlesDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setAssessmentTitlesDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        NSDate *now = [NSDate date];
        statement = @"insert into AssessmentsTitle(AssessmentID,AssessmentTitle) Values(?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                
                sqlite3_bind_text(addStmt, 1, [object.assesID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.assesTitle UTF8String], -1, SQLITE_TRANSIENT);
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}



-(void)deleteAssessmentRolesMapping {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from AssessmentRoleTable"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}
//here

-(void)loadAssessmentRolesMappingDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"assessment_roles"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setRoleMapID:[ID stringValue]];
                NSNumber *aID =  [jsonDict valueForKey:@"AssessmentID"] ;
                [theObject setRoleMapAID:[aID stringValue]];
                NSNumber *rID =  [jsonDict valueForKey:@"RoleID"] ;
                [theObject setRoleMapRID:[rID stringValue]];
                [theObject setRoleMapTitle:[jsonDict valueForKey:@"QuestionText"]];
                
                [tArray addObject:theObject];
            }
            [ self deleteAssessmentRolesMapping];
            [self setAssessmentRolesMappingDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setAssessmentRolesMappingDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into AssessmentRoleTable(ID,AssessmentID,RoleID,QuestionText) Values(?,?,?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                sqlite3_bind_text(addStmt, 1, [object.roleMapID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.roleMapAID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 3, [object.roleMapRID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 4, [object.roleMapTitle UTF8String], -1, SQLITE_TRANSIENT);
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)deleteAssessmentQuestionMapping {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from AssessmentCategoryTable"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}
-(void)loadAssessmentQuestionDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"assessment_categories"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setAssessQID:[ID stringValue]];
                NSNumber *aID =  [jsonDict valueForKey:@"AssessmentID"] ;
                [theObject setAssessAssessmentID:[aID stringValue]];
                NSNumber *cID =  [jsonDict valueForKey:@"CategoryID"] ;
                [theObject setAssessCatID:[cID stringValue]];
                [theObject setAssessQuestionTitle:[jsonDict valueForKey:@"QuestionText"]];
                [tArray addObject:theObject];
            }
            [ self deleteAssessmentQuestionMapping];
            [self setAssessmentQuestionDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setAssessmentQuestionDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into AssessmentCategoryTable(ID,AssessmentID,CategoryID,QuestionText) Values(?,?,?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                
                sqlite3_bind_text(addStmt, 1, [object.assessQID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.assessAssessmentID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 3, [object.assessCatID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 4, [object.assessQuestionTitle UTF8String], -1, SQLITE_TRANSIENT);
                
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)deleteInspiredQuestionRolesMapping {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from InspiredQuestionMaster"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}
-(void)loadInspiredQuestionDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"inspired_questions_master"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *aID =  [jsonDict valueForKey:@"QuestionID"] ;
                [theObject setInsQID:[aID stringValue]];
           
                [theObject setInsAssessNumber: [jsonDict valueForKey:@"AssessmentNumber"]];
                [theObject setInsTitle:[jsonDict valueForKey:@"AssessmentQuestion"]];
                [theObject setInsSection:[jsonDict valueForKey:@"SectionName"]];

                
                [tArray addObject:theObject];
            }
            [ self deleteInspiredQuestionRolesMapping];
            [self setInspiredQuestionDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setInspiredQuestionDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into InspiredQuestionMaster(QuestionID,AssessmentNumber,AssessmentQuestion,SectionName) Values(?,?,?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                
                sqlite3_bind_text(addStmt, 1, [object.insQID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.insAssessNumber UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 3, [object.insTitle UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 4, [object.insSection UTF8String], -1, SQLITE_TRANSIENT);
                
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}


+(void)deleteAllData {

    sqlite3_open([[Utility getDBPath] UTF8String], &database);
    
    NSArray *queryArray = @[ @"DELETE from Properties",
                             @"DELETE from propertiesUsersDepartmentsMapping",
                             @"DELETE from Departments",
                             @"DELETE from trainers",
                             @"DELETE from AssessmentsTitle",
                             @"DELETE from AssessmentsDepartmentMappings"
                            
                            ];
    sqlite3_stmt *compiledStatement;

     for(int i = 0; i < [queryArray count]; i++) {
         
         const char *sqlStatement = [[queryArray objectAtIndex:i] UTF8String];
         if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
             // Loop through the results and add them to the feeds array
             while(sqlite3_step(compiledStatement) == SQLITE_ROW)
             {
                 
             }
           
         }
           sqlite3_finalize(compiledStatement);
     }

    sqlite3_close(database);
    // Release the compiled statement from memory
  
}
//
-(void)loadAssessmentMappingDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"assessments_department_mappings"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setAssesMapID:[ID stringValue]];
                
                NSNumber *pID =  [jsonDict valueForKey:@"DepartmentID"] ;
                [theObject setAssesMapProID:[pID stringValue]];
                
                NSNumber *dID =  [jsonDict valueForKey:@"AssessmentID"] ;
                [theObject setAssesMapDeptID:[dID stringValue]];

                
                [tArray addObject:theObject];
            }
            [self deleteAssessmentsMapping];
            [self setAssessmentMappingDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)deleteAssessmentsMapping {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from AssessmentsDepartmentMappings"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}


-(void)setAssessmentMappingDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        NSDate *now = [NSDate date];
        statement = @"insert into AssessmentsDepartmentMappings(DepartmentID,AssessmentID) Values(?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                
                sqlite3_bind_text(addStmt, 1, [object.assesMapProID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.assesMapDeptID UTF8String], -1, SQLITE_TRANSIENT);
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}
-(void)loadTrianerMappingDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"properties_trainers_mappings"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setTMapID:[ID stringValue]];
                
                NSNumber *pID =  [jsonDict valueForKey:@"PropertyID"] ;
                [theObject setTMapProID:[pID stringValue]];
                
                NSNumber *dID =  [jsonDict valueForKey:@"UserID"] ;
                [theObject setTMapTrainerID:[dID stringValue]];
                
                
                [tArray addObject:theObject];
            }
            [self deleteTrainerMapping];
            [self setTrainerMappingDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)deleteTrainerMapping {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from PropertiesTrainerMapping"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}


-(void)setTrainerMappingDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into PropertiesTrainerMapping(TrainierID,PropertyID) Values(?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                
                sqlite3_bind_text(addStmt, 1, [object.tMapTrainerID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.tMapProID UTF8String], -1, SQLITE_TRANSIENT);
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}
-(void)loadRolesDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"roles"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                PropertyModel  *theObject =[[PropertyModel alloc] init];
                
                NSNumber *ID =  [jsonDict valueForKey:@"ID"] ;
                [theObject setRoleID:[ID stringValue]];
                
                [theObject setRoleTitle:[jsonDict valueForKey:@"Title"]];
       
                
                [tArray addObject:theObject];
            }
            [self deleteRole];
            [self setRolesDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)deleteRole{
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from RoleMaster"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(addStmt) == SQLITE_ROW) {
                
            }
        }
    }
}


-(void)setRolesDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into RoleMaster(RoleID,RoleTitle) Values(?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                PropertyModel  *object =[[PropertyModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                
                sqlite3_bind_text(addStmt, 1, [object.roleID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.roleTitle UTF8String], -1, SQLITE_TRANSIENT);
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

///Get from Local DB
+(NSMutableArray*)getPropertyFromDB {
    
    
   NSMutableArray *proArray = [NSMutableArray new];

    NSString *query = @"Select * from Properties order by ProTitle";
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                PropertyModel *pModel = [[PropertyModel alloc]init];
                pModel.proID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                pModel.proTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                [proArray addObject:pModel];
            }
            
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
    return proArray;
}

+(NSMutableArray*)getDepartmentFromDBWithPropertyID:(NSString*)pID {
    
    
    NSMutableArray *proArray = [NSMutableArray new];
    
    NSString *query =[NSString stringWithFormat: @"SELECT  TP.DeptID,TP.DeptTitle FROM Departments TP INNER JOIN propertiesUsersDepartmentsMapping PM ON PM.DepartmentID = TP.DeptID WHERE  PM.PropertyID ='%@' order by DeptTitle",pID];
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                PropertyModel *pModel = [[PropertyModel alloc]init];
                pModel.deptID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                pModel.deptTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                [proArray addObject:pModel];
            }
            
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
    return proArray;
}

+(NSMutableArray*)getTrainerFromDBwithPropertyID:(NSString *)pID {
    
    
    NSMutableArray *proArray = [NSMutableArray new];
    
  //  NSString *query =[NSString stringWithFormat: @"Select * from trainers where PropertyID = '%@' order by UserFirstName",pID];
    
    
    NSString *query =[NSString stringWithFormat: @"SELECT  TP.UserFirstName,TP.UserLastName,TP.TrainerID FROM trainers TP INNER JOIN PropertiesTrainerMapping  PM ON PM.TrainierID  = TP.TrainerID WHERE  PM.PropertyID='%@' order by UserFirstName",pID];
    
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                PropertyModel *pModel = [[PropertyModel alloc]init];
                pModel.tFName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                pModel.tLName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                pModel.tProID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];


                [proArray addObject:pModel];
            }
            
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
    return proArray;
}

+(NSMutableArray*)getAssessmentTitleFromDBWithDpartmentID:(NSString*)dID {
    
    
    NSMutableArray *proArray = [NSMutableArray new];
    
    NSString *query =[NSString stringWithFormat: @"SELECT  AT.AssessmentID,AT.AssessmentTitle FROM AssessmentsTitle AT INNER JOIN AssessmentsDepartmentMappings PM ON PM.AssessmentID= AT.AssessmentID  WHERE  PM.DepartmentID ='%@' order by AssessmentTitle",dID];
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                PropertyModel *pModel = [[PropertyModel alloc]init];
                pModel.assesID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                pModel.assesTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                [proArray addObject:pModel];
            }
            
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
    return proArray;
}
@end
