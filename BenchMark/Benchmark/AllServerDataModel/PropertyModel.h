//
//  PropertyModel.h
//  Benchmark
//
//  Created by Mac on 7/9/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface PropertyModel : NSObject
//Property
@property (strong, nonatomic) NSString* proID;
@property (strong, nonatomic) NSString* proTitle;
-(void)loadPropertyDataFromJSON:(NSDictionary*)jsonDic;
//Department
@property (strong, nonatomic) NSString* deptID;
@property (strong, nonatomic) NSString* deptTitle;
@property (strong, nonatomic) NSString* deptProID;
-(void)loadDepartmentDataFromJSON:(NSDictionary*)jsonDic;


//Trainer
@property (strong, nonatomic) NSString* tID;
@property (strong, nonatomic) NSString* tFName;
@property (strong, nonatomic) NSString* tLName;
@property (strong, nonatomic) NSString* tProID;

-(void)loadTrainerDataFromJSON:(NSDictionary*)jsonDic;

//Department Mapping
@property (strong, nonatomic) NSString* mapID;
@property (strong, nonatomic) NSString* mapProID;
@property (strong, nonatomic) NSString* mapTrainerID;
@property (strong, nonatomic) NSString* mapDeptID;
-(void)loadMapDataFromJSON:(NSDictionary*)jsonDic;

//Assessment Title
@property (strong, nonatomic) NSString* assesID;
@property (strong, nonatomic) NSString* assesTitle;
-(void)loadAssessmentTitleDataFromJSON:(NSDictionary*)jsonDic;

//Assessment MApping
@property (strong, nonatomic) NSString* assesMapID;
@property (strong, nonatomic) NSString* assesMapProID;
@property (strong, nonatomic) NSString* assesMapDeptID;
-(void)loadAssessmentMappingDataFromJSON:(NSDictionary*)jsonDic;
//Trainer MApping
@property (strong, nonatomic) NSString* tMapID;
@property (strong, nonatomic) NSString* tMapProID;
@property (strong, nonatomic) NSString* tMapTrainerID;
-(void)loadTrianerMappingDataFromJSON:(NSDictionary*)jsonDic;
//Roles
@property (strong, nonatomic) NSString* roleID;
@property (strong, nonatomic) NSString* roleTitle;
-(void)loadRolesDataFromJSON:(NSDictionary*)jsonDic;
//Roles Mapping
@property (strong, nonatomic) NSString* roleMapID;
@property (strong, nonatomic) NSString* roleMapRID;
@property (strong, nonatomic) NSString* roleMapAID;
@property (strong, nonatomic) NSString* roleMapTitle;
-(void)loadAssessmentRolesMappingDataFromJSON:(NSDictionary*)jsonDic;
//InspiredService Assessment Questions
@property (strong, nonatomic) NSString* insQID;
@property (strong, nonatomic) NSString* insAssessNumber;
@property (strong, nonatomic) NSString* insTitle;
@property (strong, nonatomic) NSString* insSection;
-(void)loadInspiredQuestionDataFromJSON:(NSDictionary*)jsonDic; 
//Assessments Questions
@property (strong, nonatomic) NSString* assessQID;
@property (strong, nonatomic) NSString* assessAssessmentID;
@property (strong, nonatomic) NSString* assessCatID;
@property (strong, nonatomic) NSString* assessQuestionTitle;
-(void)loadAssessmentQuestionDataFromJSON:(NSDictionary*)jsonDic;

+(void)deleteAllData;
+(NSMutableArray*)getPropertyFromDB;
+(NSMutableArray*)getDepartmentFromDBWithPropertyID:(NSString*)pID;
+(NSMutableArray*)getTrainerFromDBwithPropertyID:(NSString *)pID ;
+(NSMutableArray*)getAssessmentTitleFromDBWithDpartmentID:(NSString*)dID;
@end
