//
//  GeneralInformationModel.m
//  Benchmark
//
//  Created by Mac on 8/8/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import "GeneralInformationModel.h"
#import "AppDelegate.h"
#import "Utility.h"


static sqlite3 *database = nil;
static sqlite3_stmt *addStmt= nil;
static sqlite3_stmt *deleteStmt = nil;

@implementation GeneralInformationModel {
    AppDelegate *appDelegate;
}

-(id)init {
    
    self =[super init];
    if (self) {
        
        appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
      
    }
    
    return self;
}

-(GeneralInformationModel *) getGeneralInfomationData:(NSString *)iD withController:(UIViewController*)vc {
    
    [MBProgressHUD showHUDAddedTo:vc.view animated:true];
    
    NSString *query=  [NSString stringWithFormat:@"select * from GeneralInformation where AssessmentID = '%@'",iD];
    
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    GeneralInformationModel *obj =[GeneralInformationModel new];
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {

         
                obj.gAssessID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                  obj.gRoomNo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                obj.gMeal = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                
            }
        
        }
    }
    sqlite3_close(database);
    [MBProgressHUD hideHUDForView:vc.view animated:true];
    return  obj;
}

-(void)deleteGeneralInformation {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from GeneralInformation"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &deleteStmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(deleteStmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadGeneralInformationDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"general_information"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                GeneralInformationModel  *theObject =[[GeneralInformationModel alloc] init];
                
                [theObject setGAssessID: [jsonDict valueForKey:@"AssessmentID"]];
                [theObject setGRoomNo:[jsonDict valueForKey:@"RoomNumber"]];
                [theObject setGMeal:[jsonDict valueForKey:@"MealPeriod"]];
            
                [tArray addObject:theObject];
            }
            [ self deleteGeneralInformation];
            [self setGeneralInformationDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setGeneralInformationDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        appDelegate.serverRomeInfoArray = [NSMutableArray new];
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into GeneralInformation (AssessmentID, RoomNumber, MealPeriod) Values(?,?,?)";
        
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                GeneralInformationModel  *object =[[GeneralInformationModel alloc] init];
                object =[pArray objectAtIndex:i];
                object = [pArray objectAtIndex:i];
                sqlite3_bind_text(addStmt, 1, [object.gAssessID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.gRoomNo UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 3, [object.gMeal UTF8String], -1, SQLITE_TRANSIENT);
           
            [appDelegate.serverRomeInfoArray addObject:[Utility roomGeneralInformationWithAssessID:object.gAssessID  withRoomNo:object.gRoomNo withMeal:object.gMeal ]];
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error Meal: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error Meal: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}


@end
