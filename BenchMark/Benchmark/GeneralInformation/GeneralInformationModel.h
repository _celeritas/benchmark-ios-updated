//
//  GeneralInformationModel.h
//  Benchmark
//
//  Created by Mac on 8/8/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import <UIKit/UIKit.h>


@interface GeneralInformationModel : NSObject
@property (strong, nonatomic) NSString* gAssessID;
@property (strong, nonatomic) NSString* gRoomNo;
@property (strong, nonatomic) NSString* gMeal;


-(GeneralInformationModel *) getGeneralInfomationData:(NSString *)iD withController:(UIViewController*)vc;

-(void)loadGeneralInformationDataFromJSON:(NSDictionary*)jsonDic;
-(void)setGeneralInformationDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray;

@end
