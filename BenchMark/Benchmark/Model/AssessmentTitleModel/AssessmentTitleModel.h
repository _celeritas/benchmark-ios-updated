//
//  AssessmentTitleModel.h
//  Benchmark
//
//  Created by Admin on 4/24/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface AssessmentTitleModel : NSObject
@property (nonatomic, retain) NSString *AssessmentID;
@property (nonatomic, retain) NSString *AssessmentName;
@property (nonatomic, retain) NSString *AssessmentColumnName;

+(NSMutableArray*)getAssessmentTitleDataWithDepartmentID:(NSString*)dID;
@end
