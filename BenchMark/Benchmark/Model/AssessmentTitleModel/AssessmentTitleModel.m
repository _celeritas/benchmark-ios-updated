//
//  AssessmentTitleModel.m
//  Benchmark
//
//  Created by Admin on 4/24/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "AssessmentTitleModel.h"
#import "Utility.h"

static sqlite3 *database = nil;

@implementation AssessmentTitleModel

@synthesize AssessmentID, AssessmentName;


+(NSMutableArray*)getAssessmentTitleDataWithDepartmentID:(NSString*)dID {
    
    NSMutableArray *tempArray = [NSMutableArray new];
    
   // NSString *query=  [NSString stringWithFormat:@"select * from AssessmentTitleTable order by Assessment"];
        NSString *query =[NSString stringWithFormat: @"SELECT  AT.AssessmentID,AT.AssessmentTitle FROM AssessmentsTitle AT INNER JOIN AssessmentsDepartmentMappings PM ON PM.AssessmentID= AT.AssessmentID  WHERE  PM.DepartmentID ='%@'",dID];
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
               AssessmentTitleModel* assessmentTitleObj = [[AssessmentTitleModel alloc]init];
                
                assessmentTitleObj.AssessmentID =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                assessmentTitleObj.AssessmentName=  [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
       
                
                [tempArray addObject:assessmentTitleObj];
            }
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
    return tempArray;
}

@end
