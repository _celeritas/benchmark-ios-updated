//
//  UserLoginModel.h
//  Benchmark
//
//  Created by Admin on 5/24/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserLoginModel : NSObject

@property (strong, nonatomic) NSString *userName;

@property (strong, nonatomic) NSString *userPassword;

@end
