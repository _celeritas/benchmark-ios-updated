//
//  AssessmentResponse.h
//  Benchmark
//
//  Created by Admin on 4/24/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
@interface AssessmentResponse : NSObject

@property (nonatomic ,strong) NSString *responseID;
@property (nonatomic ,strong) NSString *assessmentID;
@property (nonatomic ,strong) NSString *assesmentName;
@property (nonatomic ,strong) NSString *assesmentDate;
@property (nonatomic ,strong) NSString *assessmenTrainer;
@property (nonatomic ,strong) NSString *assessmentQuestion;
@property (nonatomic ,strong) NSString *responseText;
@property (nonatomic ,strong) NSString *sectionName;
@property (nonatomic ,strong) NSString *QuestionID;
@property (nonatomic ,strong) NSString *responseComments;
@property (nonatomic ,strong) NSString *departmentID;
@property (strong , nonatomic) NSData *imageData;
@property (strong , nonatomic) NSString *isImage;
@property (nonatomic ,strong) NSString *imageURL;

@property(strong,nonatomic) NSString *isAnswerSelected;
@property(strong,nonatomic) NSString *isCommentSelected;
@property(strong,nonatomic) NSString *btnCommentSelected;
-(void)getServiceAssementResponseMasterDataWithAssessmentID:(NSString *)assessmentID;

/***new work***/
-(void)getAssessmentResonseMasterData:(NSString *)assessmentID;
-(void)updateIntoAssessmentResponseMaster;
-(void)saveIntoInspiredServiceAssessmentResponseMaster;

-(void)setAssessmentResponceDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray;
-(void)setInspiredServiceDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray ;

-(void)loadAssessmentResponseDataFromJSON:(NSDictionary*)jsonDic;
-(void)loadInspriedServiceDataFromJSON:(NSDictionary*)jsonDic;
@end
