//
//  AssessmentResponse.m
//  Benchmark
//
//  Created by Admin on 4/24/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "AssessmentResponse.h"
#import "AppDelegate.h"
#import "Utility.h"


static sqlite3 *database = nil;
static sqlite3_stmt *addStmt= nil;
static sqlite3_stmt *deletestmt = nil;

@implementation AssessmentResponse {
    AppDelegate *appDelegate;
}

-(id)init {
    
    self = [super init];
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    return self;
    
}


// new
-(void)getAssessmentResonseMasterData:(NSString *)assessmentID {
    // from review
    appDelegate.arrAssessmentResponseMaster  =[NSMutableArray new];
    
    NSString *query1=  [NSString stringWithFormat:@"select * from AssessmentResponseMaster where AssessmentID = '%@'",assessmentID];
    
    const char *sql =[query1 UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                AssessmentResponse *assessmentResponse = [[AssessmentResponse alloc]init];
                
                assessmentResponse.responseID =[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                assessmentResponse.assessmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                assessmentResponse.QuestionID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                
                assessmentResponse.responseText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                
                assessmentResponse.assesmentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,4)];
                
                assessmentResponse.assesmentDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,5)];
                
                assessmentResponse.assessmentQuestion = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,6)];
                
                assessmentResponse.responseComments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                
                assessmentResponse.departmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,8)];
                
                assessmentResponse.sectionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,9)];
                
                assessmentResponse.isImage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,10)];
                if( ![assessmentResponse.isImage isEqualToString:@""]){
                    assessmentResponse.imageURL = [Utility getPhotoFromServer:assessmentResponse.isImage];
                }
                
                [assessmentResponse setIsAnswerSelected:@"0"];
                [assessmentResponse setBtnCommentSelected:@"0"];
                [appDelegate.arrAssessmentResponseMaster addObject:assessmentResponse];
            }
            
            // [tableViewAssessment reloadData];
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
}

-(void)updateIntoAssessmentResponseMaster {
    
    for (int i  = 0; i < appDelegate.arrAssessmentResponseMaster.count; i++)
    {
        AssessmentResponse *assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:i];
        
        // Prepare the query string.
        NSString *query;
        if (assessmentResponse.responseComments.length>0 &&  assessmentResponse.isImage.length==0) {
            
            query =  [NSString stringWithFormat:@"update AssessmentResponseMaster set ResponseText = '%@',ResponseComment = '%@' where ResponseID = %ld", assessmentResponse.responseText, assessmentResponse.responseComments,(long)assessmentResponse.responseID];
        }
        else  if (assessmentResponse.isImage.length>0 && assessmentResponse.responseComments.length==0) {
            
            query =  [NSString stringWithFormat:@"update AssessmentResponseMaster set ResponseText = '%@',ImageData = '%@' where ResponseID = %ld", assessmentResponse.responseText, assessmentResponse.isImage,(long)assessmentResponse.responseID];
        }
        else    if (assessmentResponse.responseComments.length>0 && assessmentResponse.isImage.length>0) {
            
            query =  [NSString stringWithFormat:@"update AssessmentResponseMaster set ResponseText = '%@',ResponseComment = '%@',ImageData = '%@' where ResponseID = %ld", assessmentResponse.responseText, assessmentResponse.responseComments,assessmentResponse.isImage,(long)assessmentResponse.responseID];
        }
        
        else{
            query =  [NSString stringWithFormat:@"update AssessmentResponseMaster set ResponseText = '%@' where ResponseID = %ld", assessmentResponse.responseText, (long)assessmentResponse.responseID];
        }
        
        const char *sql =[query UTF8String];
        
        sqlite3_stmt *selectstmt;
        
        if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
        {
            if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(selectstmt) == SQLITE_DONE)
                {
                    
                    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) != SQLITE_OK)
                    {
                        NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
                    }
                }
                else
                {
                    NSLog(@"DB SUPPORT - ERROR AssessmentResponseMaster UPDATED");
                }
            }
            sqlite3_finalize(selectstmt);
        }
        sqlite3_close(database);
    }
}


-(void)getServiceAssementResponseMasterDataWithAssessmentID:(NSString *)assessmentID {
    
    appDelegate.inspiredServicesResponseMaster =[[NSMutableArray alloc]init];

     NSString *query=  [NSString stringWithFormat:@"select * from InspiredServiceAssessmentResponseMaster where AssessmentID = '%@'",assessmentID];
    const char *sql =[query UTF8String];
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                
                // NSInteger primaryKey = sqlite3_column_int(selectstmt, 0);
                AssessmentResponse *assessmentResponse = [[AssessmentResponse alloc] init];
                assessmentResponse.responseID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                assessmentResponse.assessmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                assessmentResponse.QuestionID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                
                assessmentResponse.responseText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                
                assessmentResponse.assesmentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,4)];
                
                assessmentResponse.assesmentDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,5)];
                
                assessmentResponse.assessmentQuestion = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,6)];
                
                assessmentResponse.responseComments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                
                assessmentResponse.departmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,8)];
                
                assessmentResponse.sectionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,9)];
                
                assessmentResponse.isImage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,10)];
                
                if( ![assessmentResponse.isImage isEqualToString:@""]){
                    assessmentResponse.imageURL = [Utility getPhotoFromServer:assessmentResponse.isImage];
                }
                [assessmentResponse setIsAnswerSelected:@"0"];
                
                [assessmentResponse setBtnCommentSelected:@"0"];
                [appDelegate.inspiredServicesResponseMaster addObject:assessmentResponse];
            }
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
}
//
//here

-(void)deleteResponseMasterListing {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from AssessmentResponseMaster"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &deletestmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(deletestmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadAssessmentResponseDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"assessment_response_master"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                AssessmentResponse  *theObject =[[AssessmentResponse alloc] init];
                NSNumber *rID = [jsonDict valueForKey:@"ResponseID"];
                [theObject setResponseID:[rID stringValue]];

                [theObject setAssessmentID: [jsonDict valueForKey:@"AssessmentID"]];
                [theObject setQuestionID: [jsonDict valueForKey:@"QuestionID"]];
                [theObject setResponseText: [jsonDict valueForKey:@"ResponseText"]];
                [theObject setAssesmentName: [jsonDict valueForKey:@"AssessmentName"]];
                [theObject setAssesmentDate: [jsonDict valueForKey:@"AssessmentDateTime"]];
                [theObject setAssessmentQuestion: [jsonDict valueForKey:@"AssessmentQuestion"]];
                [theObject setResponseComments: [jsonDict valueForKey:@"ResponseComment"]];
                [theObject setDepartmentID: [jsonDict valueForKey:@"DepartmentID"]];
                [theObject setSectionName: [jsonDict valueForKey:@"SectionName"]];
                [theObject setIsImage: [jsonDict valueForKey:@"ImageData"]];
        
                [tArray addObject:theObject];
            }
            [ self deleteResponseMasterListing];
            [self setAssessmentResponceDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}
-(void)setAssessmentResponceDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into AssessmentResponseMaster (ResponseID,AssessmentID,QuestionID,ResponseText,AssessmentName,AssessmentDateTime,AssessmentQuestion,ResponseComment,DepartmentID,SectionName,ImageData) Values(?,?,?,?,?,?,?,?,?,?,?)";;
        appDelegate.serverResponseArray =[NSMutableArray new];
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++) {
                
                AssessmentResponse *assessmentResponse = [pArray objectAtIndex:i];
                  sqlite3_bind_text(addStmt, 1, [assessmentResponse.responseID  UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [assessmentResponse.assessmentID  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 3, [assessmentResponse.QuestionID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 4, [assessmentResponse.responseText  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 5, [assessmentResponse.assesmentName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 6, [assessmentResponse.assesmentDate  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 7, [assessmentResponse.assessmentQuestion UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 8, [assessmentResponse.responseComments  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 9, [assessmentResponse.departmentID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 10, [assessmentResponse.sectionName  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 11, [assessmentResponse.isImage UTF8String], -1, SQLITE_TRANSIENT);
                //assessment responce dictionary utility
                [appDelegate.serverResponseArray addObject:[Utility assessmentResponseMasterObject:assessmentResponse]];
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)deleteInspriedServices {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from InspiredServiceAssessmentResponseMaster"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &deletestmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(deletestmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadInspriedServiceDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"inspired_ervice_assessment_response_master"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                AssessmentResponse  *theObject =[[AssessmentResponse alloc] init];
                NSNumber *rID = [jsonDict valueForKey:@"ResponseID"];
                [theObject setResponseID: [rID stringValue]];

                [theObject setAssessmentID: [jsonDict valueForKey:@"AssessmentID"]];
                [theObject setQuestionID: [jsonDict valueForKey:@"QuestionID"]];
                [theObject setResponseText: [jsonDict valueForKey:@"ResponseText"]];
                [theObject setAssesmentName: [jsonDict valueForKey:@"AssessmentName"]];
                [theObject setAssesmentDate: [jsonDict valueForKey:@"AssessmentDateTime"]];
                [theObject setAssessmentQuestion: [jsonDict valueForKey:@"AssessmentQuestion"]];
                [theObject setResponseComments: [jsonDict valueForKey:@"ResponseComment"]];
                [theObject setDepartmentID: [jsonDict valueForKey:@"DepartmentID"]];
                [theObject setSectionName: [jsonDict valueForKey:@"SectionName"]];
                [theObject setIsImage: [jsonDict valueForKey:@"ImageData"]];
         
                [tArray addObject:theObject];
            }
            [ self deleteInspriedServices];
            [self setInspiredServiceDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}


-(void)setInspiredServiceDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement = @"insert into InspiredServiceAssessmentResponseMaster (ResponseID,AssessmentID,QuestionID,ResponseText,AssessmentName,AssessmentDateTime,AssessmentQuestion,ResponseComment,DepartmentID,SectionName,ImageData)Values(?,?,?,?,?,?,?,?,?,?,?)";
        appDelegate.serverInspiredResponseArray =[NSMutableArray new];
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++) {
                
                AssessmentResponse *assessmentResponse = [pArray objectAtIndex:i];
                 sqlite3_bind_text(addStmt, 1, [assessmentResponse.responseID  UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [assessmentResponse.assessmentID  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 3, [assessmentResponse.QuestionID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 4, [assessmentResponse.responseText  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 5, [assessmentResponse.assesmentName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 6, [assessmentResponse.assesmentDate  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 7, [assessmentResponse.assessmentQuestion UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 8, [assessmentResponse.responseComments  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 9, [assessmentResponse.departmentID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 10, [assessmentResponse.sectionName  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 11, [assessmentResponse.isImage UTF8String], -1, SQLITE_TRANSIENT);
                //assessment responce dictionary utility
                [appDelegate.serverInspiredResponseArray addObject:[Utility inspiredServiceResponseMasterObject:assessmentResponse]];
                
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}

-(void)saveIntoInspiredServiceAssessmentResponseMaster
{
   // [MBProgressHUD showHUDAddedTo:self.view animated:true];
    appDelegate.serverInspiredResponseArray =[NSMutableArray new];

    for (int i  = 0; i < appDelegate.inspiredServicesResponseMaster.count; i++)
    {
        AssessmentResponse *assessmentResponse = [appDelegate.inspiredServicesResponseMaster objectAtIndex:i];
        
        assessmentResponse.assessmentQuestion = [assessmentResponse.assessmentQuestion stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
        // Prepare the query string.
        if (assessmentResponse.isImage.length ==0) {
            assessmentResponse.isImage =@"";
        }
        
        sqlite3_stmt *addStmt =nil;
        NSString * statement = @"insert into InspiredServiceAssessmentResponseMaster (AssessmentID,QuestionID,ResponseText,AssessmentName,AssessmentDateTime,AssessmentQuestion,ResponseComment,DepartmentID,SectionName,ImageData)Values(?,?,?,?,?,?,?,?,?,?)";
        
        if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK) {
            
            if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK)
            {
                  sqlite3_bind_text(addStmt, 1, [assessmentResponse.responseID  UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [assessmentResponse.assessmentID  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 3, [assessmentResponse.QuestionID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 4, [assessmentResponse.responseText  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 5, [assessmentResponse.assesmentName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 6, [assessmentResponse.assesmentDate  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 7, [assessmentResponse.assessmentQuestion UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 8, [assessmentResponse.responseComments  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 9, [assessmentResponse.departmentID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 10, [assessmentResponse.sectionName  UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 11, [assessmentResponse.isImage UTF8String], -1, SQLITE_TRANSIENT);
                //assessment responce dictionary utility
                [appDelegate.serverInspiredResponseArray addObject:[Utility inspiredServiceResponseMasterObject:assessmentResponse]];
                
                if (sqlite3_step(addStmt) == SQLITE_DONE)
                {
                    NSLog(@"DB SUPPORT - InspiredServiceAssessmentResponseMaster INSERTED");
                }
                else
                {
                    NSLog(@"DB SUPPORT - ERROR InspiredServiceAssessmentResponseMaster INSERT");
                    return;
                }
            }
            sqlite3_finalize(addStmt);
        }
        sqlite3_close(database);
    }
    
}

@end
