//
//  AssessmentListingModel.m
//  Benchmark
//
//  Created by Admin on 4/27/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "AssessmentListingModel.h"
#import "AppDelegate.h"
#import "Utility.h"

static sqlite3 *database = nil;
static sqlite3_stmt *deletestmt = nil;
static sqlite3_stmt *addStmt =nil;
@implementation AssessmentListingModel
{
    AppDelegate *appDelegate;
}

-(id)init {
    
    self = [super init];
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    return self;
    
}

-(void)updateIntoAssessmentListing:(NSString *)status :(NSString *)score {
    
    
    // Prepare the query string.
    NSDate * date = [NSDate date];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    
    NSString *assessmentSubmittedDateTime = [dateFormatter stringFromDate:date];
    
    NSString *query = [NSString stringWithFormat:@"update AssessmentListing set AssessmentCurrentStatus = '%@', AssessmentScore = '%@', AssessmentSubmittedDateTime = '%@',AssessmentEmployees = '%@'  where ID = %d",status,score, assessmentSubmittedDateTime,[appDelegate.arrAssessmentEmployees componentsJoinedByString:@","],[appDelegate.reviewID intValue]];
    
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(selectstmt) == SQLITE_DONE)
            {
               // NSLog(@"DB SUPPORT - AssessmentResponseMaster UPDATED");
            }
            else
            {
                NSLog(@"DB SUPPORT - ERROR AssessmentResponseMaster UPDATED");
            }
            sqlite3_finalize(selectstmt);
        }
        sqlite3_close(database);
    }
    appDelegate.reviewID = @"";
}

-(void)getDashboardAssessmentsData {
    NSString *uID= [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    appDelegate.arrDashboardData = [[NSMutableArray alloc]init];
    NSString *query1=  [NSString stringWithFormat:@"select * from AssessmentListing where UserID = %@",uID];
    
    const char *sql =[query1 UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
              AssessmentListingModel *assessmentListingModel = [[AssessmentListingModel alloc]init];
                
                assessmentListingModel.ID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                assessmentListingModel.departmentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                assessmentListingModel.assessmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                
                assessmentListingModel.assessmentDataLaunchDateTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                
                assessmentListingModel.assessmentSubmittedDateTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,4)];
                
                assessmentListingModel.assessmentTrainer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,5)];
                
                assessmentListingModel.assessmentScore = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,6)];
                
                assessmentListingModel.assessmentCurrentStatus = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,7)];
                
                assessmentListingModel.assessmentCommnents = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,8)];
                
                assessmentListingModel.assessmentNameID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,9)];
                
                assessmentListingModel.assessmentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,10)];
                
                assessmentListingModel.assessmentPropertyName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,11)];
                assessmentListingModel.assessmentRoles = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,12)];
                assessmentListingModel.assessmentEmployees = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,13)];
                 assessmentListingModel.assessmentMeal = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,15)];
                 assessmentListingModel.assessmentRoomNO = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,16)];
                [appDelegate.arrDashboardData addObject:assessmentListingModel];
            }
            
            NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
            
            for (NSInteger i = appDelegate.arrDashboardData.count - 1; i >= 0;)
            {
                [arrTemp addObject:[appDelegate.arrDashboardData objectAtIndex:i]];
                i--;
            }
            appDelegate.arrDashboardData = arrTemp;
      
        }
        sqlite3_finalize(selectstmt);
        
    }
    sqlite3_close(database);
}
-(void)deleteAssessmentListing {
    
    if (sqlite3_open([[Utility getDBPath] UTF8String], &database) == SQLITE_OK) {
        
        NSString *query =[NSString stringWithFormat: @"DELETE from AssessmentListing"];
        const char *sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &deletestmt, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(deletestmt) == SQLITE_ROW) {
                
            }
        }
    }
}

-(void)loadAssessmentLisingDataFromJSON:(NSDictionary*)jsonDic {
    
    NSArray *resultDic = [jsonDic objectForKey:@"assessment_listing"];
    NSMutableArray *tArray = [NSMutableArray new];
    
    @try {
        if ([resultDic count] != 0){
            for (int i = 0; i<[resultDic count]; i++)
            {
                id item = [resultDic objectAtIndex:i];
                
                NSDictionary *jsonDict = (NSDictionary *) item;
                AssessmentListingModel  *theObject =[[AssessmentListingModel alloc] init];
                
                NSNumber *aID = [jsonDict valueForKey:@"ID"];
                [theObject setID: [aID stringValue]];

                [theObject setDepartmentName: [jsonDict valueForKey:@"DepartmentName"]];

                [theObject setDepartmentName: [jsonDict valueForKey:@"DepartmentName"]];
                [theObject setAssessmentID: [jsonDict valueForKey:@"AssessmentID"]];
                [theObject setAssessmentDataLaunchDateTime: [jsonDict valueForKey:@"AssessmentDataLaunchDateTime"]];
                [theObject setAssessmentSubmittedDateTime:[jsonDict valueForKey:@"AssessmentSubmittedDateTime"]];
                 [theObject setAssessmentTrainer:[jsonDict valueForKey:@"AssessmentTrainer"]];
                [theObject setAssessmentScore:[jsonDict valueForKey:@"AssessmentScore"]];
                [theObject setAssessmentCurrentStatus:[jsonDict valueForKey:@"AssessmentCurrentStatus"]];
                [theObject setAssessmentCommnents:[jsonDict valueForKey:@"AssessmentCommnents"]];
                 [theObject setAssessmentNameID:[jsonDict valueForKey:@"AssessmentNameID"]];
                 [theObject setAssessmentName:[jsonDict valueForKey:@"AssessmentName"]];
                 [theObject setAssessmentPropertyName:[jsonDict valueForKey:@"AssessmentPropertyName"]];
                 [theObject setAssessmentRoles:[jsonDict valueForKey:@"AssessmentRoles"]];
                 [theObject setAssessmentEmployees:[jsonDict valueForKey:@"AssessmentEmployees"]];
                
                [theObject setUserID:[jsonDict valueForKey:@"UserID"]];
                if ([jsonDict valueForKey:@"MealPeriod"] == [NSNull null]||[jsonDict valueForKey:@"RoomNumber"] == [NSNull null]) {
                    [theObject setAssessmentMeal:@"NA"];
                    [theObject setAssessmentRoomNO:@"NA"];
                }
                else {
               
               [theObject setAssessmentMeal:[jsonDict valueForKey:@"MealPeriod"]];
                [theObject setAssessmentRoomNO:[jsonDict valueForKey:@"RoomNumber"]];
                    
                    [theObject setAssessmentCat1:[jsonDict valueForKey:@"FAndBQuality"]];
                    [theObject setAssessmentCat2:[jsonDict valueForKey:@"ServiceAndGuestQuality"]];
                    [theObject setAssessmentCat3:[jsonDict valueForKey:@"CleanlinessAndCondition"]];
                    [theObject setAssessmentCat4:[jsonDict valueForKey:@"RevenueImpact"]];

                }
                
                [tArray addObject:theObject];
            }
            [ self deleteAssessmentListing];
          [self setAssessmentLsitingDataToLocalDB:[Utility getDBPath] withArray:tArray];
        }
        
    }
    @catch (NSException *exception) {
        
        return;
    }
}

-(void)setAssessmentLsitingDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray {
    //no use
    if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK) {
        NSString* statement;
        
        statement = @"BEGIN EXCLUSIVE TRANSACTION";
        
        if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) != SQLITE_OK) {
            printf("db error: %s\n", sqlite3_errmsg(database));
            // return NO;
        }
        if (sqlite3_step(addStmt) != SQLITE_DONE) {
            sqlite3_finalize(addStmt);
            printf("db error: %s\n", sqlite3_errmsg(database));
            //  return NO;
        }
        
        NSTimeInterval timestampB = [[NSDate date] timeIntervalSince1970];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        statement =@"insert into AssessmentListing (ID,DepartmentName, AssessmentID, AssessmentDataLaunchDateTime, AssessmentSubmittedDateTime, AssessmentTrainer, AssessmentScore, AssessmentCurrentStatus, AssessmentCommnents, AssessmentNameID, AssessmentName, AssessmentPropertyName,AssessmentRoles,AssessmentEmployees,UserID,MealPeriod,RoomNo,FAndBQuality,ServiceAndGuestQuality,CleanlinessAndCondition,RevenueImpact) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        appDelegate.serverListingArray =[NSMutableArray new];
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK) {
            for(int i = 0; i < [pArray count]; i++){
                AssessmentListingModel  *object =[[AssessmentListingModel alloc] init];
                
                object = [pArray objectAtIndex:i];
                sqlite3_bind_text(addStmt, 1, [object.ID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 2, [object.departmentName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 3, [object.assessmentID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 4, [object.assessmentDataLaunchDateTime UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 5, [object.assessmentSubmittedDateTime UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 6, [object.assessmentTrainer UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 7, [object.assessmentScore UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 8, [object.assessmentCurrentStatus UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 9, [object.assessmentCommnents UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 10, [object.assessmentNameID UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 11, [object.assessmentName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 12, [object.assessmentPropertyName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 13, [object.assessmentRoles UTF8String], -1, SQLITE_TRANSIENT);
                
                sqlite3_bind_text(addStmt, 14, [object.assessmentEmployees UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 15, [object.UserID UTF8String], -1, SQLITE_TRANSIENT);
                  sqlite3_bind_text(addStmt, 16, [object.assessmentMeal UTF8String], -1, SQLITE_TRANSIENT);
                  sqlite3_bind_text(addStmt, 17, [object.assessmentRoomNO UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 18, [object.assessmentCat1 UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 19, [object.assessmentCat2 UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 20, [object.assessmentCat3 UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(addStmt, 21, [object.assessmentCat4 UTF8String], -1, SQLITE_TRANSIENT);
                [appDelegate.serverListingArray  addObject:[Utility assessmentListingDataWithModel:object withCatScore1:object.assessmentCat1 withCatScore2:object.assessmentCat2 withCatScore3:object.assessmentCat3 withCatScore4:object.assessmentCat4]];
                
                while(YES){
                    NSInteger result = sqlite3_step(addStmt);
                    if(result == SQLITE_DONE){
                        break;
                    }
                    else if(result != SQLITE_BUSY){
                        printf("db error: %s\n", sqlite3_errmsg(database));
                        break;
                    }
                }
                sqlite3_reset(addStmt);
                
            }
            timestampB = [[NSDate date] timeIntervalSince1970] - timestampB;
            NSLog(@"Insert Time Taken Content: %f",timestampB);
            
            // COMMIT
            statement = @"COMMIT TRANSACTION";
            sqlite3_stmt *commitStatement =nil;
            if (sqlite3_prepare_v2(database, [statement UTF8String], -1, &commitStatement, NULL) != SQLITE_OK) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            if (sqlite3_step(commitStatement) != SQLITE_DONE) {
                printf("db error: %s\n", sqlite3_errmsg(database));
                //  return NO;
            }
            sqlite3_finalize(addStmt);
            sqlite3_finalize(commitStatement);
            //  return YES;
        }
        // return YES;
    }
    sqlite3_close(database);
}
              
-(void)saveIntoAssessmentListing: (NSString *)status : (NSString *)score {
    //no use
    NSDate * date = [NSDate date];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    
    NSString *assessmentSubmittedDateTime = [dateFormatter stringFromDate:date];
    
    AssessmentListingModel *assessmentListing = [[AssessmentListingModel alloc]init];
    AssessmentResponse *assessmentResponse = [appDelegate.arrAssessmentResponseMaster objectAtIndex:0];
    assessmentListing.ID = [NSString stringWithFormat:@"%ld",(long)appDelegate.indexValue];
    assessmentListing.departmentName = assessmentResponse.departmentID;
    assessmentListing.assessmentID = assessmentResponse.assessmentID;
    assessmentListing.assessmentDataLaunchDateTime = assessmentResponse.assesmentDate;
    assessmentListing.assessmentSubmittedDateTime = assessmentSubmittedDateTime;
    assessmentListing.assessmentTrainer = assessmentResponse.assessmenTrainer;
    assessmentListing.assessmentScore = score;
    assessmentListing.assessmentCurrentStatus = status;
    assessmentListing.assessmentCommnents = @"";
    assessmentListing.assessmentNameID = appDelegate.assessmentIDForQuestions;
    assessmentListing.assessmentName = appDelegate.assessmentName;
    assessmentListing.assessmentPropertyName = appDelegate.assessmentPropertyName;
    assessmentListing.assessmentRoles = appDelegate.assessmentRoles ;
    assessmentListing.assessmentEmployees = [appDelegate.arrAssessmentEmployees componentsJoinedByString:@","];
    
    
    //here utility  assessment listing
    sqlite3_stmt *addStmt =nil;
    NSString* statement = @"insert into AssessmentListing (DepartmentName, AssessmentID, AssessmentDataLaunchDateTime, AssessmentSubmittedDateTime, AssessmentTrainer, AssessmentScore, AssessmentCurrentStatus, AssessmentCommnents, AssessmentNameID, AssessmentName, AssessmentPropertyName,AssessmentRoles,AssessmentEmployees) Values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    
    // UTF8String]
    appDelegate.serverListingArray =[NSMutableArray new];
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [statement UTF8String], -1, &addStmt, NULL) == SQLITE_OK)
        {
            
            sqlite3_bind_text(addStmt, 1, [assessmentListing.departmentName  UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 2, [assessmentListing.assessmentID UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 3, [assessmentListing.assessmentDataLaunchDateTime  UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt,4, [assessmentListing.assessmentSubmittedDateTime UTF8String], -1, SQLITE_TRANSIENT);
            
            
            sqlite3_bind_text(addStmt, 5, [assessmentListing.assessmentTrainer  UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 6, [assessmentListing.assessmentScore UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 7, [assessmentListing.assessmentCurrentStatus  UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 8, [assessmentListing.assessmentCommnents  UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 9, [assessmentListing.assessmentNameID UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 10, [assessmentListing.assessmentName  UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 11, [assessmentListing.assessmentPropertyName UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 12, [assessmentListing.assessmentRoles  UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(addStmt, 13, [assessmentListing.assessmentEmployees UTF8String], -1, SQLITE_TRANSIENT);
            
            
            if (sqlite3_step(addStmt) == SQLITE_DONE)
            {
                NSLog(@"DB SUPPORT - AssessmentListing INSERTED");
               // [appDelegate.serverListingArray  addObject:[Utility assessmentListingDataWithModel:assessmentListing]];
            }
            else
            {
                NSLog(@"DB SUPPORT - ERROR AssessmentListing INSERT");
            }
            sqlite3_finalize(addStmt);
            
        }
        sqlite3_close(database);
    }
    [appDelegate.dicAssessmentAnswersArray removeAllObjects];
    [appDelegate.isAssessmentImage removeAllObjects];
    [appDelegate.isAssessmentComment removeAllObjects];
    appDelegate.assessmentPropertyName = @"";
    appDelegate.assessmentIDForQuestions = @"";
    appDelegate.assessmentName = @"";
}

@end
