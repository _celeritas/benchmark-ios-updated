//
//  AssessmentListingModel.h
//  Benchmark
//
//  Created by Admin on 4/27/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface AssessmentListingModel : NSObject

@property (strong, nonatomic)  NSString *ID;
@property (strong, nonatomic) NSString* departmentName;
@property (strong, nonatomic) NSString* assessmentID;
@property (strong, nonatomic) NSString* assessmentDataLaunchDateTime;
@property (strong, nonatomic) NSString* assessmentSubmittedDateTime;
@property (strong, nonatomic) NSString* assessmentTrainer;
@property (strong, nonatomic) NSString* assessmentScore;
@property (strong, nonatomic) NSString* assessmentCurrentStatus;
@property (strong, nonatomic) NSString* assessmentCommnents;
@property (strong, nonatomic) NSString* assessmentNameID;
@property (strong, nonatomic) NSString* assessmentName;
@property (strong, nonatomic) NSString* assessmentPropertyName;
@property (strong, nonatomic) NSString* assessmentRoles;
@property (strong, nonatomic) NSString* assessmentEmployees;
@property (strong, nonatomic) NSString* assessmentRoomNO;
@property (strong, nonatomic) NSString* assessmentCat1;
@property (strong, nonatomic) NSString* assessmentCat2;
@property (strong, nonatomic) NSString* assessmentCat3;
@property (strong, nonatomic) NSString* assessmentCat4;



@property (strong, nonatomic) NSString* assessmentMeal;


@property (strong, nonatomic) NSString* UserID;

-(void)updateIntoAssessmentListing:(NSString *)status :(NSString *)score;
-(void)getDashboardAssessmentsData;

-(void)setAssessmentLsitingDataToLocalDB:(NSString*)dbPath  withArray:(NSMutableArray *)pArray;

-(void)loadAssessmentLisingDataFromJSON:(NSDictionary*)jsonDic;
@end
