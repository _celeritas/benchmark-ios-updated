//
//  QuestionsModel.m
//  Benchmark
//
//  Created by Admin on 4/24/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "QuestionsModel.h"
#import "AppDelegate.h"


static sqlite3 *database = nil;

@implementation QuestionsModel
{
    
    AppDelegate *appDelegate;
}

//@synthesize QuestionID, AssessmentID, SectionName, QuestionText, btnCommentSelected, isAnswerSelected,commentText;

-(id)init {
    self =[super init];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    return self;
}

-(void)getAssessmentWithAllSections:(NSString *)assessmentID {
    
    // group by QuestionText
    appDelegate.arrAssessmentQuestions =[NSMutableArray new];

    NSString *query=  [NSString stringWithFormat:@"select * from AssessmentCategoryTable where AssessmentID = '%@'",assessmentID];
    //group by QuestionText
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
               QuestionsModel *questionsModel = [[QuestionsModel alloc]init];
                
                questionsModel.QuestionID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                questionsModel.AssessmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                questionsModel.SectionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                
                questionsModel.QuestionText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                
                [questionsModel setIsAnswerSelected:@"0"];
                [questionsModel setBtnCommentSelected:@"0"];
                [questionsModel setQuestionAnswer:@""];
                [questionsModel setCommentText:@""];
                [appDelegate.arrAssessmentQuestions addObject:questionsModel];
                
            }
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
}

-(void)getAssementQuestionMasterDataWithSectionName:(NSString *)sectionName {
    // Inspired question
  AppDelegate*  appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.inspiredServicesArray =[[NSMutableArray alloc]init];
  NSString *query=  [NSString stringWithFormat:@"select * from InspiredQuestionMaster"];
    const char *sql =[query UTF8String];
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                
                QuestionsModel *contentObj = [[QuestionsModel alloc] init];
                
                contentObj.QuestionID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                contentObj.AssessmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                contentObj.QuestionText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                   contentObj.SectionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                
                [contentObj setIsAnswerSelected:@"0"];
                [contentObj setBtnCommentSelected:@"0"];
                [contentObj setQuestionAnswer:@""];
                [contentObj setCommentText:@""];
                [appDelegate.inspiredServicesArray addObject:contentObj];
            }
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
}

- (id) initWithPrimaryKey:(NSInteger) pk {
    
    _ID = pk;
    return self;
}

#pragma mark === Database Path ===
- (NSString *) getDBPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    return [documentsDir stringByAppendingPathComponent:@"DBBenchmark.db"];
}
@end
