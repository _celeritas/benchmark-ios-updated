//
//  QuestionsModel.h
//  Benchmark
//
//  Created by Admin on 4/24/18.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
@interface QuestionsModel : NSObject
@property (nonatomic) NSInteger ID;

@property (nonatomic, retain) NSString *QuestionID;
@property (nonatomic, retain) NSString *AssessmentID;
@property (nonatomic, retain) NSString *SectionName;
@property (nonatomic, retain) NSString *QuestionText;

@property (nonatomic, retain) NSString *questionAnswer;

@property(strong,nonatomic) NSString *isAnswerSelected;
@property(strong,nonatomic) NSString *isCommentSelected;
@property(strong,nonatomic) NSString *btnCommentSelected;
@property(strong,nonatomic) NSString *isPictureSelected;
@property (nonatomic, retain) NSString *responseText;
@property(strong,nonatomic) NSString*commentText;
-(void)getAssementQuestionMasterDataWithSectionName:(NSString *)sectionName;
-(void)getAllAssessmentData:(NSString *)assessmentID;

/***new work***/

-(void)getAssessmentWithAllSections:(NSString *)assessmentID;


@end
