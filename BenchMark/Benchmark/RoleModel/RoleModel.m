//
//  RoleModel.m
//  Benchmark
//
//  Created by Mac on 1/4/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import "RoleModel.h"
#import "AppDelegate.h"

static sqlite3 *database = nil;


@implementation RoleModel {
    
    AppDelegate *appDelegate;
}

-(id)init {
    
    self =[super init];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    return self;
}

-(void)getAssessmentWithSelectedRoles:(NSString *)query {
    
    
    appDelegate.arrAssessmentRolesQuestions = [NSMutableArray new];
   // query=  [NSString stringWithFormat:@"select * from AssessmentRoleTable"];
    // = '%@' group by QuestionText ",roleID
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                RoleModel *roleModel = [[RoleModel alloc]init];
                
                roleModel.QuestionID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,0)];
                
                roleModel.AssessmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                roleModel.SectionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
                
                roleModel.QuestionText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,3)];
                
                [appDelegate.arrAssessmentRolesQuestions addObject:roleModel];
            }
          
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
}

-(void)getRolesFromDB {
    
    //Rosh
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.rolesArray =[NSMutableArray new];
    
     NSString *query = @"SELECT  * from RoleMaster order by RoleTitle";
    
    const char *sql =[query UTF8String];
    
    sqlite3_stmt *selectstmt;
    
    if (sqlite3_open([[appDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(selectstmt) == SQLITE_ROW)
            {
                RoleModel*  roleModel = [[RoleModel alloc]init];
                
                roleModel.roleID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,1)];
                
                roleModel.roleTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectstmt,2)];
            
                [appDelegate.rolesArray addObject:roleModel];
                
            }
        }
        sqlite3_finalize(selectstmt);
    }
    sqlite3_close(database);
   
}


@end
