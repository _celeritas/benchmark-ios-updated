//
//  RoleModel.h
//  Benchmark
//
//  Created by Mac on 1/4/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface RoleModel : NSObject
@property (strong, nonatomic) NSString* roleID;
@property (strong, nonatomic) NSString* roleTitle;
@property (strong, nonatomic) NSString* roleColumnName;
-(void)getRolesFromDB;

-(void)getAssessmentWithSelectedRoles:(NSString *)query;

@property (nonatomic, retain) NSString *QuestionID;
@property (nonatomic, retain) NSString *AssessmentID;
@property (nonatomic, retain) NSString *SectionName;
@property (nonatomic, retain) NSString *QuestionText;
@end
