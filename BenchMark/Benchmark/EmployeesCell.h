//
//  EmployeesCell.h
//  Benchmark
//
//  Created by Mac on 6/20/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeesCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIButton *btnDelete;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;
@end
