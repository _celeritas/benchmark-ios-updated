//
//  BMHApis.m
//  Benchmark
//
//  Created by Mac on 7/9/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import "BMHApis.h"
#import "Utility.h"
#import <MBProgressHUD/MBProgressHUD.h>


@implementation BMHApis



-(id) init {
    
    //self = [super init];
    if (self = [super init]) {

    self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    NSSet *set = [NSSet setWithObjects:KAcceptContentTypeJSON,kAcceptContentTypeText,KAcceptContentTypeURLEncode,nil];
    self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [self.sessionManager.responseSerializer setAcceptableContentTypes:set];
        // self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        // self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }

   
    return self;
}

#pragma mark ==== shared instance  ======
+(instancetype)sharedInstance {
    
    static BMHApis *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[BMHApis alloc] init];
    });
    return sharedClient;
}

-(void)requestForGetLogin:(NSString *)email WithPassword:(NSString *)password withController:(UIViewController*)viewController WithSuccessBlock:(void(^)(BOOL success, NSDictionary *result))success failure:(void(^)(NSError * error, NSString *message))failure {
    [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    
    NSDictionary *parameters = @{
                                 @"UserEmailAddress": email,
                                 @"UserPassword" : password
                                 
                                 };
    
    [self.sessionManager POST:kLoginURL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *dict = (NSDictionary *)responseObject;
        if (success) {
            [MBProgressHUD hideHUDForView:viewController.view animated:YES];
            success(true,dict);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:viewController.view animated:YES];

    }];
    
}


-(void)requestForPassword:(NSString *)email viewCOntroller:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    
    [MBProgressHUD showHUDAddedTo:currentViewController.view animated:YES];

    NSDictionary *parameters =
    @{@"UserEmailAddress":email
      };
    [self.sessionManager POST:kForgotPasswordURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        
        if (success)
        {
            [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
            
            success(YES,responseDict);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
     }];
    
    
}

-(void)requestForGetDataFromServerWithViewController:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    
    [MBProgressHUD showHUDAddedTo:currentViewController.view animated:YES];

    NSString *saveDate= [[NSUserDefaults standardUserDefaults] objectForKey:@"SaveUpdatedDate"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSString *date = saveDate.length == 0 ? @"1562931064" :saveDate;
    NSDictionary *parameters =
                                @{@"LastUpdateDate":date
                                  };
    [self.sessionManager POST:kGetAllDataURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        
        if (success)
        {
            [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
            
            success(YES,responseDict);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
     }];
    
}
-(void)requestForGetAssessmentDataFromServerWithViewController:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
    
    [MBProgressHUD showHUDAddedTo:currentViewController.view animated:YES];
    
    NSString *uID= [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSDictionary *parameters =
    @{@"UserID":uID
      };
    [self.sessionManager POST:kGetAssessmentURL parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        
        if (success)
        {
           // [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
            
            success(YES,responseDict);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
     }];
    
}
-(void)requestForSaveDataToServerWithParameter:(NSDictionary*)dict  ViewController:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure {
   
    [MBProgressHUD showHUDAddedTo:currentViewController.view animated:YES];

    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    
    sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    
    [sessionManager POST:kSaveAssessementURL parameters:dict progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         NSDictionary *responseDict = (NSDictionary *)responseObject;
         
         if (success)
         {
            // [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
             success(YES,responseDict);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error)
     {
         [MBProgressHUD hideHUDForView:currentViewController.view animated:YES];
     }];
}

-(void)requestToUpdateAssessemntDataToServerWithParameters:(NSDictionary *)parameters ViewController:(UIViewController*)controller WithSuccessBlock:(void(^)(BOOL success, NSDictionary *result))success failure:(void (^)(NSError *error, NSString *message))failure{
   
    [MBProgressHUD showHUDAddedTo:controller.view animated:YES];

    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    
    sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];

    [sessionManager POST:kUpdateAssessmentURL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success (YES,responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:controller.view animated:YES];

    }];
}

@end
