//
//  BMHApis.h
//  Benchmark
//
//  Created by Mac on 7/9/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface BMHApis : NSObject
@property(nonatomic,strong) AFHTTPSessionManager *sessionManager;
+(instancetype)sharedInstance;

-(void)requestForGetLogin:(NSString *)email WithPassword:(NSString *)password withController:(UIViewController*)viewController WithSuccessBlock: (void (^)(BOOL success, NSDictionary *result))success failure:(void (^)(NSError *error, NSString *message))failure;

-(void)requestForPassword:(NSString *)email viewCOntroller:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;

/////***** Get All Data **********///////////
-(void)requestForGetDataFromServerWithViewController:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;
-(void)requestForSaveDataToServerWithParameter:(NSDictionary*)dict  ViewController:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;
-(void)requestForGetAssessmentDataFromServerWithViewController:(UIViewController *)currentViewController WithSuccessBlock:(void (^)(BOOL success, NSDictionary* result))success failure:(void (^)(NSError *error, NSString *message))failure;


-(void)requestToUpdateAssessemntDataToServerWithParameters:(NSDictionary *)parameters ViewController:(UIViewController*)controller WithSuccessBlock:(void(^)(BOOL success, NSDictionary *result))success failure:(void (^)(NSError *error, NSString *message))failure;

@end
