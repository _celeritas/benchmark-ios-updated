//
//  Utility.m
//  Benchmark
//
//  Created by Mac on 1/10/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import "Utility.h"
#import "GeneralInformationModel.h"


@implementation Utility

NSString *const kAcceptContentTypeText = @"text/html";
NSString *const KAcceptContentTypeJSON = @"application/json";
NSString *const KAcceptContentTypeURLEncode = @"application/x-www-form-urlencoded";
NSString *const kBaseURL =@"http://ec2-3-92-174-152.compute-1.amazonaws.com:9090/api/";
NSString *const kGetAllDataURL = @"get/data";
NSString *const kForgotPasswordURL = @"forgot/password";
NSString *const kSaveAssessementURL = @"assessment/save";
NSString *const kGetAssessmentURL = @"assessment/get";
NSString *const kLoginURL =@"login";
NSString *const kUpdateAssessmentURL= @"assessment/update";

NSString *const PICTURE_BUCKET =@"jamshed";
NSString *const ACCESS_KEY_ID =@"AKIAJ4DDXAATIVJBCJPQ";
NSString *const SECRET_KEY =@"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";


+(NSString *)getDBPath {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    return [documentsDir stringByAppendingPathComponent:@"DBBenchmark.db"];
}

+(void)deshLineWithImageView:(UIImageView *)imgV {
    
    UIBezierPath * path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0.0, 10.0)];
    [path addLineToPoint:CGPointMake(imgV.frame.size.width, 10.0)];
    [path setLineWidth:2.0];
    CGFloat dashes[] = { path.lineWidth, path.lineWidth * 2 };
    [path setLineDash:dashes count:2 phase:0];
    [path setLineCapStyle:kCGLineCapSquare];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(imgV.frame.size.width
                                                      , 10), false, 2);
    //  [[UIColor  colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1]setStroke];
    [[UIColor lightGrayColor]setStroke];
    
    [path stroke];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    imgV.image =image;
    
}
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(NSDictionary *)assessmentListingDataWithModel:(AssessmentListingModel *)assessmentListing withCatScore1:(NSString *)score1 withCatScore2:(NSString *)score2 withCatScore3:(NSString *)score3 withCatScore4:(NSString *)score4  {
    

    NSString *uID =[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];

   NSDictionary *parameters = @{@"DepartmentName": assessmentListing.departmentName ,
   
     @"AssessmentID": assessmentListing.assessmentID,
     @"AssessmentDataLaunchDateTime": assessmentListing.assessmentDataLaunchDateTime,
     @"AssessmentSubmittedDateTime": assessmentListing.assessmentSubmittedDateTime,
     @"AssessmentTrainer":assessmentListing.assessmentTrainer,
     @"AssessmentScore":assessmentListing.assessmentScore ,
     @"AssessmentCurrentStatus":assessmentListing.assessmentCurrentStatus,
     @"AssessmentCommnents":assessmentListing.assessmentCommnents,
     @"AssessmentNameID": assessmentListing.assessmentNameID ,
     @"AssessmentName": assessmentListing.assessmentName ,
     @"AssessmentPropertyName":assessmentListing.assessmentPropertyName ,
     @"AssessmentRoles": assessmentListing.assessmentRoles ,
     @"AssessmentEmployees": assessmentListing.assessmentEmployees,
    @"UserID":uID,
    @"RoomNumber":assessmentListing.assessmentRoomNO,
    @"MealPeriod":assessmentListing.assessmentMeal,
                                @"CleanlinessAndCondition":score1,
                                @"FAndBQuality":score2,
                                @"RevenueImpact":score3,
                                @"ServiceAndGuestQuality":score4
        };
    
    return parameters;
}

+(NSDictionary *)assessmentResponseMasterObject:(AssessmentResponse *)assessmentResponse {
    
    
    NSString *uID =[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *parameters = @{@"AssessmentID": assessmentResponse.assessmentID ,
                                 @"QuestionID": assessmentResponse.QuestionID,
                                 @"ResponseText": assessmentResponse.responseText ,
                                 @"AssessmentName": assessmentResponse.assesmentName,
                                 @"AssessmentDateTime": assessmentResponse.assesmentDate ,
                                 @"AssessmentQuestion": assessmentResponse.assessmentQuestion,
                                 @"ResponseComment": assessmentResponse.responseComments ,
                                 @"DepartmentID": assessmentResponse.departmentID,
                                 @"SectionName":assessmentResponse.sectionName,
                                 @"ImageData":assessmentResponse.isImage ,
                                 @"UserID": uID};
    
    return parameters;
}

+(NSDictionary *)inspiredServiceResponseMasterObject:(AssessmentResponse *)assessmentResponse {
    
    
    NSString *uID =[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    NSDictionary *parameters = @{@"AssessmentID": assessmentResponse.assessmentID ,
                                 @"QuestionID": assessmentResponse.QuestionID,
                                 @"ResponseText": assessmentResponse.responseText ,
                                 @"AssessmentName": assessmentResponse.assesmentName,
                                 @"AssessmentDateTime": assessmentResponse.assesmentDate ,
                                 @"AssessmentQuestion": assessmentResponse.assessmentQuestion,
                                 @"ResponseComment": assessmentResponse.responseComments ,
                                 @"DepartmentID": assessmentResponse.departmentID,
                                 @"SectionName":assessmentResponse.sectionName,
                                 @"ImageData":assessmentResponse.isImage ,
                                 @"UserID": uID};
    

    
    return parameters;
}

+(NSDictionary *)roomGeneralInformationWithAssessID:(NSString *)assessID withRoomNo:(NSString *)roomNo withMeal:(NSString *)meal   {
    
    
    NSString *uID =[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *parameters = @{@"AssessmentID":assessID,
                                 @"RoomNumber":roomNo,
                                 @"MealPeriod":meal,
                                 @"UserID": uID};
    
    
    return parameters;
}

//update
+(NSDictionary *)updateAssessmentListingDataWithModel:(AssessmentListingModel *)assessmentListing withCatScore1:(NSString *)score1 withCatScore2:(NSString *)score2 withCatScore3:(NSString *)score3 withCatScore4:(NSString *)score4 {
    
    
    NSString *uID =[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
 
    NSDictionary *parameters = @{
                                 @"AssessmentID": assessmentListing.assessmentID,
                                @"AssessmentSubmittedDateTime": assessmentListing.assessmentSubmittedDateTime,
                                 @"AssessmentScore":assessmentListing.assessmentScore ,
                            @"AssessmentCurrentStatus":assessmentListing.assessmentCurrentStatus,
                                @"AssessmentEmployees": assessmentListing.assessmentEmployees,
                                 @"UserID":uID ,
                                 @"RoomNumber": assessmentListing.assessmentRoomNO,
                                 @"MealPeriod":assessmentListing.assessmentMeal,
                                 @"CleanlinessAndCondition":score1,
                                 @"FAndBQuality":score2,
                                 @"RevenueImpact":score3,
                                 @"ServiceAndGuestQuality":score4
                                 };
    
    return parameters;
}

+(NSDictionary *)updateAssessmentResponseMasterObject:(AssessmentResponse *)assessmentResponse {
    
    
    NSString *uID =[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *parameters = @{@"AssessmentID": assessmentResponse.assessmentID ,
                                 @"QuestionID": assessmentResponse.QuestionID,
                                 @"ResponseText": assessmentResponse.responseText ,
                                 @"ResponseID": assessmentResponse.responseID,
                                 @"ResponseComment": assessmentResponse.responseComments ,
                                 @"ImageData":assessmentResponse.isImage ,
                                 @"UserID": uID};
    
    return parameters;
}

+(NSDictionary *)updateInspiredServiceResponseMasterObject:(AssessmentResponse *)assessmentResponse {
    
    
    NSString *uID =[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *parameters =  @{@"AssessmentID": assessmentResponse.assessmentID ,
                                  @"QuestionID": assessmentResponse.QuestionID,
                                  @"ResponseText": assessmentResponse.responseText ,
                                  @"ResponseID": assessmentResponse.responseID,
                                  @"ResponseComment": assessmentResponse.responseComments ,
                                  @"ImageData":assessmentResponse.isImage ,
                                  @"UserID": uID};
    
    
    
    return parameters;
}

+(void)setUILabelBorder:(CustomUILabel *)lbl {
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: lbl.bounds byRoundingCorners: (UIRectCornerTopRight | UIRectCornerTopLeft) cornerRadii: CGSizeMake(10, 10)].CGPath;
    maskLayer.frame = lbl.bounds;
    lbl.layer.mask = maskLayer;
}

+(NSString *)getPhotoFromServer:(NSString *)pictureName {
    
    AmazonS3Client *s3;
    s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] ;
    s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    S3ResponseHeaderOverrides *override = [[S3ResponseHeaderOverrides alloc] init] ;
    override.contentType = @"image/jpeg";
    S3GetPreSignedURLRequest *gpsur = [[S3GetPreSignedURLRequest alloc] init] ;
    gpsur.key                     = pictureName;
    gpsur.bucket                  = PICTURE_BUCKET;
    gpsur.expires                 = [NSDate dateWithTimeIntervalSinceNow:(NSTimeInterval) 3600000]; // Added an hour's worth of seconds to the current time.
    gpsur.responseHeaderOverrides = override;
    
    // Get the URL
    NSError *error = nil;
    NSURL *url = [s3 getPreSignedURL:gpsur error:&error];
    return [url absoluteString];
    
    //NSURL*  globeURL = [url copy];
   // NSData *dataImage1 =[NSData dataWithContentsOfURL:globeURL];
    
 
}

+(NSData *)getPhotoFromServer1:(NSString *)pictureName {
    
    AmazonS3Client *s3;
    s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] ;
    s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    S3ResponseHeaderOverrides *override = [[S3ResponseHeaderOverrides alloc] init] ;
    override.contentType = @"image/jpeg";
    S3GetPreSignedURLRequest *gpsur = [[S3GetPreSignedURLRequest alloc] init] ;
    gpsur.key                     = pictureName;
    gpsur.bucket                  = PICTURE_BUCKET;
    gpsur.expires                 = [NSDate dateWithTimeIntervalSinceNow:(NSTimeInterval) 3600000]; // Added an hour's worth of seconds to the current time.
    gpsur.responseHeaderOverrides = override;
    
    // Get the URL
    NSError *error = nil;
    NSURL *url = [s3 getPreSignedURL:gpsur error:&error];
    
    NSURL*  globeURL = [url copy];
    NSData *dataImage1 =[NSData dataWithContentsOfURL:globeURL];
    
    return dataImage1;
}

+(NSString *)setDocumentDirectoryPath:(NSString *)path {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

   return  [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@.png",path]];
    
}

@end
