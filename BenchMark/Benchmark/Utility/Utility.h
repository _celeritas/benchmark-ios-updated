//
//  Utility.h
//  Benchmark
//
//  Created by Mac on 1/10/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "BMHApis.h"
#import "AssessmentListingModel.h"
#import "AssessmentResponse.h"
#import "AssessmentResponse.h"
#import "CustomUILabel.h"
#import <AWSRuntime/AWSRuntime.h>
#import <AWSS3/AWSS3.h>

@interface Utility : NSObject

extern NSString *const kAcceptContentTypeText;
extern NSString *const KAcceptContentTypeJSON;
extern NSString *const KAcceptContentTypeURLEncode;
extern NSString *const kBaseURL;
extern NSString *const kGetAllDataURL;
extern NSString *const kForgotPasswordURL;
extern NSString *const kSaveAssessementURL;
extern NSString *const kGetAssessmentURL;
extern NSString *const kLoginURL;
extern NSString *const kUpdateAssessmentURL;

extern NSString *const ACCESS_KEY_ID;
extern NSString *const SECRET_KEY;
extern NSString *const PICTURE_BUCKET;
+(NSString *)getDBPath;
+(void)deshLineWithImageView:(UIImageView *)imgV;
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString;

//save data
+(NSDictionary *)assessmentResponseMasterObject:(AssessmentResponse *)assessmentResponse;
+(NSDictionary *)inspiredServiceResponseMasterObject:(AssessmentResponse *)assessmentResponse;
//+(NSDictionary *)assessmentListingDataWithModel:(AssessmentListingModel *)assessmentListing;
+(NSDictionary *)assessmentListingDataWithModel:(AssessmentListingModel *)assessmentListing withCatScore1:(NSString *)score1 withCatScore2:(NSString *)score2 withCatScore3:(NSString *)score3 withCatScore4:(NSString *)score4;
+(NSDictionary *)roomGeneralInformationWithAssessID:(NSString *)assessID withRoomNo:(NSString *)roomNo withMeal:(NSString *)meal;

+(NSDictionary *)updateInspiredServiceResponseMasterObject:(AssessmentResponse *)assessmentResponse;
+(NSDictionary *)updateAssessmentResponseMasterObject:(AssessmentResponse *)assessmentResponse
    ;
+(NSDictionary *)updateAssessmentListingDataWithModel:(AssessmentListingModel *)assessmentListing withCatScore1:(NSString *)score1 withCatScore2:(NSString *)score2 withCatScore3:(NSString *)score3 withCatScore4:(NSString *)score4;
//+(NSDictionary *)updateAssessmentListingDataWithModel:(AssessmentListingModel *)assessmentListing;

+(NSData *)getPhotoFromServer1:(NSString *)pictureName;
+(NSString *)getPhotoFromServer:(NSString *)pictureName;
+(NSString *)setDocumentDirectoryPath:(NSString *)path;
@end
