//
//  UIPrintPageRenderer+PDF.h
//  Benchmark
//
//  Created by Mac on 1/9/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPrintPageRenderer (PDF)
- (NSData*) printToPDF;
@end
