#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PDFAction1 : NSObject
{
    UIView *titlesHeaderView;
    UIView *valueCellView;
}

// General Information Section

-(void)accountInformation:(NSString *)narcName :(NSString *)narcID :(NSString *)area :(NSString *)territory :(NSString *)mzrTier;

// Header/Footer & Sections Headers

-(void)generatePDFFile:(UIView *)headerView :(UIView *)valueCell  :(NSString *)pdfFileName;

@property(strong,nonatomic) NSString *file_name;

// Summary Section

@property(strong,nonatomic) NSString *totalJulyAug;
@property(strong,nonatomic) NSString *totalSeptOct;
@property(strong,nonatomic) NSString *totalNovDec;
@property(strong,nonatomic) NSString *totalPtdSales;
@property(strong,nonatomic) NSString *totalPtdSuggested;
@property(strong,nonatomic) NSString *totalRebate;
@property(strong,nonatomic) NSString *totalThreshRebate;
@property(strong,nonatomic) NSString *totalThreshold;
@property(strong,nonatomic) NSString *totalEarlyRebate;
@property(strong,nonatomic) NSString *totalThreshEarlyRebate;
@property(strong,nonatomic) NSString *totalEstRebate;

@property(strong,nonatomic) UILabel *_pdflblCategory;
@property(strong,nonatomic) UILabel *_pdflblJulyAug;
@property(strong,nonatomic) UILabel *_pdflblSeptOct;
@property(strong,nonatomic) UILabel *_pdflblNovToDec;
@property(strong,nonatomic) UILabel *_pdflblPTDSales;
@property(strong,nonatomic) UILabel *_pdflblPTDSuggOrder;
@property(strong,nonatomic) UILabel *_pdflblThreshold;
@property(strong,nonatomic) UILabel *_pdflblRebate;
@property(strong,nonatomic) UILabel *_pdflblThresRebate;
@property(strong,nonatomic) UILabel *_pdflblEarlyRebate;
@property(strong,nonatomic) UILabel *_pdflblThresEarlyRebate;
@property(strong,nonatomic) UILabel *_pdflblEstRebate;

// Summary - With Growth Kicker Section


@property(strong,nonatomic) NSString *growthKickerValue;

@property(strong,nonatomic) UILabel *_pdflblGrowthCategory;
@property(strong,nonatomic) UILabel *_pdflblGrowthJulyAug;
@property(strong,nonatomic) UILabel *_pdflblGrowthSeptOct;
@property(strong,nonatomic) UILabel *_pdflblGrowthNovToDec;
@property(strong,nonatomic) UILabel *_pdflblGrowthPTDSales;
@property(strong,nonatomic) UILabel *_pdflblGrowthPTDSuggOrder;
@property(strong,nonatomic) UILabel *_pdflblGrowthThreshold;
@property(strong,nonatomic) UILabel *_pdflblGrowthKicker;
@property(strong,nonatomic) UILabel *_pdflblGrowthRebate;
@property(strong,nonatomic) UILabel *_pdflblGrowthThresRebate;
@property(strong,nonatomic) UILabel *_pdflblGrowthEarlyRebate;
@property(strong,nonatomic) UILabel *_pdflblGrowthThresEarlyRebate;
@property(strong,nonatomic) UILabel *_pdflblGrowthEstRebate;


@property(strong,nonatomic) UIView *bottomUIView;

-(void)summaryDetails:(NSArray *)_julyAug :(NSArray *)_sepOct :(NSArray *)_novDec :(NSArray *)_ptdSales :(NSArray *)_ptdSuggestedOrder :(NSArray *)_threshold;

-(void)summaryRebateDetails:(NSArray *)_rebate :(NSArray *)_thresholdRebate :(NSArray *)_earlyRebate :(NSArray *)_ThresholdEarlyRebate :(NSArray *)_EstimatedRebate;

// Order Details Section
@property(strong,nonatomic) UIView *orderTitlesHeader;
@property(strong,nonatomic) UIView *orderValuesCell;
@property(strong,nonatomic) UIView *orderCategoryHeading;
@property(strong,nonatomic) UIView *orderBottomUIView;

@property(strong,nonatomic) UILabel *_pdflblOrderCategory;
@property(strong,nonatomic) UILabel *_pdflblOrderTotalCost;

@property(strong,nonatomic) UILabel *_pdflblProducts;
@property(strong,nonatomic) UILabel *_pdflblSKU;
@property(strong,nonatomic) UILabel *_pdflblSize;
@property(strong,nonatomic) UILabel *_pdflblOrderJuly;
@property(strong,nonatomic) UILabel *_pdflblOrderSept;
@property(strong,nonatomic) UILabel *_pdflblOrderNov;
@property(strong,nonatomic) UILabel *_pdflblOrderPTD;
@property(strong,nonatomic) UILabel *_pdflblOrderEarlyRebate;
@property(strong,nonatomic) UILabel *_pdflblOrderSeptOct;
@property(strong,nonatomic) UILabel *_pdflblOrderPrice;
@property(strong,nonatomic) UILabel *_pdflblOrderCost;

@property(nonatomic) int orderCatBiologicalsCount;
@property(nonatomic) int orderCatParasiticidesCount;
@property(nonatomic) int orderCatSynovexCount;
@property(nonatomic) int orderCatDairyCount;
@property(nonatomic) int orderEqBiologicalsCount;
@property(nonatomic) int orderEqParasiticidesCount;

-(void)orderDetails:(NSArray *)_catBiologicals :(NSArray *)_catParas :(NSArray *)_catSynovex :(NSArray *)_catDairy :(NSArray *)_eqBiologicals :(NSArray *)_eqParas;

@property(strong,nonatomic) NSArray *categoryTotalCost;

@end
