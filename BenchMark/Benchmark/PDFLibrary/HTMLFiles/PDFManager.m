//
//  PDFManager.m
//  Benchmark
//
//  Created by Mac on 8/29/19.
//  Copyright © 2019 Celeritas. All rights reserved.
//

#define kPaperSizeA4 CGSizeMake(595.2,841.8)

#define CELL_LABEL_HEIGHT 40
#define CELL_TABLECELLL_HEIGHT 60
#define LABEL_LEADING 8
#define TABLEVIEW_LEADING 0
#define FONT_SIZE 18
#define FONT_NAME @"Roboto-Bold"

#import "PDFManager.h"

@implementation PDFManager

@end
