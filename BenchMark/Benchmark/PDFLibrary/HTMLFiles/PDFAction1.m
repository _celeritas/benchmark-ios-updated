#import "PDFAction1.h"
//#import <OrderBuilderTool-Swift.h>


@interface PDFAction1 ()
{
    CGSize pagSize;
    UIView *viewLoading;
    int maxHeightToFooter, pageNumber;
    float yQuestions;
    NSString *farmId;
    
    NSMutableArray *selectedCategories;
    
    // Fonts
    UIFont *fntTitleBold, *fntTitle, *fntSubtitle, *fntFarm, *fntPreTitle, *fntQuestion, *fntResult1, *fntResult2, *fntResultSmall, *fntPager;
    // Colors
    UIColor *colorBackground, *colorTitle, *colorGreen, *colorLightGreen, *colorBlack, *colorRed, *colorOrange, *colorWhite, *colorBar, *colorBorderResult;
    // Image results
    UIImage *imgTotal, *imgExternal, *imgInternal;
    
    int totalPoints, maxPoints, externalTotalPoints, internalTotalPoints, maxExternalPoints, maxInternalPoints, finalScore, finalExternalScore, finalInternalScore;
    
    NSArray *categoryTitles;
    
    NSMutableArray *accountInfoArray;
    
    NSArray *_SumJulyAugArray;
    NSArray *_SumSepOctArray;
    NSArray *_SumNovDecArray;
    NSArray *_SumPTDArray;
    NSArray *_SumPTDSuggOrderArray;
    
    NSArray *_SumThresholdArray;
    
    NSArray *_SumRebate;
    NSArray *_SumThresholdRebate;
    NSArray *_SumEarlyRebate;
    NSArray *_SumThreshEarlyRebate;
    NSArray *_SumEstimatedRebate;
    
    NSArray *_orderCattleBio;
    NSArray *_orderCattleParas;
    NSArray *_orderCattleSynovex;
    NSArray *_orderCattleDairy;
    NSArray *_orderEquineBio;
    NSArray *_orderEquineParas;

   // OrderDetailsModel *orderDetailsModel;
}

@end

@implementation PDFAction1

/*-(void)orderDetails:(NSArray *)_catBiologicals :(NSArray *)_catParas :(NSArray *)_catSynovex :(NSArray *)_catDairy :(NSArray *)_eqBiologicals :(NSArray *)_eqParas {
    _orderCattleBio = [[NSArray alloc] init];
    _orderCattleParas = [[NSArray alloc] init];
    _orderCattleSynovex = [[NSArray alloc] init];
    _orderCattleDairy = [[NSArray alloc] init];
    _orderEquineBio = [[NSArray alloc] init];
    _orderEquineParas = [[NSArray alloc] init];

    _orderCattleBio = _catBiologicals;
    _orderCattleParas = _catParas;
    _orderCattleSynovex = _catSynovex;
    _orderCattleDairy = _catDairy;
    _orderEquineBio = _eqBiologicals;
    _orderEquineParas = _eqParas;
}

-(void)summaryDetails:(NSArray *)_julyAug :(NSArray *)_sepOct :(NSArray *)_novDec :(NSArray *)_ptdSales :(NSArray *)_ptdSuggestedOrder :(NSArray *)_threshold
{
    _SumJulyAugArray = [[NSArray alloc] init];
    _SumSepOctArray = [[NSArray alloc] init];
    _SumNovDecArray = [[NSArray alloc] init];
    _SumPTDArray = [[NSArray alloc] init];
    _SumPTDSuggOrderArray = [[NSArray alloc] init];
    _SumThresholdArray = [[NSArray alloc] init];
    
    _SumJulyAugArray = _julyAug;
    _SumSepOctArray = _sepOct;
    _SumNovDecArray = _novDec;
    _SumPTDArray = _ptdSales;
    _SumPTDSuggOrderArray = _ptdSuggestedOrder;
    _SumThresholdArray = _threshold;
    
}

-(void)summaryRebateDetails:(NSArray *)_rebate :(NSArray *)_thresholdRebate :(NSArray *)_earlyRebate :(NSArray *)_ThresholdEarlyRebate :(NSArray *)_EstimatedRebate {
    
    _SumRebate = [[NSArray alloc] init];
    _SumThresholdRebate = [[NSArray alloc] init];
    _SumEarlyRebate = [[NSArray alloc] init];
    _SumThreshEarlyRebate = [[NSArray alloc] init];
    _SumEstimatedRebate = [[NSArray alloc] init];
    
    _SumRebate = _rebate;
    _SumThresholdRebate = _thresholdRebate;
    _SumEarlyRebate = _earlyRebate;
    _SumThreshEarlyRebate = _ThresholdEarlyRebate;
    _SumEstimatedRebate = _EstimatedRebate;
    
}

-(void)accountInformation:(NSString *)narcName :(NSString *)narcID :(NSString *)area :(NSString *)territory :(NSString *)mzrTier {
    accountInfoArray = [[NSMutableArray alloc] init];
    [accountInfoArray addObject:narcName];
    [accountInfoArray addObject:narcID];
    [accountInfoArray addObject:area];
    [accountInfoArray addObject:territory];
    [accountInfoArray addObject:mzrTier];

}

-(void)generatePDFFile:(UIView *)headerView :(UIView *)valueCell  :(NSString *)pdfFileName
{
    
    categoryTitles = [NSArray arrayWithObjects:@"CATTLE BIOLOGICALS",@"CATTLE PARASITICIDES",@"CATTLE REPROS/SYNOVEX",@"DAIRY IMMS",@"EQUINE BIOLOGICALS",@"EQUINE PARASITICIDES",@"TOTAL", nil];
    titlesHeaderView = headerView;
    valueCellView = valueCell;
    pagSize = CGSizeMake(595, 842);
    
    maxHeightToFooter = pagSize.height-80;
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    NSString *pdfName = [NSString stringWithFormat:@"%@", pdfFileName];
    NSString *pdfPath = [documentDirectory
                         stringByAppendingPathComponent:pdfName];
    
    [self loadResources];
    [self generateDocument:pdfPath];
}

-(void)generateDocument:(NSString *) filePath {
    UIGraphicsBeginPDFContextToFile(filePath, CGRectZero, nil); // PDF Start
    
    // First Page
    pageNumber=1;
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
    [self drawBackground];
    [self drawHeaderFooter:pageNumber];
    
    if ([self.growthKickerValue isEqualToString: @""] || [self.growthKickerValue isEqualToString: @"$ 0"]) {
        [self tableLayoutDesign];
    } else {
        [self tableLayoutGrowthKickerDesign];

    }
    
    UIGraphicsEndPDFContext(); // PDF End
}

-(void) drawBackground {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, pagSize.width, pagSize.height);
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    CGContextFillRect(context, rect);
}

// Header, footer and pager
-(void) drawHeaderFooter:(int)pageNmbr {
    // Header
    float widthHeader = pagSize.width;
    float heightHeader = 80;
    float xHeader = 0;
    float yHeader = 0;
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    //    [PDFAction1 SetGradientColorToUIView:viewH];
    [viewH setBackgroundColor:[UIColor colorWithRed:23.0f/255.0f green:28.0f/255.0f blue:140.0f/255.0f alpha:1.0f]];
    [viewH drawRect:imageHeader];
    
    UILabel *lblHeading = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 300, viewH.bounds.size.height)];
    [lblHeading setBackgroundColor:[UIColor clearColor]];
    [lblHeading setTextColor:[UIColor whiteColor]];
    //    [lblHeading setText:[NSString stringWithFormat:@"%@ %@",petObj.petName,userName]];
    [lblHeading setText:[NSString stringWithFormat:@"%@",@"FALL PROGRAM ORDER BUILDER FOR CATTLE & EQUINE"]];
    [lblHeading setNumberOfLines:0];
    //    [lblHeading setFont:[UIFont fontWithName:@"OpenSans-Bold.ttf" size:64.0f]];
    [lblHeading setFont:[UIFont fontWithName:@"GOTHAMXNARROW-MEDIUM" size:20.0]];
    [lblHeading setTextAlignment:NSTextAlignmentLeft];
    [viewH addSubview:lblHeading];
    
    UIImageView *imgHeading = [[UIImageView alloc] initWithFrame:CGRectMake(viewH.bounds.size.width - 100, 10, 70, 47)];
    [imgHeading setBackgroundColor:[UIColor clearColor]];
    [imgHeading setImage:[UIImage imageNamed:@"zoetisLogo.png"]];
    [imgHeading setContentMode:UIViewContentModeScaleAspectFit];
    [viewH addSubview:imgHeading];
    
    UIGraphicsBeginImageContextWithOptions(viewH.frame.size, NO, 0.0);
    struct CGContext *context = UIGraphicsGetCurrentContext();
    [viewH.layer renderInContext:context];
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [snapshotImageFromMyView drawInRect:imageHeader];
    
    // Footer
    float widthFooter = pagSize.width;
    float heightFooter = 60;
    float xFooter = 0;
    float yFooter = 782;
    
    CGRect imageFooter = CGRectMake(xFooter, yFooter, widthFooter, heightFooter);
    
    UIView *viewF = [[UIView alloc] initWithFrame:imageFooter];
    [viewF setBackgroundColor:[UIColor whiteColor]];
    [viewF drawRect:imageFooter];
    
    UIView *lineBreak = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewF.bounds.size.width, 2)];
    [lineBreak setBackgroundColor:[UIColor lightGrayColor]];
    [viewF addSubview:lineBreak];
    
    UILabel *lblFooter = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 500, viewF.bounds.size.height)];
    [lblFooter setBackgroundColor:[UIColor clearColor]];
    [lblFooter setTextColor:[UIColor darkGrayColor]];
    [lblFooter setFont:[UIFont fontWithName:@"GothamNarrow-Medium" size:12.0f]];
    [lblFooter setTextAlignment:NSTextAlignmentCenter];
    [lblFooter setText:@"© 2019 Zoetis, All Rights Reserved"];
    //    [lblFooter setCenter:viewF.center];
    CGPoint points = CGPointMake(viewF.center.x, 30);
    [lblFooter setCenter:points];
    [viewF addSubview:lblFooter];
    
    
    UIGraphicsBeginImageContextWithOptions(viewF.frame.size, NO, 0.0);
    struct CGContext *contextF = UIGraphicsGetCurrentContext();
    [viewF.layer renderInContext:contextF];
    UIImage *snapshotImageFooter = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [snapshotImageFooter drawInRect:imageFooter];
    
    // Pager
    //    NSString *txt = [NSString stringWithFormat:@"%d",pageNmbr];
    NSString *txt = [NSString stringWithFormat:@"%d",pageNmbr];
    CGSize size = [txt sizeWithAttributes: @{NSFontAttributeName: fntPager}];
    float width = size.width;
    float height = size.height;
    float x = pagSize.width-(20+width);
    float y = pagSize.height-(20+height);
    
    CGRect txtSize= CGRectMake(x, y, width, height);
    
    NSDictionary *textAttributes = @{NSFontAttributeName:fntPager,NSForegroundColorAttributeName:[UIColor darkGrayColor]};
    
    [txt drawWithRect:txtSize options:NSStringDrawingUsesLineFragmentOrigin attributes:textAttributes context:[[NSStringDrawingContext alloc] init]];
}

-(float) generateTitle:(float)y Index:(int)index {
    float widthHeader = pagSize.width;
    float heightHeader = 40;
    float xHeader = 0;
    float yHeader = 80;
    NSString *lblString = @"ACCOUNT INFORMATION";
    if (index == 1) {
        yHeader =  y;
        lblString = @"PROGRAM SUMMARY";
    }
    else if (index == 9) {
        yHeader =  y;
        lblString = @"*Completion of order form in no way guarantees rebate. Rebate provided as an estimate and may vary from final calculated rebate due.";
    }
    else if (index == 10) {
        yHeader =  y;
        lblString = @"ORDER DETAILS";
    }
    
    CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
    UIView *viewH = [[UIView alloc] initWithFrame:imageHeader];
    [viewH drawRect:imageHeader];
    
    UILabel *lblHeading = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, widthHeader-100, viewH.bounds.size.height)];
    [lblHeading setBackgroundColor:[UIColor clearColor]];
    [lblHeading setTextColor:[UIColor darkGrayColor]];
    [lblHeading setText:lblString];
    [lblHeading setNumberOfLines:0];
    if (index == 9) {
        [lblHeading setFont:[UIFont fontWithName:@"GothamNarrow-Book" size:10.0]];
    }
    else {
        [lblHeading setFont:[UIFont fontWithName:@"GOTHAMXNARROW-BOLD" size:14.0]];
    }
    [lblHeading setTextAlignment:NSTextAlignmentLeft];
    [viewH addSubview:lblHeading];
    
    UIGraphicsBeginImageContextWithOptions(viewH.frame.size, NO, 0.0);
    struct CGContext *context = UIGraphicsGetCurrentContext();
    [viewH.layer renderInContext:context];
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [snapshotImageFromMyView drawInRect:imageHeader];
    
    y = y + viewH.frame.size.height;
    
    return y;
}

// With growth Kicker
-(void) tableLayoutGrowthKickerDesign {
    float x = 20;
    float width = pagSize.width-20;
    float y = 100;
    float heightSum = 0;
    
    int heightCounter = 100;
    BOOL isGIExists = false;
    if ([selectedCategories containsObject:@"General Information"]) {
        isGIExists = true;
    }
    
    for (int i=0; i <= 10  ; i++) {
        
        // Header
        if (i == 0) {
            y = [self generateTitle:y Index:i];
            heightCounter = [self GeneralInformation:width :heightSum :x :y :colorBorderResult];
            y =   heightCounter;
            
        } else if (i == 1) {
            y = [self generateTitle:y Index:i];
            float widthHeader = pagSize.width;
            float heightHeader = 70;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 70 ;
            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [titlesHeaderView drawRect:imageHeader];
            [self setupValuesView:imageHeader :titlesHeaderView];
        }
        else if (i > 1 && i < 9) {
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y  + 30;
            y =   yHeader ;
            UIColor *color = [UIColor darkGrayColor];
            if (i == 8) {
                color = [UIColor whiteColor];
                [valueCellView setBackgroundColor:[UIColor lightGrayColor]];
            }
            else {
                [valueCellView setBackgroundColor:[UIColor whiteColor]];
            }
            
            [self._pdflblGrowthCategory setTextColor:color];
            [self._pdflblGrowthJulyAug setTextColor:color];
            [self._pdflblGrowthSeptOct setTextColor:color];
            [self._pdflblGrowthNovToDec setTextColor:color];
            [self._pdflblGrowthPTDSales setTextColor:color];
            [self._pdflblGrowthPTDSuggOrder setTextColor:color];
            [self._pdflblGrowthKicker setTextColor:[UIColor darkGrayColor]];
            [self._pdflblGrowthThreshold setTextColor:color];
            [self._pdflblGrowthRebate setTextColor:color];
            [self._pdflblGrowthThresRebate setTextColor:color];
            [self._pdflblGrowthEarlyRebate setTextColor:color];
            [self._pdflblGrowthThresEarlyRebate setTextColor:color];
            [self._pdflblGrowthEstRebate setTextColor:color];

            self._pdflblGrowthCategory.text = [NSString stringWithFormat:@"%@",categoryTitles[i-2]];
            
            float tempJulyAug = [[NSString stringWithFormat:@"%@",_SumJulyAugArray[i-2]] floatValue];
            self._pdflblGrowthJulyAug.text = [NSString stringWithFormat:@"%.0f",tempJulyAug];
            
            float tempSepOct = [[NSString stringWithFormat:@"%@",_SumSepOctArray[i-2]] floatValue];
            self._pdflblGrowthSeptOct.text = [NSString stringWithFormat:@"%.0f",tempSepOct];
            
            float tempNovDec = [[NSString stringWithFormat:@"%@",_SumNovDecArray[i-2]] floatValue];
            self._pdflblGrowthNovToDec.text = [NSString stringWithFormat:@"%.0f",tempNovDec];
            
            float tempPTDSales = [[NSString stringWithFormat:@"%@",_SumPTDArray[i-2]] floatValue];
            self._pdflblGrowthPTDSales.text = [NSString stringWithFormat:@"%.0f",tempPTDSales];
            
            NSString *tempPTDSuggOrder = [NSString stringWithFormat:@"%@",_SumPTDSuggOrderArray[i-2]];
            self._pdflblGrowthPTDSuggOrder.text = tempPTDSuggOrder;
            
            NSString *tempThreshold = [NSString stringWithFormat:@"%@",_SumThresholdArray[i-2]];
            if (i == 8) {
                self._pdflblGrowthThreshold.text = self.totalThreshold;
                [self.orderValuesCell setBackgroundColor:[UIColor darkGrayColor]];
            }
            else {
                self._pdflblGrowthThreshold.text = tempThreshold;
            }

            if (i == 5 || i == 8) {

                [self._pdflblGrowthKicker setTextColor:[UIColor whiteColor]];
                self._pdflblGrowthKicker.text = [NSString stringWithFormat:@"%@",self.growthKickerValue];
            } else {
                self._pdflblGrowthKicker.text = [NSString stringWithFormat:@""];
            }

            self._pdflblGrowthRebate.text = [NSString stringWithFormat:@"%@",_SumRebate[i-2]];
            self._pdflblGrowthThresRebate.text = [NSString stringWithFormat:@"%@",_SumThresholdRebate[i-2]];
            self._pdflblGrowthEarlyRebate.text = [NSString stringWithFormat:@"%@",_SumEarlyRebate[i-2]];
            self._pdflblGrowthThresEarlyRebate.text = [NSString stringWithFormat:@"%@",_SumThreshEarlyRebate[i-2]];
            self._pdflblGrowthEstRebate.text = [NSString stringWithFormat:@"%@",_SumEstimatedRebate[i-2]];
            
            
            if (i == 8) {
                
                self._pdflblGrowthJulyAug.text = [NSString stringWithFormat:@"%@",self.totalJulyAug];
                
                self._pdflblGrowthSeptOct.text = [NSString stringWithFormat:@"%@",self.totalSeptOct];
                
                self._pdflblGrowthNovToDec.text = [NSString stringWithFormat:@"%@",self.totalNovDec];
                
                self._pdflblGrowthPTDSales.text = [NSString stringWithFormat:@"%@",self.totalPtdSales];
                
                self._pdflblGrowthPTDSuggOrder.text = [NSString stringWithFormat:@"%@",self.totalPtdSuggested];


                self._pdflblGrowthRebate.text = [NSString stringWithFormat:@"%@",self.totalRebate];
                self._pdflblGrowthThresRebate.text = [NSString stringWithFormat:@"%@",self.totalThreshRebate];
                self._pdflblGrowthEarlyRebate.text = [NSString stringWithFormat:@"%@",self.totalEarlyRebate];
                self._pdflblGrowthThresEarlyRebate.text = [NSString stringWithFormat:@"%@",self.totalThreshEarlyRebate];
                self._pdflblGrowthEstRebate.text = [NSString stringWithFormat:@"%@",self.totalEstRebate];

            }
            
            
            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [valueCellView drawRect:imageHeader];
            [self setupValuesView:imageHeader :valueCellView];
        }
        else if (i == 9) {
            y = y + 40;
            y = [self generateTitle:y Index:i];
        }
        else if (i == 10) {
            y = [self generateTitle:y Index:i];
            float widthHeader = pagSize.width;
            float heightHeader = 70;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 70 ;
            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderTitlesHeader drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderTitlesHeader];
        }
        
        yQuestions = y+170;
        
        if (yQuestions >= (maxHeightToFooter)) {
            if (i != 4) {
                yQuestions = 0;
                pageNumber++;
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                [self drawBackground];
                [self drawHeaderFooter:pageNumber];
                heightCounter = 100;
                y = heightCounter;
            }
        }
    }
    
    float bioYAxis = [self orderBiologicalsColumns:y];
    float paraYAxis = [self orderParasiticidesColumns:bioYAxis];
    float synovexYAxis = [self orderSynovexColumns:paraYAxis];
    float dairyYAxis = [self orderDairyColumns:synovexYAxis];
    float eqBioYAxis = [self orderEqBiologicalsColumns:dairyYAxis];
    [self orderEqParasiticidesColumns:eqBioYAxis];
    
}

-(void) tableLayoutDesign {
    float x = 20;
    float width = pagSize.width-20;
    float y = 100;
    float heightSum = 0;
    
    int heightCounter = 100;
    BOOL isGIExists = false;
    if ([selectedCategories containsObject:@"General Information"]) {
        isGIExists = true;
    }
    
    for (int i=0; i <= 10  ; i++) {
        
        // Header
        if (i == 0) {
            y = [self generateTitle:y Index:i];
            heightCounter = [self GeneralInformation:width :heightSum :x :y :colorBorderResult];
            y =   heightCounter;
            
        } else if (i == 1) {
            y = [self generateTitle:y Index:i];
            float widthHeader = pagSize.width;
            float heightHeader = 80;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 40 ;
            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [titlesHeaderView drawRect:imageHeader];
            [self setupValuesView:imageHeader :titlesHeaderView];
        }
        else if (i > 1 && i < 9) {
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y  + 30;
            y =   yHeader ;
            UIColor *color = [UIColor darkGrayColor];
            if (i == 8) {
                color = [UIColor whiteColor];
                [valueCellView setBackgroundColor:[UIColor lightGrayColor]];
                [self.bottomUIView setHidden:false];
                [self.bottomUIView setHidden:NO];
                [self.bottomUIView setBackgroundColor:[UIColor lightGrayColor]];
            }
            else {
                [self.bottomUIView setBackgroundColor:[UIColor whiteColor]];
                [self.bottomUIView setHidden:YES];
                [valueCellView setBackgroundColor:[UIColor whiteColor]];
            }
            
            
            [self._pdflblCategory setTextColor:color];
            [self._pdflblJulyAug setTextColor:color];
            [self._pdflblSeptOct setTextColor:color];
            [self._pdflblNovToDec setTextColor:color];
            [self._pdflblPTDSales setTextColor:color];
            [self._pdflblPTDSuggOrder setTextColor:color];
            [self._pdflblThreshold setTextColor:color];
            [self._pdflblRebate setTextColor:color];
            [self._pdflblThresRebate setTextColor:color];
            [self._pdflblEarlyRebate setTextColor:color];
            [self._pdflblThresEarlyRebate setTextColor:color];
            [self._pdflblEstRebate setTextColor:color];

            self._pdflblCategory.text = [NSString stringWithFormat:@"%@",categoryTitles[i-2]];
            
            float tempJulyAug = [[NSString stringWithFormat:@"%@",_SumJulyAugArray[i-2]] floatValue];
            self._pdflblJulyAug.text = [NSString stringWithFormat:@"%.0f",tempJulyAug];
            
            float tempSepOct = [[NSString stringWithFormat:@"%@",_SumSepOctArray[i-2]] floatValue];
            self._pdflblSeptOct.text = [NSString stringWithFormat:@"%.0f",tempSepOct];
            
            float tempNovDec = [[NSString stringWithFormat:@"%@",_SumNovDecArray[i-2]] floatValue];
            self._pdflblNovToDec.text = [NSString stringWithFormat:@"%.0f",tempNovDec];
            
            float tempPTDSales = [[NSString stringWithFormat:@"%@",_SumPTDArray[i-2]] floatValue];
            self._pdflblPTDSales.text = [NSString stringWithFormat:@"%.0f",tempPTDSales];
            
            NSString *tempPTDSuggOrder = [NSString stringWithFormat:@"%@",_SumPTDSuggOrderArray[i-2]];
            self._pdflblPTDSuggOrder.text = tempPTDSuggOrder;
            
            NSString *tempThreshold = [NSString stringWithFormat:@"%@",_SumThresholdArray[i-2]];
            if (i == 8) {
                self._pdflblThreshold.text = self.totalThreshold;
                [self.orderValuesCell setBackgroundColor:[UIColor darkGrayColor]];
            }
            else {
                self._pdflblThreshold.text = tempThreshold;
            }
            
            self._pdflblRebate.text = [NSString stringWithFormat:@"%@",_SumRebate[i-2]];
            self._pdflblThresRebate.text = [NSString stringWithFormat:@"%@",_SumThresholdRebate[i-2]];
            self._pdflblEarlyRebate.text = [NSString stringWithFormat:@"%@",_SumEarlyRebate[i-2]];
            self._pdflblThresEarlyRebate.text = [NSString stringWithFormat:@"%@",_SumThreshEarlyRebate[i-2]];
            self._pdflblEstRebate.text = [NSString stringWithFormat:@"%@",_SumEstimatedRebate[i-2]];
            
            if (i == 8) {
                
                self._pdflblJulyAug.text = [NSString stringWithFormat:@"%@",self.totalJulyAug];
                
                self._pdflblSeptOct.text = [NSString stringWithFormat:@"%@",self.totalSeptOct];
                
                self._pdflblNovToDec.text = [NSString stringWithFormat:@"%@",self.totalNovDec];
                
                self._pdflblPTDSales.text = [NSString stringWithFormat:@"%@",self.totalPtdSales];
                self._pdflblPTDSuggOrder.text = [NSString stringWithFormat:@"%@",self.totalPtdSuggested];
                self._pdflblRebate.text = [NSString stringWithFormat:@"%@",self.totalRebate];
                self._pdflblThresRebate.text = [NSString stringWithFormat:@"%@",self.totalThreshRebate];
                self._pdflblEarlyRebate.text = [NSString stringWithFormat:@"%@",self.totalEarlyRebate];
                self._pdflblThresEarlyRebate.text = [NSString stringWithFormat:@"%@",self.totalThreshEarlyRebate];
                self._pdflblEstRebate.text = [NSString stringWithFormat:@"%@",self.totalEstRebate];
                
            }
            
            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [valueCellView drawRect:imageHeader];
            [self setupValuesView:imageHeader :valueCellView];
        }
        else if (i == 9) {
            y = y + 40;
            y = [self generateTitle:y Index:i];
        }
        else if (i == 10) {
            y = [self generateTitle:y Index:i];
            float widthHeader = pagSize.width;
            float heightHeader = 70;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 70 ;
            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderTitlesHeader drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderTitlesHeader];
        }

        yQuestions = y+170;
            
            if (yQuestions >= (maxHeightToFooter)) {
                if (i != 4) {
                    yQuestions = 0;
                    pageNumber++;
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                    [self drawBackground];
                    [self drawHeaderFooter:pageNumber];
                    heightCounter = 100;
                    y = heightCounter;
                }
            }
        }
    
    float bioYAxis = [self orderBiologicalsColumns:y];
    float paraYAxis = [self orderParasiticidesColumns:bioYAxis];
    float synovexYAxis = [self orderSynovexColumns:paraYAxis];
    float dairyYAxis = [self orderDairyColumns:synovexYAxis];
    float eqBioYAxis = [self orderEqBiologicalsColumns:dairyYAxis];
    [self orderEqParasiticidesColumns:eqBioYAxis];

}

#pragma -mark Cattle-Biologicals

-(float)orderBiologicalsColumns :(float)y {
    
    int counter = 0;
    for (int i=0; i<self.orderCatBiologicalsCount; i++) {
        
        NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i] objectAtIndex:7]];
        NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i] objectAtIndex:8]];
        if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
            counter = counter + 1;
        }
    }
    
    if (counter == 0) {
        return y;
    }

    for (int i=0; i<self.orderCatBiologicalsCount; i++) {
        if (i == 0) {
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 0 ;
            self._pdflblOrderCategory.text = @"BIOLOGICALS";
            if (self.categoryTotalCost.count > 0) {
//                float synovexValue = [[self.categoryTotalCost objectAtIndex:0] floatValue];
                self._pdflblOrderTotalCost.text = [self.categoryTotalCost objectAtIndex:0];
            }
            else {
                self._pdflblOrderTotalCost.text = @"";
            }

            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderCategoryHeading drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderCategoryHeading];
            
            yQuestions = y+170;
            
            if (yQuestions >= (maxHeightToFooter)) {
                if (i != 4) {
                    yQuestions = 0;
                    pageNumber++;
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                    [self drawBackground];
                    [self drawHeaderFooter:pageNumber];
                    float heightCounter = 50;
                    y = heightCounter;
                }
            }
        }
        else if (i >= 1 ) {
            
            self._pdflblProducts.text = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:0]];
            self._pdflblSKU.text = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:1]];
            self._pdflblSize.text = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:2]];
            self._pdflblOrderJuly.text = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:3]];
            self._pdflblOrderSept.text = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:4]];
            self._pdflblOrderNov.text = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:5]];
            self._pdflblOrderPTD.text = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:6]];
            
            NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:7]];
            NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:8]];
            
            self._pdflblOrderEarlyRebate.text = txtEarlyPurchase;
            self._pdflblOrderSeptOct.text = txtSeptOct;
            
            self._pdflblOrderPrice.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:9]];
            self._pdflblOrderCost.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleBio objectAtIndex:i-1] objectAtIndex:10]];
            
            if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
                
                float widthHeader = pagSize.width;
                float heightHeader = 30;
                float xHeader = 0;
                float yHeader = y  + 30;
                y =   yHeader ;
//                if (y == 80) {
//                    [self.orderBottomUIView setHidden:false];
//                }
//                else {
//                    [self.orderBottomUIView setHidden:true];
//                }
                CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
                [self.orderValuesCell drawRect:imageHeader];
                [self setupValuesView:imageHeader :self.orderValuesCell];
                yQuestions = y+170;
                
                if (yQuestions >= (maxHeightToFooter)) {
                    if (i != 4) {
                        yQuestions = 0;
                        pageNumber++;
                        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                        [self drawBackground];
                        [self drawHeaderFooter:pageNumber];
                        float heightCounter = 50;
                        y = heightCounter;
                    }
                }
            }
        }
    }
    
    return y  + 30;
}

-(float)orderParasiticidesColumns :(float)y {

    int counter = 0;
    for (int i=0; i<self.orderCatParasiticidesCount; i++) {
        
        NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i] objectAtIndex:7]];
        NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i] objectAtIndex:8]];
        if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
            counter = counter + 1;
        }
    }
    
    if (counter == 0) {
        return y;
    }
    
    for (int i=0; i<self.orderCatParasiticidesCount; i++) {
        if (i == 0) {
            
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 0 ;
            
            self._pdflblOrderCategory.text = @"PARASITICIDES";
            if (self.categoryTotalCost.count > 1) {
//                float synovexValue = [[self.categoryTotalCost objectAtIndex:1] floatValue];
                self._pdflblOrderTotalCost.text = [self.categoryTotalCost objectAtIndex:1];
            }
            else {
                self._pdflblOrderTotalCost.text = @"";
            }

            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderCategoryHeading drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderCategoryHeading];
            
            yQuestions = y+170;
            
            if (yQuestions >= (maxHeightToFooter)) {
                if (i != 4) {
                    yQuestions = 0;
                    pageNumber++;
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                    [self drawBackground];
                    [self drawHeaderFooter:pageNumber];
                    float heightCounter = 50;
                    y = heightCounter;
                }
            }
        }
        else if (i >= 1 ) {
            
            self._pdflblProducts.text = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:0]];
            self._pdflblSKU.text = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:1]];
            self._pdflblSize.text = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:2]];
            self._pdflblOrderJuly.text = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:3]];
            self._pdflblOrderSept.text = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:4]];
            self._pdflblOrderNov.text = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:5]];
            self._pdflblOrderPTD.text = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:6]];
            
            NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:7]];
            NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:8]];
            
            self._pdflblOrderEarlyRebate.text = txtEarlyPurchase;
            self._pdflblOrderSeptOct.text = txtSeptOct;
            
            self._pdflblOrderPrice.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:9]];
            self._pdflblOrderCost.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleParas objectAtIndex:i-1] objectAtIndex:10]];
            
            if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
                
                float widthHeader = pagSize.width;
                float heightHeader = 30;
                float xHeader = 0;
                float yHeader = y  + 30;
                y =   yHeader ;
                CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
                [self.orderValuesCell drawRect:imageHeader];
                [self setupValuesView:imageHeader :self.orderValuesCell];
                
                yQuestions = y+170;
                
                if (yQuestions >= (maxHeightToFooter)) {
                    if (i != 4) {
                        yQuestions = 0;
                        pageNumber++;
                        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                        [self drawBackground];
                        [self drawHeaderFooter:pageNumber];
                        float heightCounter = 50;
                        y = heightCounter;
                    }
                }
            }
        }
    }
    return y + 30;
}

-(float)orderSynovexColumns :(float)y {
    
    int counter = 0;
    for (int i=0; i<self.orderCatSynovexCount; i++) {
        
        NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i] objectAtIndex:7]];
        NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i] objectAtIndex:8]];
        if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
            counter = counter + 1;
        }
    }
    
    if (counter == 0) {
        return y;
    }
    
    for (int i=0; i<self.orderCatSynovexCount; i++) {
        if (i == 0) {
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 0 ;
            
            self._pdflblOrderCategory.text = @"CATTLE REPROS/SYNOVEX";
            if (self.categoryTotalCost.count > 2) {
//                float synovexValue = [[self.categoryTotalCost objectAtIndex:2] floatValue];
                self._pdflblOrderTotalCost.text = [self.categoryTotalCost objectAtIndex:2];
            }
            else {
                self._pdflblOrderTotalCost.text = @"";
            }

            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderCategoryHeading drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderCategoryHeading];
            
            yQuestions = y+170;
            
            if (yQuestions >= (maxHeightToFooter)) {
                if (i != 4) {
                    yQuestions = 0;
                    pageNumber++;
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                    [self drawBackground];
                    [self drawHeaderFooter:pageNumber];
                    float heightCounter = 50;
                    y = heightCounter;
                }
            }
        }
        else if (i >= 1 ) {
            
            self._pdflblProducts.text = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:0]];
            self._pdflblSKU.text = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:1]];
            self._pdflblSize.text = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:2]];
            self._pdflblOrderJuly.text = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:3]];
            self._pdflblOrderSept.text = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:4]];
            self._pdflblOrderNov.text = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:5]];
            self._pdflblOrderPTD.text = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:6]];
            
            NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:7]];
            NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:8]];
            
            self._pdflblOrderEarlyRebate.text = txtEarlyPurchase;
            self._pdflblOrderSeptOct.text = txtSeptOct;
            
            self._pdflblOrderPrice.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:9]];
            self._pdflblOrderCost.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleSynovex objectAtIndex:i-1] objectAtIndex:10]];
            
            if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
                
                float widthHeader = pagSize.width;
                float heightHeader = 30;
                float xHeader = 0;
                float yHeader = y  + 30;
                y =   yHeader ;
                CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
                [self.orderValuesCell drawRect:imageHeader];
                [self setupValuesView:imageHeader :self.orderValuesCell];
                
                yQuestions = y+170;
                
                if (yQuestions >= (maxHeightToFooter)) {
                    if (i != 4) {
                        yQuestions = 0;
                        pageNumber++;
                        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                        [self drawBackground];
                        [self drawHeaderFooter:pageNumber];
                        float heightCounter = 50;
                        y = heightCounter;
                    }
                }
            }
        }
    }
    return y  + 30;
}

-(float)orderDairyColumns :(float)y {
    
    int counter = 0;
    for (int i=0; i<self.orderCatDairyCount; i++) {
        
        NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i] objectAtIndex:7]];
        NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i] objectAtIndex:8]];
        if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
            counter = counter + 1;
        }
    }
    
    if (counter == 0) {
        return y;
    }

    for (int i=0; i<self.orderCatDairyCount; i++) {
        if (i == 0) {
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 0 ;
            
            self._pdflblOrderCategory.text = @"DAIRY IMMS";
            if (self.categoryTotalCost.count > 3) {
//                float synovexValue = [[self.categoryTotalCost objectAtIndex:3] floatValue];
                self._pdflblOrderTotalCost.text = [self.categoryTotalCost objectAtIndex:3];
            }
            else {
                self._pdflblOrderTotalCost.text = @"";
            }

            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderCategoryHeading drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderCategoryHeading];
            
            yQuestions = y+170;
            
            if (yQuestions >= (maxHeightToFooter)) {
                if (i != 4) {
                    yQuestions = 0;
                    pageNumber++;
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                    [self drawBackground];
                    [self drawHeaderFooter:pageNumber];
                    float heightCounter = 50;
                    y = heightCounter;
                }
            }
        }
        else if (i >= 1 ) {
            
            self._pdflblProducts.text = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:0]];
            self._pdflblSKU.text = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:1]];
            self._pdflblSize.text = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:2]];
            self._pdflblOrderJuly.text = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:3]];
            self._pdflblOrderSept.text = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:4]];
            self._pdflblOrderNov.text = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:5]];
            self._pdflblOrderPTD.text = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:6]];
            
            NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:7]];
            NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:8]];
            
            self._pdflblOrderEarlyRebate.text = txtEarlyPurchase;
            self._pdflblOrderSeptOct.text = txtSeptOct;
            
            self._pdflblOrderPrice.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:9]];
            self._pdflblOrderCost.text = [NSString stringWithFormat:@"$ %@",[[_orderCattleDairy objectAtIndex:i-1] objectAtIndex:10]];
            
            if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
                
                float widthHeader = pagSize.width;
                float heightHeader = 30;
                float xHeader = 0;
                float yHeader = y  + 30;
                y =   yHeader ;
                CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
                [self.orderValuesCell drawRect:imageHeader];
                [self setupValuesView:imageHeader :self.orderValuesCell];
                
                yQuestions = y+170;
                
                if (yQuestions >= (maxHeightToFooter)) {
                    if (i != 4) {
                        yQuestions = 0;
                        pageNumber++;
                        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                        [self drawBackground];
                        [self drawHeaderFooter:pageNumber];
                        float heightCounter = 50;
                        y = heightCounter;
                    }
                }
            }
        }
    }
    return y  + 30;
}

-(float)orderEqBiologicalsColumns :(float)y {
    
    int counter = 0;
    for (int i=0; i<self.orderEqBiologicalsCount; i++) {
        
        NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i] objectAtIndex:7]];
        NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i] objectAtIndex:8]];
        if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
            counter = counter + 1;
        }
    }
    
    if (counter == 0) {
        return y;
    }

    for (int i=0; i<self.orderEqBiologicalsCount; i++) {
        if (i == 0) {
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 0 ;
            
            self._pdflblOrderCategory.text = @"EQUINE BIOLOGICALS";
            if (self.categoryTotalCost.count > 4) {
//                float synovexValue = [[self.categoryTotalCost objectAtIndex:4] floatValue];
                self._pdflblOrderTotalCost.text = [self.categoryTotalCost objectAtIndex:4];
            }
            else {
                self._pdflblOrderTotalCost.text = @"";
            }

            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderCategoryHeading drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderCategoryHeading];
            
            yQuestions = y+170;
            
            if (yQuestions >= (maxHeightToFooter)) {
                if (i != 4) {
                    yQuestions = 0;
                    pageNumber++;
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                    [self drawBackground];
                    [self drawHeaderFooter:pageNumber];
                    float heightCounter = 50;
                    y = heightCounter;
                }
            }
        }
        else if (i >= 1 ) {
            
            self._pdflblProducts.text = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:0]];
            self._pdflblSKU.text = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:1]];
            self._pdflblSize.text = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:2]];
            self._pdflblOrderJuly.text = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:3]];
            self._pdflblOrderSept.text = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:4]];
            self._pdflblOrderNov.text = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:5]];
            self._pdflblOrderPTD.text = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:6]];
            
            NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:7]];
            NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:8]];
            
            self._pdflblOrderEarlyRebate.text = txtEarlyPurchase;
            self._pdflblOrderSeptOct.text = txtSeptOct;
            
            self._pdflblOrderPrice.text = [NSString stringWithFormat:@"$ %@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:9]];
            self._pdflblOrderCost.text = [NSString stringWithFormat:@"$ %@",[[_orderEquineBio objectAtIndex:i-1] objectAtIndex:10]];
            
            if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
                
                float widthHeader = pagSize.width;
                float heightHeader = 30;
                float xHeader = 0;
                float yHeader = y  + 30;
                y =   yHeader ;
                CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
                [self.orderValuesCell drawRect:imageHeader];
                [self setupValuesView:imageHeader :self.orderValuesCell];
                
                yQuestions = y+170;
                
                if (yQuestions >= (maxHeightToFooter)) {
                    if (i != 4) {
                        yQuestions = 0;
                        pageNumber++;
                        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                        [self drawBackground];
                        [self drawHeaderFooter:pageNumber];
                        float heightCounter = 50;
                        y = heightCounter;
                    }
                }
            }
        }
    }
    return y  + 30;
}

-(void)orderEqParasiticidesColumns :(float)y {
    
    int counter = 0;
    for (int i=0; i<self.orderEqParasiticidesCount; i++) {
        
        NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i] objectAtIndex:7]];
        NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i] objectAtIndex:8]];
        if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
            counter = counter + 1;
        }
    }
    
    if (counter == 0) {
        return;
    }
    

    
    for (int i=0; i<self.orderEqParasiticidesCount; i++) {
        if (i == 0) {
            float widthHeader = pagSize.width;
            float heightHeader = 30;
            float xHeader = 0;
            float yHeader = y ;
            y =   yHeader + 0 ;
            
            self._pdflblOrderCategory.text = @"EQUINE PARASITICIDES";
            if (self.categoryTotalCost.count > 5) {
//                float synovexValue = [[self.categoryTotalCost objectAtIndex:5] floatValue];
                self._pdflblOrderTotalCost.text = [self.categoryTotalCost objectAtIndex:5];
            }
            else {
                self._pdflblOrderTotalCost.text = @"";
            }

            CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
            [self.orderCategoryHeading drawRect:imageHeader];
            [self setupValuesView:imageHeader :self.orderCategoryHeading];
            
            yQuestions = y+170;
            
            if (yQuestions >= (maxHeightToFooter)) {
                if (i != 4) {
                    yQuestions = 0;
                    pageNumber++;
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                    [self drawBackground];
                    [self drawHeaderFooter:pageNumber];
                    float heightCounter = 50;
                    y = heightCounter;
                }
            }
        }
        else if (i >= 1 ) {
            
            self._pdflblProducts.text = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:0]];
            self._pdflblSKU.text = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:1]];
            self._pdflblSize.text = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:2]];
            self._pdflblOrderJuly.text = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:3]];
            self._pdflblOrderSept.text = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:4]];
            self._pdflblOrderNov.text = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:5]];
            self._pdflblOrderPTD.text = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:6]];
            
            NSString *txtEarlyPurchase = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:7]];
            NSString *txtSeptOct = [NSString stringWithFormat:@"%@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:8]];
            
            self._pdflblOrderEarlyRebate.text = txtEarlyPurchase;
            self._pdflblOrderSeptOct.text = txtSeptOct;
            
            self._pdflblOrderPrice.text = [NSString stringWithFormat:@"$ %@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:9]];
            self._pdflblOrderCost.text = [NSString stringWithFormat:@"$ %@",[[_orderEquineParas objectAtIndex:i-1] objectAtIndex:10]];
            
            if (![txtEarlyPurchase isEqualToString:@"0"] || ![txtSeptOct isEqualToString:@"0"] ) {
                
                float widthHeader = pagSize.width;
                float heightHeader = 30;
                float xHeader = 0;
                float yHeader = y  + 30;
                y =   yHeader ;
                CGRect imageHeader = CGRectMake(xHeader, yHeader, widthHeader, heightHeader);
                [self.orderValuesCell drawRect:imageHeader];
                [self setupValuesView:imageHeader :self.orderValuesCell];
                
                yQuestions = y+170;
                
                if (yQuestions >= (maxHeightToFooter)) {
                    if (i != 4) {
                        yQuestions = 0;
                        pageNumber++;
                        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0,pagSize.width, pagSize.height), nil);
                        [self drawBackground];
                        [self drawHeaderFooter:pageNumber];
                        float heightCounter = 50;
                        y = heightCounter;
                    }
                }
            }
        }
    }
}

-(void)setupValuesView:(CGRect)imageHeader :(UIView *)subView {
    
    UIGraphicsBeginImageContextWithOptions(imageHeader.size, subView.opaque, 0.0);
    struct CGContext *context = UIGraphicsGetCurrentContext();
    //    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    //    CGContextSetStrokeColorWithColor(context, [[UIColor greenColor] CGColor]);
    //    CGContextFillRect(context, imageHeader);
    [subView.layer renderInContext:context];
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    [snapshotImageFromMyView drawInRect:imageHeader];
}
- (float) heightForText: (NSString *) string font: (UIFont *) font width: (float) width {
    NSDictionary *fontAttributes = [NSDictionary dictionaryWithObject: font
                                                               forKey: NSFontAttributeName];
    CGRect rect = [string boundingRectWithSize: CGSizeMake(width, INT_MAX)
                                       options: NSStringDrawingUsesLineFragmentOrigin
                                    attributes: fontAttributes
                                       context: nil];
    return rect.size.height;
}

#pragma -mark PDF Table - Cells

-(void)drawTextRect :(NSString *)drawableText :(float)x :(float)y :(int)index :(CGSize) size
{
    CGSize sizeBioaudit = size;
    float widthBioaudit = sizeBioaudit.width;
    float heightBioaudit = sizeBioaudit.height;
    
    CGRect txtBioauditSize = CGRectMake( x, y, widthBioaudit, heightBioaudit);
    
    UIView *viewH = [[UIView alloc] initWithFrame:txtBioauditSize];
    [viewH setBackgroundColor:[UIColor clearColor]];
    [viewH drawRect:txtBioauditSize];
    
    UILabel *lblHeading = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, viewH.bounds.size.width-30, viewH.bounds.size.height)];
    [lblHeading setBackgroundColor:[UIColor clearColor]];
    [lblHeading setTextColor:[UIColor darkGrayColor]];
    [lblHeading setText:drawableText];
    [lblHeading setNumberOfLines:0];
    [lblHeading setContentMode:UIViewContentModeCenter];
    [lblHeading setLineBreakMode:NSLineBreakByWordWrapping];
    
    [lblHeading setFont:fntTitle];
    [lblHeading setTextAlignment:NSTextAlignmentLeft];
    [viewH addSubview:lblHeading];
    
    UIGraphicsBeginImageContextWithOptions(viewH.frame.size, NO, 0.0);
//    UIGraphicsBeginImageContext(viewH.bounds.size);
    struct CGContext *context = UIGraphicsGetCurrentContext();
    [viewH.layer renderInContext:context];
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [snapshotImageFromMyView drawInRect:txtBioauditSize];
}

#pragma -mark General Information
-(CGSize)generalInfoHeight:(float) width :(int) index
{
    NSString *drawableText = @"";
    
    switch (index) {
        case 0:
            drawableText = [NSString stringWithFormat:@"NARC NAME: "];
            break;
        case 2:
            drawableText = [NSString stringWithFormat:@"NARC ID: "];
            break;
        case 4:
            drawableText = [NSString stringWithFormat:@"AREA:  "];
            break;
        case 6:
            drawableText = [NSString stringWithFormat:@"TERRITORY: "];
            break;
        case 8:
            drawableText = [NSString stringWithFormat:@"MZR TIER: "];
            break;
        case 1:
            drawableText = [accountInfoArray objectAtIndex:0];
            break;
        case 3:
            drawableText = [accountInfoArray objectAtIndex:1];
            break;
        case 5:
            drawableText = [accountInfoArray objectAtIndex:2];
            break;
        case 7:
            drawableText = [accountInfoArray objectAtIndex:3];
            break;
        case 9:
            drawableText = [accountInfoArray objectAtIndex:4];
            drawableText = drawableText.length == 0 ? @"-" : drawableText;
            break;

        default:
            break;
    }
    
    //    float height = [self heightForText:drawableText font:fntSubtitle width:width];
    float height = 25;
    return CGSizeMake(width, height);
    
}

-(void)generalInfoSetting:(float) x :(float) y :(int) index : (CGSize)size
{
    NSString *drawableText = @"";
    
    switch (index) {
        case 0:
            drawableText = [NSString stringWithFormat:@"NARC NAME: "];
            break;
        case 2:
            drawableText = [NSString stringWithFormat:@"NARC ID: "];
            break;
        case 4:
            drawableText = [NSString stringWithFormat:@"AREA:  "];
            break;
        case 6:
            drawableText = [NSString stringWithFormat:@"TERRITORY: "];
            break;
        case 8:
            drawableText = [NSString stringWithFormat:@"MZR TIER: "];
            break;
        case 1:
            drawableText = [accountInfoArray objectAtIndex:0];
            break;
        case 3:
            drawableText = [accountInfoArray objectAtIndex:1];
            break;
        case 5:
            drawableText = [accountInfoArray objectAtIndex:2];
            break;
        case 7:
            drawableText = [accountInfoArray objectAtIndex:3];
            break;
        case 9:
            drawableText = [accountInfoArray objectAtIndex:4];
            drawableText = drawableText.length == 0 ? @"-" : drawableText;
            break;

        default:
            break;
    }
    
    [self drawTextRect:drawableText :x :y :index :size];
    
}

-(float)GeneralInformation:(float) width :(float) height :(float) x :(float) y :(UIColor*) color {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    float border = 1;
    
    UIColor *colorBorderRslt = [UIColor lightGrayColor];
    
    float yAxis = y ;
    int   fieldsCounter = 0 ;
    for (int i=0; i <= 4; i++) {
        if (i > 4) {
            
            CGContextSetLineWidth(context,border);
            CGContextSetStrokeColorWithColor(context, [colorBorderRslt CGColor]);
            CGRect headRectangle = CGRectMake(x, yAxis, (width) - 20, 35);
            CGContextAddRect(context, headRectangle);
            CGContextStrokePath(context);
            CGContextSetFillColorWithColor(context, [[UIColor colorWithRed:244.0f/255.0f green:159.0f/255.0f blue:63.0f/255.0f alpha:1.0f] CGColor]);
            CGContextFillRect(context, headRectangle);
            [self generalInfoSetting:headRectangle.origin.x+10 :headRectangle.origin.y+5 :0 :CGSizeMake(width, 35)];
            
            yAxis = yAxis + headRectangle.size.height;
            
            
        } else {
            
            //            CGRect rectangle = CGRectMake(x, yAxis, (width/2), [self generalInfoHeight:(width/2) :fieldsCounter].height);
            CGRect rectangle = CGRectMake(x, yAxis, (width/2), 25);
            
            //            CGRect rectangle2 = CGRectMake((width/2), yAxis, (width/2), [self generalInfoHeight:(width/2) :fieldsCounter + 1].height);
            CGRect rectangle2 = CGRectMake((width/2), yAxis, (width/2), 25);
            
            if (rectangle.size.height > rectangle2.size.height) {
                rectangle2.size.height = rectangle.size.height;
            }
            else if (rectangle2.size.height > rectangle.size.height) {
                rectangle.size.height = rectangle2.size.height;
            }
            
            CGContextSetLineWidth(context,border);
            CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
            CGContextAddRect(context, rectangle);
            CGContextStrokePath(context);
            
            CGContextSetFillColorWithColor(context, [colorWhite CGColor]);
            CGContextFillRect(context, rectangle);
            
            CGContextSetLineWidth(context,border);
            CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
            CGContextAddRect(context, rectangle2);
            CGContextStrokePath(context);
            
            CGContextSetFillColorWithColor(context, [colorWhite CGColor]);
            CGContextFillRect(context, rectangle2);
            
            [self generalInfoSetting:rectangle.origin.x+10 :rectangle.origin.y :fieldsCounter :[self generalInfoHeight:(width/2) :fieldsCounter]];
            fieldsCounter++;
            //            if (fieldsCounter < 8) {
            [self generalInfoSetting:rectangle2.origin.x+10 :rectangle2.origin.y :fieldsCounter :[self generalInfoHeight:(width/2) :fieldsCounter]];
            fieldsCounter++;
            
            yAxis = yAxis + rectangle.size.height;
            
        }
    }
    
    return yAxis;
}

#pragma -mark Color Rendering
+(void)SetGradientColorToUIView:(UIView *)gView {
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = gView.bounds;
    
    gradientLayer.colors = @[ (__bridge id)[UIColor colorWithRed:38.0/255.0 green:36.0/255.0 blue:99.0/255.0 alpha:1.0].CGColor,
                              (__bridge id)[UIColor colorWithRed:40.0/255.0 green:170.0/255.0 blue:220.0/255.0 alpha:1.0].CGColor ];
    gradientLayer.startPoint = CGPointMake(1.0, 0.0);
    gradientLayer.endPoint = CGPointMake(0.0, 0.0);
    [gView.layer insertSublayer:gradientLayer atIndex:0];
    
    [gView setNeedsDisplay];
}

- (void)loadResources {
    colorBackground = [UIColor colorWithRed:242.0f/255.0f green:246.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
    colorBar = [UIColor colorWithRed:231.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f];
    colorTitle = [UIColor colorWithRed:197.0f/255.0f green:54.0f/255.0f blue:78.0f/255.0f alpha:1.0f];
    colorGreen = [UIColor colorWithRed:47.0f/255.0f green:118.0f/255.0f blue:102.0f/255.0f alpha:1.0f];
    colorLightGreen = [UIColor colorWithRed:137.0f/255.0f green:194.0f/255.0f blue:197.0f/255.0f alpha:1.0f];
    colorBlack = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1.0f];
    colorRed = [UIColor colorWithRed:197.0f/255.0f green:54.0f/255.0f blue:78.0f/255.0f alpha:1.0f];
    colorOrange = [UIColor colorWithRed:255.0f/255.0f green:140.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    colorWhite = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    
    fntPager = [UIFont fontWithName:@"GOTHAMXNARROW-MEDIUM" size:12];
    
    fntTitleBold = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    fntTitle = [UIFont fontWithName:@"GOTHAMXNARROW-MEDIUM" size:12.0];
    fntSubtitle = [UIFont fontWithName:@"Helvetica" size: 20.0];
    fntPreTitle = [UIFont fontWithName:@"Helvetica" size: 14];
    
    fntFarm = [UIFont fontWithName:@"Helvetica" size: 8];
    
    fntQuestion = [UIFont fontWithName:@"Helvetica" size: 10];
    
    fntResult1 = [UIFont fontWithName:@"Helvetica-Bold" size: 9];
    fntResult2 = [UIFont fontWithName:@"Helvetica" size: 7];
    
    fntResultSmall = [UIFont fontWithName:@"Helvetica" size: 5];
}*/

@end
