
//
//  AppDelegate.m
//  Benchmark
//
//  Created by Html on 19/04/2018.
//  Copyright © 2018 Celeritas. All rights reserved.
//

#import "AppDelegate.h"
#import "DashboardViewController.h"
#import "SplashViewController.h"
#import "IQKeyboardManager.h"
#import "LoginViewController.h"

#import <AppCenter/AppCenter.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    _score = 0;
    _totalYes = 0;
    _totalYesNo = 0;
    _indexValueForResponseMaster = 00000;
    _indexValue = 00000;
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:10];
    
    //Enabling autoToolbar behaviour. If It is set to NO. You have to manually create IQToolbar for keyboard.
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    //Setting toolbar behavious to IQAutoToolbarBySubviews. Set it to IQAutoToolbarByTag to manage previous/next according to UITextField’s tag property in increasing order.
    
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarBySubviews];
    //Resign textField if touched outside of UITextField/UITextView.
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    //Giving permission to modify TextView’s frame
    [[IQKeyboardManager sharedManager] setCanAdjustTextView:YES];
    
    //Show TextField placeholder texts on autoToolbar
    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:YES];

    
    // Initialize the dbManager property.
  //  self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"DBBenchmark.db"];
    
      [self copyDatabaseIfNeeded];
    
    //_dicAssessmentAnswersArray = [[NSMutableDictionary alloc]init];
    _isAssessmentComment = [[NSMutableDictionary alloc]init];
    _isAssessmentImage = [[NSMutableDictionary alloc]init];
    _arrAssessmentEmployees = [NSMutableArray new];

    
    _isReview = false;
    _isSubmitted = false;
    _isStandardSaved = false;
    _isResponsesSaved = false;
    _isPartial = false;
    _reviewID = 0;

    self.window = [UIWindow new];
    self.window.frame = [UIScreen mainScreen].bounds;
    
    UINavigationController *navigation ;
     // [NSThread sleepForTimeInterval:3];
  
    
    BOOL remember = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"];
    if (remember) {
        DashboardViewController *welcomeVC = [DashboardViewController new];
        navigation = [[UINavigationController alloc]initWithRootViewController:welcomeVC];
    }
    else {
        
        LoginViewController *welcomeVC = [LoginViewController new];
        navigation = [[UINavigationController alloc]initWithRootViewController:welcomeVC];
    }

    navigation.navigationBar.hidden = YES;

    [self.window setRootViewController:navigation];    
    [self.window makeKeyAndVisible];
    
    // Override point for customization after application launch.
    return YES;
}

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];
    if (self) {
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        
        // Copy the database file into the documents directory if necessary.
        [self copyDatabaseIntoDocumentsDirectory];
    }
    return self;
}

-(void)copyDatabaseIntoDocumentsDirectory
{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath])
    {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        
        NSError *error;
        
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) copyDatabaseIfNeeded {
    
    //Using NSFileManager we can perform many file system operations.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *dbPath = [self getDBPath];
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    NSLog(@"DBPath:%@",dbPath);
    if(!success) {
        
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"DBBenchmark.db"];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

- (NSString *) getDBPath {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"DBBenchmark.db"];
}
@end
